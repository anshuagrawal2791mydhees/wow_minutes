package com.mydhees.wow.partner.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.mydhees.wow.partner.MyWishDetails;
import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.objects.WishObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Abhishek on 23-Jun-16.
 */

public class WishesCustAdapter extends RecyclerView.Adapter<WishesCustAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater layoutInflater;
    ArrayList<WishObject> wishes = new ArrayList<>();

    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    DateFormat timeFormat = new SimpleDateFormat("HH:mm");

    public WishesCustAdapter(Context context){
        this.context=context;
        layoutInflater = layoutInflater.from(context);
    }

    public void popular(WishObject popular){
        wishes.add(popular);
        notifyItemInserted(wishes.size());
    }

    public ArrayList<WishObject> getWisheslist()
    {
        return wishes;
    }
    public void clearAll(){
        wishes.clear();
        notifyDataSetChanged();
    }
    public void addAll(ArrayList<WishObject> list){
        wishes.clear();
        wishes.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.wish_row,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, final int position) {
        WishObject wishObject = wishes.get(position);
        Log.e("wish log",wishes.toString());

            holder.name.setText(wishObject.getSerious() + "");
            holder.address.setText(wishObject.getAddress() + "");
            holder.minPrice.setText(wishObject.getMinPrice() + "");
            holder.maxPrice.setText(wishObject.getMaxPrice() + "");
            holder.checkInDate.setText(dateFormat.format(wishObject.getDate_checkin()) + "");
            holder.checkInTime.setText(timeFormat.format(wishObject.getDate_checkin()) + "");
            holder.checkOutDate.setText(dateFormat.format(wishObject.getDate_check_out()) + "");
            holder.checkOutTime.setText(timeFormat.format(wishObject.getDate_check_out()) + "");

            holder.cv_wish.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, MyWishDetails.class);
                    intent.putExtra("minPrice", wishes.get(position).getMinPrice());
                    intent.putExtra("maxPrice", wishes.get(position).getMaxPrice());
                    intent.putExtra("serious", wishes.get(position).getSerious());
                    intent.putExtra("rooms", wishes.get(position).getRooms());
                    intent.putExtra("child", wishes.get(position).getChild());
                    intent.putExtra("adults", wishes.get(position).getAdults());
                    intent.putExtra("city", wishes.get(position).getAddress());
                    intent.putExtra("wishid", wishes.get(position).getWishid());
                    intent.putExtra("cin", dateFormat.format(wishes.get(position).getDate_checkin()));
                    intent.putExtra("cinTime", timeFormat.format(wishes.get(position).getDate_checkin()));
                    intent.putExtra("cout", dateFormat.format(wishes.get(position).getDate_check_out()));
                    intent.putExtra("coutTime", timeFormat.format(wishes.get(position).getDate_check_out()));

                    context.startActivity(intent);

                }
            });



    }

    @Override
    public int getItemCount() {
        return wishes.size();
    }
    public void removeAt(int position) {
        wishes.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, wishes.size());
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder{
        private EditText name,address,checkInTime,checkInDate,checkOutTime,checkOutDate;
        private TextView minPrice,maxPrice;

        CardView cv_wish;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            name = (EditText) itemView.findViewById(R.id.nameWishRow);
            address = (EditText) itemView.findViewById(R.id.cityWishRow);
            minPrice = (TextView) itemView.findViewById(R.id.minPriceWishRow);
            maxPrice = (TextView) itemView.findViewById(R.id.maxPriceWishRow);
            checkInTime = (EditText) itemView.findViewById(R.id.checkin_timeWishR);
            checkInDate = (EditText) itemView.findViewById(R.id.checkin_dateWishR);
            checkOutTime = (EditText) itemView.findViewById(R.id.checkout_timeWishR);
            checkOutDate = (EditText) itemView.findViewById(R.id.checkout_dateWishR);


            cv_wish= (CardView) itemView.findViewById(R.id.cardo);



        }
    }
}
