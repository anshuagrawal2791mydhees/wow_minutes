package com.mydhees.wow.partner;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class registration2 extends AppCompatActivity {
    Button btn;
    EditText company_name;
    EditText address;
    EditText phone;
    EditText mobile;
    EditText email;
    EditText display_name;
    CheckBox hotel;
    CheckBox bus;
    CheckBox pg;
    CheckBox restaurants;
    CheckBox movies;
    CheckBox others;
    ArrayList<String> business_type = new ArrayList<>();
    Set<String> business_type_set = new HashSet<>();


    SharedPreferences prefs;

    SharedPreferences profile;
    Button locate_me;
    int PLACE_PICKER_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
       // final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs = getSharedPreferences("launchtime", Context.MODE_PRIVATE);
        profile = getSharedPreferences(
                "profile", Context.MODE_PRIVATE);

        company_name = (EditText)findViewById(R.id.companyname);
        address = (EditText)findViewById(R.id.address);
        phone = (EditText)findViewById(R.id.phone);
        mobile = (EditText)findViewById(R.id.mobile);
        email = (EditText)findViewById(R.id.email);
        display_name = (EditText)findViewById(R.id.displayname);

        hotel = (CheckBox)findViewById(R.id.hotel);
        bus = (CheckBox)findViewById(R.id.bus);
        pg = (CheckBox)findViewById(R.id.pg);
        restaurants = (CheckBox)findViewById(R.id.resturants);
        movies = (CheckBox)findViewById(R.id.movies);
        others = (CheckBox)findViewById(R.id.others);

        btn =(Button)findViewById(R.id.finish);
        locate_me = (Button)findViewById(R.id.locate_me);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(company_name.getText().toString().equals(""))
                    company_name.setError("Required");
                else if (address.getText().toString().equals(""))
                    address.setError("Required");
                else if (phone.getText().toString().equals(""))
                    phone.setError("Required");
                else if (mobile.getText().toString().equals(""))
                    mobile.setError("Required");
                else if (email.getText().toString().equals(""))
                    email.setError("Required");
                else if (display_name.getText().toString().equals(""))
                    display_name.setError("Required");
                else if(!(hotel.isChecked()||bus.isChecked()||pg.isChecked()||restaurants.isChecked()||movies.isChecked()||others.isChecked()))
                    hotel.setError("Check at least one");



                else {
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putBoolean("firstTime",true);
                    editor.commit();
                    SharedPreferences.Editor editor2 = profile.edit();
                    editor2.putString("company_name",company_name.getText().toString());
                    editor2.putString("address",address.getText().toString());
                    editor2.putString("phone",phone.getText().toString());
                    editor2.putString("email",email.getText().toString());
                    editor2.putString("mobile",mobile.getText().toString());
                    editor2.putString("display_name",display_name.getText().toString());

                    if(hotel.isChecked())
                        business_type.add(hotel.getText().toString());
                    if(bus.isChecked())
                        business_type.add(bus.getText().toString());
                    if(pg.isChecked())
                        business_type.add(pg.getText().toString());
                    if(restaurants.isChecked())
                        business_type.add(restaurants.getText().toString());
                    if(movies.isChecked())
                        business_type.add(movies.getText().toString());
                    if(others.isChecked())
                        business_type.add(others.getText().toString());

                    business_type_set.addAll(business_type);
                    editor2.putStringSet("business_type",business_type_set);
                    editor2.commit();
                    startActivity(new Intent(registration2.this,Home.class));
                }
            }
        });



        locate_me.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Please Wait...",Toast.LENGTH_SHORT).show();
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try
                {
                    startActivityForResult(builder.build(registration2.this), PLACE_PICKER_REQUEST);
                }
                catch (GooglePlayServicesRepairableException e)
                {
                    e.printStackTrace();
                }
                catch (GooglePlayServicesNotAvailableException e)
                {
                    e.printStackTrace();
                }            }
        });
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                String toastMsg = String.format("Place: %s", place.getName());
                Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
            }
        }
    }

//


}
