package com.mydhees.wow.partner.drawer_fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.VolleySingleton;
import com.mydhees.wow.partner.adapters.ReleaseOfferAdapter;
import com.mydhees.wow.partner.allConstants.AddroomConstants1;
import com.mydhees.wow.partner.allConstants.Constants;
import com.mydhees.wow.partner.allConstants.HotelRegistrationConstants;
import com.mydhees.wow.partner.allConstants.Release_constants;
import com.mydhees.wow.partner.objects.offer;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReleaseOffer extends Fragment {

    EditText no_rooms,sales_price,check_in,check_in_time,check_out,check_out_time,schedule_date,schedule_time,offername;
    Button add_btn;
    RecyclerView recycler;
    Calendar c;
    DateFormat simpledateformat12= new SimpleDateFormat("dd-MM-yyyy");
    DateFormat simpledateformat1= new SimpleDateFormat("yyyy-MM-dd");
    DateFormat simpledateformat123= new SimpleDateFormat("HH:mm");
    String Date, Time,min_exp;
    Spinner spinner;
    SeekBar seek,minute_seek;
    String hour1,min1;
    int roomtype;
    int index;
    int no_of_rooms;
    int salesprice;
    int mYear;
    int status;
    int mMonth;
    int mDay;
    int mHour,mHour1;
    int mMinute,mMinute1;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;


    //    DateFormat parser = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm");
    DateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
    String checkindatestring,scheduleDatestring,scheduleTimestring,expiryTimestring="",expireStringDate;
    String checkoutdatestring;
    String checkintimestring;
    Calendar as = Calendar.getInstance();
    String checkouttimestring;
    Date checkin_date,sched_date;
    Date checkout_date,expire_date;
    String checkinstring,scheduledatestring,expire_string;
    String checkoutstring;
    LinearLayoutManager linearLayoutManager;
    ReleaseOfferAdapter myadapter;
    boolean error = false,error2=false,error3=false,error4=false;
    Button releaseOffers;
    AlertDialog.Builder builder;
    AlertDialog alert;
    RecyclerView recycler2;
    TextView offer_time,offer_time1;
    ArrayList<String> hotel_names = new ArrayList<>();
    Spinner hotel_names_spin;
    SharedPreferences profile;
    SharedPreferences.Editor editor;
    HashMap<String,String> data = new HashMap<>();
    ArrayList<JSONObject> registered_rooms_jsons = new ArrayList<>();
    HashSet<String> registered_rooms_strings = new HashSet<>();


    ArrayList<String> registered_hotels_names = new ArrayList<>();
    ArrayList<String> registered_hotels_ids = new ArrayList<>();
    ArrayList<String> registered_room_noofrooms = new ArrayList<>();
    ArrayList<String> registered_room_id = new ArrayList<>();
    ArrayList<String> registered_room_type = new ArrayList<>();
    ArrayList<String> registered_room_priceMRP = new ArrayList<>();
    ArrayList<Integer> hotel_index = new ArrayList<>();
    ProgressDialog check_credit_dialog;

    int no_room;
    int room_price;
    public ReleaseOffer() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_release_offer,container, false);
       // parser.setTimeZone(TimeZone.getTimeZone("IST"));
       hideKeyboard(getContext());



        volleySingleton= VolleySingleton.getinstance();
        requestQueue = volleySingleton.getrequestqueue();

         check_credit_dialog = new ProgressDialog(getContext());
        check_credit_dialog.setCancelable(false);
        check_credit_dialog.show();


        profile = getActivity().getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE,Context.MODE_PRIVATE);
        editor= profile.edit();

        if(!check_credit(profile.getString(Constants.ID,"default")))
            //check_credit_dialog.dismiss();

        {
            Toast.makeText(getContext(),"Not Enough Credit",Toast.LENGTH_LONG).show();
            FragmentManager mFragmentManager;
            FragmentTransaction mFragmentTransaction;
            mFragmentManager = getActivity().getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction().setCustomAnimations(R.anim.card_flip_left_out, R.anim.card_flip_right_in);//R.anim.card_flip_right_in,R.anim.card_flip_right_out,R.anim.card_flip_left_in,R.anim.card_flip_left_out);
            mFragmentTransaction.replace(R.id.containerView,new Credits()).commit();
        }
        seek = (SeekBar) v.findViewById(R.id.seekbar);
        minute_seek = (SeekBar) v.findViewById(R.id.seekbar_time);
        no_rooms = (EditText)v.findViewById(R.id.no_rooms);
        offername = (EditText)v.findViewById(R.id.offer_name);
        sales_price = (EditText)v.findViewById(R.id.sales_price);
        check_in = (EditText)v.findViewById(R.id.check_in);

        check_in_time = (EditText)v.findViewById(R.id.check_in_time);
        check_in_time.setKeyListener(null);
        check_out = (EditText)v.findViewById(R.id.check_out);
        check_out.setKeyListener(null);
        check_out_time = (EditText)v.findViewById(R.id.check_out_time);
        check_out_time.setKeyListener(null);
        myadapter = new ReleaseOfferAdapter(getContext());
        spinner = (Spinner)v.findViewById(R.id.spi_roomtype);
        releaseOffers = (Button)v.findViewById(R.id.release_offer);
        schedule_date= (EditText) v.findViewById(R.id.schedule_date);
        schedule_time= (EditText) v.findViewById(R.id.schedule_time);
        offer_time= (TextView) v.findViewById(R.id.offer_time_view);
        offer_time1= (TextView) v.findViewById(R.id.offer_minutes1_view);


        hotel_names_spin= (Spinner) v.findViewById(R.id.spinner_hotel1);

        as.add(Calendar.MINUTE, 5);

        String date_sched_auto = simpledateformat12.format(as.getTime());

        scheduleTimestring=simpledateformat123.format(as.getTime());
        scheduleDatestring=simpledateformat1.format(as.getTime());
        schedule_date.setText(date_sched_auto);
        schedule_time.setText(scheduleTimestring);
        scheduledatestring = scheduleDatestring + "T" + scheduleTimestring;
        try {
            sched_date = parser.parse(scheduledatestring);
        } catch (ParseException e) {
            Toast.makeText(getContext(), "parse error in sched date", Toast.LENGTH_LONG).show();

        }
        Log.e("ach",scheduleDatestring);
        Log.e("ach",scheduledatestring);
        Log.e("jashan",sched_date.toString());

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                roomtype = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if(profile.getStringSet(Constants.REGISTERED_ROOMS,new HashSet<String>()).size()>0) {
            registered_rooms_strings.addAll(profile.getStringSet(Constants.REGISTERED_ROOMS,new HashSet<String>()));

            Iterator iterator = registered_rooms_strings.iterator();
            while(iterator.hasNext())
            {
                try {
                    registered_rooms_jsons.add(new JSONObject(iterator.next().toString()));
                } catch (JSONException e) {
                    Log.e("err",e.toString());
                }
            }
            for(int i=0;i<registered_rooms_jsons.size();i++)
            {
                try {
                    registered_hotels_names.add(registered_rooms_jsons.get(i).getString(Release_constants.HOTEL_NAME));
                    registered_hotels_ids.add(registered_rooms_jsons.get(i).getString(Release_constants.HOTEL_ID));
                    registered_room_noofrooms.add(registered_rooms_jsons.get(i).getString(Release_constants.NO_OF_ROOMS));
                    registered_room_id.add(registered_rooms_jsons.get(i).getString(Release_constants.ROOM_ID));
                    registered_room_type.add(registered_rooms_jsons.get(i).getString(Release_constants.ROOM_TYPE));
                    registered_room_priceMRP.add(registered_rooms_jsons.get(i).getString(Release_constants.PRICE_MRP));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            Log.e("rid",registered_room_id.toString());
            Log.e("rt",registered_room_type.toString());
            Log.e("rprc",registered_room_priceMRP.toString());
            Log.e("hid",registered_hotels_ids.toString());
            Log.e("names",registered_hotels_names.toString());
            Log.e("nroom",registered_room_noofrooms.toString());

            HashSet<String> registered_hotels_set_spinner = new HashSet<>(registered_hotels_names);
            ArrayList<String> hotel_list = new ArrayList<>(registered_hotels_set_spinner);
            ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item,hotel_list
            );
            hotel_names_spin.setAdapter(adapter1);


        }
        else{
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setCancelable(false);
            builder.setMessage("Add Rooms First.");
            builder.setPositiveButton("Add Rooms", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    FragmentManager mFragmentManager;
                    FragmentTransaction mFragmentTransaction;
                    mFragmentManager = getActivity().getSupportFragmentManager();
                    mFragmentTransaction = mFragmentManager.beginTransaction().setCustomAnimations(R.anim.card_flip_left_out, R.anim.card_flip_right_in);//R.anim.card_flip_right_in,R.anim.card_flip_right_out,R.anim.card_flip_left_in,R.anim.card_flip_left_out);
                    mFragmentTransaction.replace(R.id.containerView,new AddRoom()).commit();

                }
            });

            builder.setNegativeButton("Home", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    FragmentManager mFragmentManager;
                    FragmentTransaction mFragmentTransaction;
                    mFragmentManager = getActivity().getSupportFragmentManager();
                    mFragmentTransaction = mFragmentManager.beginTransaction().setCustomAnimations(R.anim.card_flip_left_out, R.anim.card_flip_right_in);//R.anim.card_flip_right_in,R.anim.card_flip_right_out,R.anim.card_flip_left_in,R.anim.card_flip_left_out);
                    mFragmentTransaction.replace(R.id.containerView,new LeadsDashboard()).commit();

                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();


        }

        hotel_names_spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                hotel_index.clear();
                Log.e("abc",parent.getItemAtPosition(position)+"");
                for(int i=0;i<registered_hotels_names.size();i++){

                    if(registered_hotels_names.get(i).equals(parent.getItemAtPosition(position))){

                        hotel_index.add(i);

                    }


                }

                Log.e("abc11",hotel_index.toString());
                ArrayList<String> room_type=new ArrayList<String>();
                for(int i=0; i< hotel_index.size(); i++){

                    room_type.add(registered_room_type.get(hotel_index.get(i)));

                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, room_type
                );

                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                index=hotel_index.get(position);
                Log.e("indexx",index+"");
                Log.e("hotelid",registered_hotels_ids.get(index));
                Log.e("roomid",registered_room_id.get(index));

                room_price = Integer.parseInt(registered_room_priceMRP.get(index));
                no_room=Integer.parseInt(registered_room_noofrooms.get(index));
                seek.setMax(room_price);
                sales_price.setText(registered_room_priceMRP.get(index));


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


//        linearLayoutManager = new LinearLayoutManager(getContext());
//        recycler.setLayoutManager(linearLayoutManager);
//        recycler.setAdapter(myadapter);
//      recycler2.setLayoutManager(new LinearLayoutManager(getContext()));
//        recycler2.setAdapter(myadapter);
        seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                sales_price.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        minute_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                 offer_time1.setText(String.valueOf(progress));
                min_exp=offer_time1.getText().toString();
                Calendar tmp = (Calendar) as.clone();


                tmp.add(Calendar.MINUTE, progress);
                expiryTimestring=simpledateformat123.format(tmp.getTime());
              //  expiryTimestring=scheduleTimestring;
                offer_time.setText(expiryTimestring);


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        no_rooms.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                int eneterd_room = 0;
                if(!no_rooms.getText().toString().matches(""))
                    eneterd_room=Integer.parseInt(no_rooms.getText().toString());
                if(eneterd_room>no_room){

                    no_rooms.setError("Entered No. of rooms are greater than registered.");
                    error3=true;
                }
                else {error3=false;}
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        sales_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //You can set value to seekbar here. obviously after checking if its valid and converting it into int/float.

                if(!s.toString().matches("") ){
                    int enteredProgress = Integer.parseInt(s.toString());
                    seek.setProgress(enteredProgress);
                    if(enteredProgress> room_price){
                        sales_price.setError("WoW Price value is greater than MRP");
                        error4=true;
                    }
                    else{error4=false;}
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                sales_price.setSelection(sales_price.getText().length());
            }
        });

        check_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                hideSoftKeyboard();

                // Get Current Date
                c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                  check_in.setError(null);
                check_out.setError(null);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                //String month = (monthOfYear+one)+"";
                                String month = String.format("%02d", monthOfYear + 1);
                                String day = String.format("%02d", dayOfMonth);
                               // String day1 = String.format("%02d", dayOfMonth+one);

//                                    checkindatestring = day + "-" + month + "-" + year;
                                checkindatestring = year + "-" + month + "-" + day;

                                check_in.setText(day + "-" + month + "-" + year);
                                try {
                                    c.setTime(simpledateformat1.parse(checkindatestring));

                                    c.add(Calendar.DATE,1);
                                    checkoutdatestring = simpledateformat1.format(c.getTime());
                                    String preview_checkout=simpledateformat12.format(c.getTime());
                                    check_out.setText(preview_checkout);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }



                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                //Toast.makeText(getContext(),mMonth+"",Toast.LENGTH_LONG).show();


            }



        });

        check_in_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();

                // Get Current Time
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
check_in_time.setError(null);
                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(),
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                String hour = String.format("%02d",hourOfDay);
                                String min = String.format("%02d",minute);
                                checkintimestring = hour + ":" + min;
                                check_in_time.setText(checkintimestring);

                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();

            }

        });

        check_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();

                // Get Current Date
                c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                check_out.setError(null);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                String month = String.format("%02d",monthOfYear+1);
                                String day = String.format("%02d",dayOfMonth);

//                                    checkoutdatestring = day + "-" + month + "-" + year;
                                checkoutdatestring = year+"-"+month+"-"+day;
                                // checkoutdatestring = dayOfMonth + "-" + (monthOfYear + one) + "-" + year;
                                check_out.setText(day+"-"+month+"-"+year);


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }



        });

        check_out_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();

                // Get Current Time
                c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
check_out_time.setError(null);
                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(),
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                String hour = String.format("%02d",hourOfDay);
                                String min = String.format("%02d",minute);
                                checkouttimestring = hour + ":" + min;
                                check_out_time.setText(checkouttimestring);

                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
            }

        });
        schedule_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();

                // Get Current Date
                mYear = as.get(Calendar.YEAR);
                mMonth = as.get(Calendar.MONTH);
                mDay = as.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth){
                                //String month = (monthOfYear+one)+"";
                                String month = String.format("%02d",monthOfYear+1);
                                String day = String.format("%02d",dayOfMonth);

//                                    scheduleDatestring = day + "-" + month + "-" + year;
                                scheduleDatestring = year+"-"+month+"-"+day;

                                schedule_date.setText(day + "-" + month + "-" + year);
//
                            }
                        },mYear,mMonth,mDay);
                datePickerDialog.show();
                //Toast.makeText(getContext(),mMonth+"",Toast.LENGTH_LONG).show();


            }



        });
        schedule_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();

                // Get Current Time
                min_exp=null;
                offer_time.setText("");
                offer_time1.setText("");
                minute_seek.setProgress(0);
                mHour1 = as.get(Calendar.HOUR_OF_DAY);
                mMinute1 = as.get(Calendar.MINUTE);


                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(),
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                hour1 = String.format("%02d",hourOfDay);
                                min1 = String.format("%02d",minute);
                                scheduleTimestring = hour1 + ":" + min1;
                                schedule_time.setText(scheduleTimestring);
                                try {
                                    as.setTime(simpledateformat123.parse(scheduleTimestring));
                                    expiryTimestring=simpledateformat123.format(as.getTime());
                                    //  expiryTimestring=scheduleTimestring;
                                    offer_time.setText(expiryTimestring);

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, mHour1, mMinute1, false);
                timePickerDialog.show();
            }

        }



        );



        releaseOffers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                error = false;
                if(offername.getText().toString().matches(""))
                {
                    offername.setError("required");
                    error=true;

                }
                if(no_rooms.getText().toString().matches(""))
                {
                    no_rooms.setError("required");
                    error=true;

                }
                else{

                    no_of_rooms = Integer.parseInt(no_rooms.getText().toString());

                }
                if(sales_price.getText().toString().matches(""))
                {
                    sales_price.setError("required");
                    error = true;
                }else{

                    salesprice = Integer.parseInt(sales_price.getText().toString());
                }
                if(check_in.getText().toString().matches(""))
                {
                    check_in.setError("required");
                    error = true;
                }
                if(check_in_time.getText().toString().matches(""))
                {
                    check_in_time.setError("required");
                    error = true;
                }
                if(check_out.getText().toString().matches(""))
                {
                    check_out.setError("required");
                    error = true;
                }
                if(check_out_time.getText().toString().matches(""))
                {
                    check_out_time.setError("required");
                    error = true;
                }
                if(schedule_date.getText().toString().matches(""))
                {
                    schedule_date.setError("required");
                    error = true;
                }
                if(schedule_time.getText().toString().matches(""))
                {
                    schedule_time.setError("required");
                    error = true;
                }
                if(minute_seek.getProgress()==0)
                {
                    Toast.makeText(getContext(), "Please set expiry time", Toast.LENGTH_SHORT).show();

                    error=true;

                }

                if(!(check_in.getText().toString().matches("")||check_in_time.getText().toString().matches("")))
                {
                    checkinstring = checkindatestring + "T" + checkintimestring;
                    try {
                        checkin_date = parser.parse(checkinstring);
                    } catch (ParseException e) {
                        Toast.makeText(getContext(), "parse error", Toast.LENGTH_LONG).show();

                    }
                    if(System.currentTimeMillis() > checkin_date.getTime())
                    {
                        Toast.makeText(getContext(),"wrong check in date",Toast.LENGTH_LONG).show();
                        error=true;
                    }

                }
                if(!(check_out.getText().toString().matches("")||check_out_time.getText().toString().matches("")))
                {
                    checkoutstring = checkoutdatestring + 'T' + checkouttimestring;
                    try {
                        checkout_date = parser.parse(checkoutstring);
                    } catch (ParseException e) {
                        Toast.makeText(getContext(), "parse error", Toast.LENGTH_LONG).show();

                    }
                    if(System.currentTimeMillis() > checkout_date.getTime())
                    {
                        Toast.makeText(getContext(),"wrong check out date", Toast.LENGTH_LONG).show();
                        error=true;
                    }

                }

                if(!(schedule_date.getText().toString().matches("")||schedule_time.getText().toString().matches(""))) {
                    scheduledatestring = scheduleDatestring + "T" + scheduleTimestring;
                    try {
                        sched_date = parser.parse(scheduledatestring);
                    } catch (ParseException e) {
                        Toast.makeText(getContext(), "parse error", Toast.LENGTH_LONG).show();

                    }


                    if (!(schedule_date.getText().toString().matches("") || check_in.getText().toString().matches("")||check_out_time.getText().toString().matches("")||check_in_time.getText().toString().matches("") || expiryTimestring.equals(""))) {

                        expireStringDate=scheduleDatestring+"T"+expiryTimestring;
                        Log.e("expdate",expireStringDate);
                        Log.e("exptime",expiryTimestring);

                        try {
                        expire_date = parser.parse(expireStringDate);
                    } catch (ParseException e) {
                        Toast.makeText(getContext(), "parse error1111", Toast.LENGTH_LONG).show();

                    }
                        Log.e("exptime",expire_date.toString());
                            if( sched_date.getTime()> checkin_date.getTime())
                        {

                            Toast.makeText(getContext(),"Wrong Schedule Date",Toast.LENGTH_LONG).show();
                            error=true;
                        }

                    }
                }
                if(!check_out.getText().toString().matches("")&&!check_in.getText().toString().matches("")&& !check_out_time.getText().toString().matches("") && !check_in_time.getText().toString().matches("") && !checkin_date.toString().matches("") && !checkout_date.toString().matches(""))
                {

                    if(checkin_date.getDate()>checkout_date.getDate())
                    {
                        Toast.makeText(getContext(),"check in can't be after check out ",Toast.LENGTH_LONG).show();
                        error = true;

                    }

                }

                if(error==false && error2==false && error3==false && error4==false) {
                    Log.e("error","result "+error);
                    Log.e("error2","result "+error2);
                    no_rooms.setError(null);
                    sales_price.setError(null);
                    check_in.setError(null);
                    check_out.setError(null);
                    check_in_time.setError(null);
                    check_out_time.setError(null);
                    schedule_time.setError(null);
                    schedule_date.setError(null);


                    if (myadapter.getItemCount() == 0)
                        //  recycler.setVisibility(View.VISIBLE);
                        Log.e("see this sched",sched_date.toString());
                    Log.e("see this che",checkout_date.toString());
                    myadapter.addoffer(new offer(registered_hotels_names.get(hotel_names_spin.getSelectedItemPosition()),offername.getText().toString(),no_of_rooms, salesprice, registered_room_type.get(spinner.getSelectedItemPosition()), checkin_date, checkout_date,sched_date,min_exp,expire_date));
                    //recycler.smoothScrollToPosition(0);

                    // Toast.makeText(getContext(), checkin_date.toString(), Toast.LENGTH_LONG).show();


                    builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Review Offers");
                    //    builder.setMessage("Release Offers?");

                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    //lp.setMargins(5,100,5,100);
                    lp.setMargins(0, 100, 5, 100);
                    recycler2 = new RecyclerView(getContext());
                    recycler2.setLayoutManager(new LinearLayoutManager(getContext()));
                    recycler2.setAdapter(myadapter);
                    //recycler2.setLayoutParams(lp);
                    LinearLayout layout = new LinearLayout(getContext());
                    layout.setOrientation(LinearLayout.VERTICAL);
                    layout.setLayoutParams(lp);
                    //layout.setLayoutParams(lp);
                    layout.addView(recycler2);


                    builder.setView(layout);
                    builder.setCancelable(false);

                    builder.setPositiveButton("Release", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            final ProgressDialog dialog1 = new ProgressDialog(getContext());
                            dialog1.show();

                            StringRequest request = new StringRequest(Request.Method.POST, Constants.RELEASE_OFFER_URL,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String respons) {
                                            dialog1.dismiss();
                                            Log.e("response",respons.toString());
                                            JSONObject response = null;
                                            HashSet<String> registered_offers = new HashSet<String>();
                                            try {
                                                response = new JSONObject(respons);
                                                registered_offers.add(response.toString());

                                            } catch (JSONException e) {
                                                Log.e("error",e.toString());
                                            }
                                            try {
                                                if(response.getBoolean("res"))
                                                {
                                                    Log.e("offerid",response.toString());

                                                    if(profile.getStringSet(Constants.RELEASED_OFFER, new HashSet<String>()).size()>0) {
                                                        Log.e("storing", "not_first");
                                                        //Log.e("data",profile.getStringSet(Constants.REGISTERED_HOTELS_NAMES,null).toString());
                                                        registered_offers.addAll(profile.getStringSet(Constants.RELEASED_OFFER, null));
                                                    }
//                                                  }
                                                    else {
                                                        Log.e("storing","first");
                                                    }
                                                    Log.e("offers",registered_offers.toString());
                                                    editor.putStringSet(Constants.RELEASED_OFFER, registered_offers);
                                                 editor.commit();

                                                    no_rooms.setText("");
                                                    sales_price.setText("");
                                                    check_in.setText("");
                                                    check_in_time.setText("");
                                                    check_out.setText("");
                                                    check_out_time.setText("");
                                                    schedule_date.setText("");
                                                    schedule_time.setText("");

                                                    offer_time.setText("");
                                                    FragmentManager mFragmentManager;
                                                    FragmentTransaction mFragmentTransaction;
                                                    mFragmentManager = getActivity().getSupportFragmentManager();
                                                    mFragmentTransaction = mFragmentManager.beginTransaction().setCustomAnimations(R.anim.card_flip_left_out, R.anim.card_flip_right_in);//R.anim.card_flip_right_in,R.anim.card_flip_right_out,R.anim.card_flip_left_in,R.anim.card_flip_left_out);
                                                    mFragmentTransaction.replace(R.id.containerView,new LeadsDashboard()).commit();

                                                    Toast.makeText(getContext(),"Successfully Released",Toast.LENGTH_LONG).show();
                                                }
                                                else
                                                {
                                                    Toast.makeText(getContext(),response.getString("response"),Toast.LENGTH_LONG).show();
                                                }
                                            } catch (JSONException e) {
                                                Toast.makeText(getContext(),e.toString(),Toast.LENGTH_LONG).show();
                                            }

                                        }
                                    }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    dialog1.dismiss();
                                    Toast.makeText(getContext(),error.toString(),Toast.LENGTH_LONG).show();
                                    Log.e("vollley error",error.toString());
                                }
                            }){
                                @Override
                                protected Map<String,String> getParams(){
                                    Map<String,String> params = new HashMap<String, String>();


                                    params.put(HotelRegistrationConstants.OWNER_ID,profile.getString(Constants.ID,"default"));
                                    params.put(Constants.EMAIL,profile.getString(Constants.EMAIL,"default"));
                                    params.put(Release_constants.HOTEL_ID,registered_hotels_ids.get(index));
                                    params.put(Release_constants.ROOM_ID,registered_room_id.get(index));
                                    params.put(Release_constants.ROOM_ID,registered_room_id.get(index));
                                    params.put(Release_constants.HOTEL_NAME,registered_hotels_names.get(index));
                                    params.put(Release_constants.OFFER_NAME,offername.getText().toString());
                                    params.put(Release_constants.NO_OF_ROOMS,no_rooms.getText().toString());
                                    params.put(Release_constants.ROOM_TYPE,registered_room_type.get(index));
                                    params.put(AddroomConstants1.PRICE_MRP,registered_room_priceMRP.get(index));
                                    params.put(Release_constants.WOW_PRICE,sales_price.getText().toString());
                                    params.put(Release_constants.CHECK_IN,checkinstring);
                                    Log.e("checkin",checkinstring);
                                    Log.e("checkout",checkoutstring);


                                    params.put(Release_constants.CHECK_OUT,checkoutstring);
                                    params.put(Release_constants.SCHEDULE,scheduledatestring);
                                    params.put(Release_constants.EXPIRY,expireStringDate);

                                    Log.e("params",params.toString());
                                    return params;
                                }
                            };
                            request.setRetryPolicy(new DefaultRetryPolicy(
                                    10000,
                                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            requestQueue.add(request);
                            // Toast.makeText(getContext(), "Congratulations! Offers relaeased.", Toast.LENGTH_LONG).show();
                            alert.dismiss();
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            alert.dismiss();
                            myadapter.clearAll();
                        }
                    });

                    alert = builder.create();
                    alert.show();
                }
            }
        });

        return v;
    }

    private boolean check_credit(final String id) {
        final String[] returnresponse = new String[1];
        StringRequest request = new StringRequest(Request.Method.POST, Constants.CHECK_CREDIT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("response",response);
                check_credit_dialog.dismiss();
                try {
                    JSONObject respons = new JSONObject(response);
                    returnresponse[0] = respons.getString("res");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                check_credit_dialog.dismiss();
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put(Constants.ID,id);
                return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(request);


        return false?returnresponse.equals("false"):true;

    }

    private void hideSoftKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            //focusOnView(policy);
        }


    }

    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }


}



