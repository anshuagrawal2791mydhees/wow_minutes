package com.mydhees.wow.partner.drawer_fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

//import com.example.anshu.wow_minutes.MyAdapter;
import com.mydhees.wow.partner.leads_dashboard_tabs.Chats;
import com.mydhees.wow.partner.leads_dashboard_tabs.Converted;
import com.mydhees.wow.partner.leads_dashboard_tabs.Released;
import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.leads_dashboard_tabs.Wishes;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LeadsDashboard.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LeadsDashboard#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LeadsDashboard extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static int int_items = 4 ;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public LeadsDashboard() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter one.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LeadsDashboard.
     */
    // TODO: Rename and change types and number of parameters
    public static LeadsDashboard newInstance(String param1, String param2) {
        LeadsDashboard fragment = new LeadsDashboard();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View x =  inflater.inflate(R.layout.fragment_leads_dashboard, null);

        tabLayout = (TabLayout) x.findViewById(R.id.tabs);
        viewPager = (ViewPager) x.findViewById(R.id.viewpager);


        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));

        /**
         * Now , this is a workaround ,
         * The setupWithViewPager doesn't works without the runnable .
         * Maybe a Support Library Bug .
         */


        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
                tabLayout.getTabAt(0).setIcon(R.drawable.icon_released);
                tabLayout.getTabAt(1).setIcon(R.drawable.icon_wishes);
                tabLayout.getTabAt(2).setIcon(R.drawable.icon_converted);
                tabLayout.getTabAt(3).setIcon(R.drawable.icon_chats);
            }
        });


        return x;
    }


    class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment with respect to Position .
         */

        @Override
        public Fragment getItem(int position)
        {
            switch (position){
                case 0 : return new Released();
                case 1 : return new Wishes();
                case 2 : return new Converted();
                case 3 : return new Chats();
            }
            return null;
        }

        @Override
        public int getCount() {

            return 4;

        }

        /**
         * This method returns the title of the tab according to the position.
         */

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position){
                case 0 :
                    return "Released";
                case 1 :
                    return "Wishes";
                case 2 :
                    return "Converted";
                case 3 :
                    return "Chats";
            }
            return null;
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
