package com.mydhees.wow.partner.objects;

import java.util.Date;
/**
 * Created by Jashan Kochar on 6/23/2016.
 */
public class offer {

    int price;
    int roomno;
    int room_type;
    String hotel_name,offer,expire,roomtype;
    Date date_checkin,expire_date;
    Date date_check_out,schedule;
    String hotelId,roomId,status,offerId;




    public String getHotelId() {
        return hotelId;
    }

    public String getRoomId() {
        return roomId;
    }

    String wowprice;

    public String getWowprice() {
        return wowprice;
    }

    public String getRoomtype() {
        return roomtype;
    }



    public offer(String hname, String offer, int roomno, int price, String wowprice, String roomtype, Date date_checkin, Date date_check_out, Date schedule,Date expire_date, String hid, String rid, String status,String offerId) {
        this.roomno = roomno;
        this.hotel_name = hname;
        this.offer=offer;
        this.price = price;

        this.roomtype = roomtype;
        this.date_checkin = date_checkin;
        this.date_check_out = date_check_out;
        this.schedule = schedule;
        this.wowprice=wowprice;
        this.hotelId=hid;
        this.roomId=rid;
        this.offerId=offerId;
        if(status.equals("false")){
            if(new Date().after(schedule)&& new Date().before(expire_date))
                this.status="LIVE";
            else if (new Date().before(schedule))
                this.status = "Placed";
            else
                this.status = "Expired";



        }

        else
            this.status = "Deleted";

    }
    public offer(String hname,String offer,int roomno, int price, String roomtype, Date date_checkin, Date date_check_out,Date schedule,String expire,Date expire_date) {
        this.roomno = roomno;
        this.hotel_name = hname;
        this.offer=offer;
        this.price = price;
        this.expire_date = expire_date;
        this.roomtype = roomtype;
        this.date_checkin = date_checkin;
        this.date_check_out = date_check_out;
        this.expire = expire;
        this.schedule = schedule;

    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public Date getDate_check_out() {
        return date_check_out;
    }
    public String getExpire() {
        return expire;
    }
    public Date getSchedule() {
        return schedule;
    }
    public Date getExpire_date() {
        return expire_date;
    }

    public void setDate_check_out(Date date_check_out) {
        this.date_check_out = date_check_out;
    }

    public Date getDate_checkin() {
        return date_checkin;
    }

    public void setDate_checkin(Date date_checkin) {
        this.date_checkin = date_checkin;
    }


    public int getPrice() {
        return price;
    }
    public String getHotel_name() {
        return hotel_name;
    }
    public String getOffer_name() {
        return offer;
    }

    public int getRoomno() {
        return roomno;
    }

    public int getRoom_type() {
        return room_type;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setRoomno(int roomno) {
        this.roomno = roomno;
    }

    public void setRoom_type(int room_type) {
        this.room_type = room_type;
    }

}
