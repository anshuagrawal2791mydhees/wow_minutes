package com.mydhees.wow.partner.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.ReReleaseOffer;
import com.mydhees.wow.partner.VolleySingleton;
import com.mydhees.wow.partner.allConstants.Constants;
import com.mydhees.wow.partner.allConstants.Release_constants;
import com.mydhees.wow.partner.objects.offer;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class ReleaseOfferAdapterJ extends RecyclerView.Adapter<ReleaseOfferAdapterJ.myviewholder> {
    Context context;
    LayoutInflater inflater;
    ArrayList<offer> offers = new ArrayList<>();
    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    DateFormat timeFormat = new SimpleDateFormat("HH:mm");
     AlertDialog dialog2;


    public ReleaseOfferAdapterJ(Context context) {
        this.context=context;
        inflater=LayoutInflater.from(context);
    }

    public void addoffer1(offer recievedoffer)
    {
        offers.add(0,recievedoffer);
        notifyItemInserted(0);
    }
    public void addAll(ArrayList<offer> offerslist){
        offers.clear();
        offers.addAll(offerslist);
        notifyDataSetChanged();
    }
    public ArrayList<offer> getofferlist()
    {
        return offers;
    }
    public void clearAll()
    {
        offers.clear();
    }

    @Override
    public myviewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.released_row,parent,false);
        myviewholder viewholder = new myviewholder(v);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final myviewholder holder, final int position) {
        String[] rooms ={"Luxury","King","Single","Double"};
        offer current_offer = offers.get(position);

        holder.no_rooms_tv.setText(current_offer.getRoomno()+"");
        holder.sales_price_tv.setText(current_offer.getWowprice()+"");
        holder.max_price.setText(current_offer.getPrice()+"");
        holder.offername.setText(current_offer.getOffer_name()+"");
        holder.hname.setText(current_offer.getHotel_name()+"");
       // holder.check_in_time_tv.setText(timeFormat.format(current_offer.getDate_checkin()));
        holder.check_in_tv.setText(dateFormat.format(current_offer.getDate_checkin()));
      //  holder.check_out_time_tv.setText(timeFormat.format(current_offer.getDate_check_out()));
        holder.check_out_tv.setText(dateFormat.format(current_offer.getDate_check_out()));
        holder.schedule.setText(dateFormat.format(current_offer.getSchedule()));
        holder.stime.setText(timeFormat.format(current_offer.getSchedule()));
        holder.room_type_tv.setText(current_offer.getRoomtype());
        holder.status.setText(current_offer.getStatus());

        if(current_offer.getStatus().equals("Deleted"))
            holder.delete.setVisibility(View.GONE);

        holder.release.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent start= new Intent(context, ReReleaseOffer.class);
                start.putExtra("roomNo",offers.get(position).getRoomno()+"");
                start.putExtra("wowPrice",offers.get(position).getWowprice());
                start.putExtra("maxPrice",offers.get(position).getPrice()+"");
                start.putExtra("offerName",offers.get(position).getOffer_name());
                start.putExtra("hotelName",offers.get(position).getHotel_name());
                start.putExtra("roomType",offers.get(position).getRoomtype());
                start.putExtra("hid",offers.get(position).getHotelId());
                start.putExtra("rid",offers.get(position).getRoomId());


                context.startActivity(start);
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Are you sure want to delete ?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog2.dismiss();
                        final ProgressDialog dialog3 = new ProgressDialog(context);
                        dialog3.show();
                        VolleySingleton volleySingleton = VolleySingleton.getinstance();
                        RequestQueue requestQueue = volleySingleton.getrequestqueue();
                        StringRequest request  = new StringRequest(Request.Method.POST, Constants.DELETE_OFFER,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        dialog3.dismiss();
                                        try {
                                            JSONObject resp = new JSONObject(response);
                                            if(resp.getString("res").equals("true")){
                                                Toast.makeText(context,"Deleted Successfully",Toast.LENGTH_LONG).show();
                                                holder.delete.setVisibility(View.GONE);
                                                holder.status.setText("Deleted");
                                            }

                                            else
                                                Toast.makeText(context,"Error",Toast.LENGTH_LONG).show();

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        dialog3.dismiss();
                                        Toast.makeText(context,"Error",Toast.LENGTH_LONG).show();

                                    }
                                }){
                            @Override
                            protected Map<String,String> getParams(){
                                Map<String,String> params = new HashMap<String, String>();
                                params.put(Release_constants.OFFER_ID,offers.get(position).getOfferId());
                                return params;
                            }

                        };
                        request.setRetryPolicy(new DefaultRetryPolicy(
                                100000,
                                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                        requestQueue.add(request);

                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog2.dismiss();
                    }
                });
                dialog2 = builder.create();
                dialog2.show();
            }
        });




    }



    @Override
    public int getItemCount() {
        return offers.size();
    }

    static class myviewholder extends RecyclerView.ViewHolder
    {
       TextView room_type_tv,no_rooms_tv,sales_price_tv,hname,max_price;
        TextView check_in_tv,check_in_time_tv,offername;
        TextView check_out_tv,check_out_time_tv,schedule,stime,expirytime,exp,status;
        ImageView release,delete;
        // Button delete;


        public myviewholder(View itemView) {
            super(itemView);
            room_type_tv = (TextView)itemView.findViewById(R.id.room_type);
            offername = (TextView)itemView.findViewById(R.id.offer_name);
            expirytime = (EditText)itemView.findViewById(R.id.expireTime_tv);
            exp = (EditText)itemView.findViewById(R.id.expireTime2_tv);
            hname = (TextView)itemView.findViewById(R.id.rhotel_name);
            no_rooms_tv = (TextView)itemView.findViewById(R.id.no_rooms2);
            max_price = (TextView)itemView.findViewById(R.id.maximum_price);
            sales_price_tv = (TextView)itemView.findViewById(R.id.wow_price);
            check_in_tv =(TextView)itemView.findViewById(R.id.checkin_date);
           // check_in_time_tv = (EditText)itemView.findViewById(R.id.check_in_time_tv);
            check_out_tv = (TextView)itemView.findViewById(R.id.checkout_date);
           // check_out_time_tv = (EditText)itemView.findViewById(R.id.check_out_time_tv);
            schedule = (TextView)itemView.findViewById(R.id.schedule_date);
            stime = (TextView)itemView.findViewById(R.id.schedule_time);
            release= (ImageView) itemView.findViewById(R.id.re);
            delete = (ImageView)itemView.findViewById(R.id.delete2);
            status = (TextView)itemView.findViewById(R.id.status2);
            //  delete = (Button)itemView.findViewById(R.id.delete);
        }
    }
}
