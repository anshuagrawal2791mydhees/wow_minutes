package com.mydhees.wow.partner.credits_tab;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.VolleySingleton;
import com.mydhees.wow.partner.adapters.PurchasedPointsadapter;
import com.mydhees.wow.partner.allConstants.Constants;
import com.mydhees.wow.partner.allConstants.HotelRegistrationConstants;
import com.mydhees.wow.partner.objects.purchasedHistory;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by jashan on 06-06-2016.
 */
public class Purchased extends Fragment {
RecyclerView pr;
PurchasedPointsadapter ppadapter;

    DateFormat parser =new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    SharedPreferences profile;
    SharedPreferences.Editor editor;

    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    public Purchased() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_purchased, container, false);
        profile = getActivity().getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE, Context.MODE_PRIVATE);
        editor= profile.edit();
       // dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        volleySingleton=VolleySingleton.getinstance();
        requestQueue = volleySingleton.getrequestqueue();
    pr= (RecyclerView) v.findViewById(R.id.purchased_recycler);
ppadapter=new PurchasedPointsadapter(getContext());
       pr.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        pr.setAdapter(ppadapter);
        pr.setVisibility(View.VISIBLE);
        parser.setTimeZone(TimeZone.getTimeZone("IST"));

        try {
            purchasedHistory();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    return v;
    }


    private void purchasedHistory() throws JSONException {
        final ProgressDialog dialog = new ProgressDialog(getContext());
//        dialog.show();
        dialog.setTitle("Fetching details...");
        dialog.setMessage("Fetching details...");
        profile = getActivity().getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE, Context.MODE_PRIVATE);
        editor= profile.edit();
        StringRequest request = new StringRequest(Request.Method.POST, Constants.GET_ORDERS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respons) {
//                        dialog.dismiss();
                        Log.e("response",respons.toString());
                        JSONObject response = null;
                        try {
                            response = new JSONObject(respons);
                            int count=0;
                            if(response.getJSONArray("list").length()>0) {
                                for (int i = 0; i < response.getJSONArray("list").length(); i++) {
//                                    Log.e("customers",response.getJSONArray("list").getJSONObject(i).getString("customer_id"));
//                                    Log.e("id",profile.getString(Constants.ID,"default"));
                                    // Date txn = parser.parse(response.getJSONArray("list").getJSONObject(i).getJSONObject("payment_gateway_response").getString("created"));
                                    if(response.getJSONArray("list").getJSONObject(i).getString("customer_id").equals(profile.getString(Constants.ID,"default"))) {
                                        count++;
                                        Date tx1 = parser.parse(response.getJSONArray("list").getJSONObject(i).getJSONObject("payment_gateway_response").getString("created"));
                                        ppadapter.addoffer(new purchasedHistory(tx1, response.getJSONArray("list").getJSONObject(i).getString("amount"), response.getJSONArray("list").getJSONObject(i).getString("order_id"),
                                                response.getJSONArray("list").getJSONObject(i).getString("txn_id"), response.getJSONArray("list").getJSONObject(i).getString("status"), response.getJSONArray("list").getJSONObject(i).getJSONObject("card").getString("card_type") + "/" + response.getJSONArray("list").getJSONObject(i).getJSONObject("card").getString("card_brand")));
                                    }
                                }
                            }
                            else{
                                Toast.makeText(getContext(), "No Transactions!!!", Toast.LENGTH_SHORT).show();
                            }
                            if(count==0)
                                Toast.makeText(getContext(), "No Transactions!!!", Toast.LENGTH_SHORT).show();

                       Log.e("response",response.toString());

                        } catch (JSONException e) {
                            Log.e("error",e.toString());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                dialog.dismiss();
                Toast.makeText(getContext(),"Internet Problem",Toast.LENGTH_LONG).show();
                Log.e("vollley error",error.toString());
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                //replaced ownerId with customerId
                params.put("customerId",profile.getString(Constants.ID,"default"));

                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }

}
