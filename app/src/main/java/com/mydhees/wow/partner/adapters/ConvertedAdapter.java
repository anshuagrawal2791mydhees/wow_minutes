package com.mydhees.wow.partner.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.objects.convertedObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;


public class ConvertedAdapter extends RecyclerView.Adapter<ConvertedAdapter.myviewholder> {
    Context context;
    LayoutInflater inflater;
    ArrayList<convertedObject> convert = new ArrayList<>();
    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");



    public ConvertedAdapter(Context context) {
        this.context=context;
        inflater=LayoutInflater.from(context);
    }

    public void addoffer(convertedObject wow_converted)
    {
        convert.add(0,wow_converted);
        notifyItemInserted(convert.size());
    }
    public ArrayList<convertedObject> getconvertedlist()
    {
        return convert;
    }
    public void clearAll()
    {
        convert.clear();
    }

    @Override
    public myviewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.converted_row,parent,false);
        myviewholder viewholder = new myviewholder(v);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(myviewholder holder, final int position) {

        convertedObject lead_convert = convert.get(position);

        holder.offerName.setText(lead_convert.getOfferName());
        holder.name.setText(lead_convert.getName());
        holder.phone.setText(lead_convert.getPhone());
        holder.email.setText(lead_convert.getEmail());
        holder.wowPrice.setText(lead_convert.getWowPrice());
        holder.bookingDate.setText(dateFormat.format(lead_convert.getBookingDate()));
        holder.checkin.setText(dateFormat.format(lead_convert.getCheckIn()));
        holder.checkout.setText(dateFormat.format(lead_convert.getCheckOut()));
        holder.status.setText(lead_convert.getStatus());
        holder.hotelName.setText(lead_convert.getHotelName());
        holder.bookingId.setText(lead_convert.getBookingId());
        holder.roomType.setText(lead_convert.getRoomType());



    }

    @Override
    public int getItemCount() {
        return convert.size();
    }

    static class myviewholder extends RecyclerView.ViewHolder
    {
        TextView offerName,name,phone,email,wowPrice,checkin,checkout,status,hotelName,bookingId,roomType,bookingDate;



        public myviewholder(View itemView) {
            super(itemView);

            offerName = (TextView)itemView.findViewById(R.id.offer_name_con);
            name = (TextView)itemView.findViewById(R.id.name_con);
            email = (TextView)itemView.findViewById(R.id.email_con);
            phone = (TextView)itemView.findViewById(R.id.phone_con);
           wowPrice = (TextView)itemView.findViewById(R.id.wow_price_con);
            checkin = (TextView)itemView.findViewById(R.id.checkin_date_con);
           checkout = (TextView)itemView.findViewById(R.id.checkout_date_con);
            status = (TextView)itemView.findViewById(R.id.status_con);
            hotelName = (TextView)itemView.findViewById(R.id.hotel_name_con);
           bookingDate = (TextView)itemView.findViewById(R.id.booking_date_con);
            bookingId = (TextView)itemView.findViewById(R.id.booking_id_con);
            roomType = (TextView)itemView.findViewById(R.id.room_type_con);

        }
    }
}
