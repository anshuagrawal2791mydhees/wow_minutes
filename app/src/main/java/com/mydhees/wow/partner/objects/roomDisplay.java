package com.mydhees.wow.partner.objects;

/**
 * Created by Jashan Kochar on 6/27/2016.
 */
public class roomDisplay {

    String roomType,image_link,roomnos,rid;

    public String getRoomType() {
        return roomType;
    }
    public String getRid() {
        return rid;
    }


    public String getRoomnos() {
        return roomnos;
    }


    public String getImage_link() {

        return image_link;
    }
    public roomDisplay(String roomnos, String roomType, String rid, String image_link) {
        this.roomnos = roomnos;
        this.roomType = roomType;
        this.rid= rid;
        this.image_link= image_link;
    }
}
