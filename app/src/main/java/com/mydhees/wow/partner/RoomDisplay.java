package com.mydhees.wow.partner;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mydhees.wow.partner.adapters.ImageDisplay;
import com.mydhees.wow.partner.allConstants.AddroomConstants1;
import com.mydhees.wow.partner.allConstants.Constants;
import com.mydhees.wow.partner.allConstants.HotelRegistrationConstants;
import com.mydhees.wow.partner.allConstants.Release_constants;
import com.mydhees.wow.partner.objects.image;
import com.mydhees.wow.partner.objects.registeredHotels;
import com.mydhees.wow.partner.utils.task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import okhttp3.Response;

public class RoomDisplay extends AppCompatActivity {
    EditText hotelName,room_type,no_of_rooms,room_price,adults,children;
    String rid;
    TextView update;

    Button a,b,c,d;
    Boolean ac = false;
    task register_rooms;
    HashMap<String,String> data = new HashMap<>();
    ArrayList<image> images_uris = new ArrayList<>();

    Boolean  edit_me=true,edit_me2=false,update_me=false;
    Button edit,delete,facilities_edit;
    SharedPreferences profile;
    SharedPreferences.Editor editor;
    ImageView wifi,food,parking,pool,tv,gym,business_services,coffeeshop,frontdesk,laundary_services,outdoor,health_spa,room_service,spa,travel_asst;
    boolean isPressed=true;
    boolean isPressed2=true;
    String ac_nonac="AC";
    ImageDisplay imgd;
    RecyclerView recycler;
    ImageView ac_btn,non_ac;
    ArrayList<JSONObject> registered_rooms_jsons = new ArrayList<>();
    ArrayList<JSONObject> registered_rooms_jsons2 = new ArrayList<>();
    HashSet<String> registered_rooms_strings = new HashSet<>();
    ArrayList<Integer> facilites_images = new ArrayList<>();

    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    int k=0;
    LinearLayout layout;
    int facilites[] = new int[]{  R.drawable.reg_hotel_gym, R.drawable.ic_live_tv_black_24dp,R.drawable.ic_restaurant_menu_black_24dp,R.drawable.ic_wifi_black_24dp1,};
    String[] extra_facilities= new String []{"wifi","food","tv","gym"};

    boolean[] extra_facilities_selected = new boolean[]{false,false,false,false};
    private Toolbar toolbar;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_display);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.primary_dark));
        }
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        toolbar = (Toolbar)findViewById(R.id.toolbar_room);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_back_white_icon);



        profile = getSharedPreferences(
                Constants.PROFILE_PREFERENCE_FILE, Context.MODE_PRIVATE);
        editor = profile.edit();
        volleySingleton= VolleySingleton.getinstance();
        requestQueue = volleySingleton.getrequestqueue();
        layout = (LinearLayout)findViewById(R.id.facility_room_dis);
        adults= (EditText) findViewById(R.id.adults);
        children= (EditText) findViewById(R.id.child1);
       // edit = (Button)findViewById(R.id.edit_dis_room);
       facilities_edit = (Button)findViewById(R.id.edit__fac_dis_room);
       // delete = (Button)findViewById(R.id.delete_dis_room);
        update= (TextView) findViewById(R.id.update_info_room);

        ac_btn = (ImageView) findViewById(R.id.ac_btn_dis);
        non_ac = (ImageView)findViewById(R.id.nonac_btn_dis);
        a=(Button)findViewById(R.id.imageButton123);            //plus
        b=(Button)findViewById(R.id.imageButton22);           //mminus
        c=(Button)findViewById(R.id.imageButton12);           //plus
        d=(Button)findViewById(R.id.imageButton21);
        hotelName = (EditText)findViewById(R.id.disr_hoteln);
        room_type = (EditText)findViewById(R.id.dis_rtype1);
        room_price = (EditText)findViewById(R.id.dis_price);
        no_of_rooms = (EditText)findViewById(R.id.dis_room_no_addroom);
        recycler = (RecyclerView) findViewById(R.id.rv21);
        recycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        imgd=new com.mydhees.wow.partner.adapters.ImageDisplay(this);
        recycler.setAdapter(imgd);
        Intent intent= getIntent();
        rid= intent.getStringExtra("rid");


        try {
            getRomdetails();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        hotelName.setEnabled(false);
room_price.setEnabled(false);
room_type.setEnabled(false);
no_of_rooms.setEnabled(false);
        a.setEnabled(false);
        b.setEnabled(false);
        c.setEnabled(false);
        d.setEnabled(false);
        facilities_edit.setEnabled(false);
adults.setEnabled(false);
children.setEnabled(false);
        ac_btn.setEnabled(false);
        non_ac.setEnabled(false);


        a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count= Integer.parseInt(adults.getText().toString());
                int count1 = count + 1;
                String a= Integer.toString(count1);
                adults.setText(a);


            }
        });

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count= Integer.parseInt(adults.getText().toString());
                if(count==0){

                    Toast.makeText(RoomDisplay.this, "Already 0!!! Can't be less.", Toast.LENGTH_SHORT).show();

                }
                else {
                    int count1 = count - 1;
                    String a= Integer.toString(count1);
                    adults.setText(a);
                }
            }
        });

        c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count= Integer.parseInt(children.getText().toString());

                int count1=count+1;
                String a= Integer.toString(count1);
                children.setText(a);

            }
        });

        d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count= Integer.parseInt(children.getText().toString());
                if(count==0){

                    Toast.makeText(RoomDisplay.this, "Already 0!!! Can't be less.", Toast.LENGTH_SHORT).show();

                }
                else {
                    int count1 = count - 1;
                    String a= Integer.toString(count1);
                    children.setText(a);

                }
            }
        });
        facilities_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(RoomDisplay.this);
                builder.setMultiChoiceItems(extra_facilities,extra_facilities_selected,new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        if (isChecked) {
                            // If the user checked the item, add it to the selected items
                            extra_facilities_selected[which]=true;

                        } else if(extra_facilities_selected[which]==true) {
                            // Else, if the item is already in the array, remove it
                            extra_facilities_selected[which]=false;
                        }
                    }
                }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int hastrue = 0;
                        layout.removeAllViews();
                        for(int i=0;i<extra_facilities_selected.length;i++)
                        {
                            if(extra_facilities_selected[i]==true)
                            {
                                hastrue=1;
                               layout.setVisibility(View.VISIBLE);
                                ImageView image = new ImageView(RoomDisplay.this);
                                int id= R.drawable.ic_launcher;
                                if(i==0)
                                    id = R.drawable.ic_wifi_black_24dp1;
                                else if(i==1)
                                    id = R.drawable.ic_restaurant_menu_black_24dp;
                                else if(i==2)
                                    id = R.drawable.ic_live_tv_black_24dp;
                                else if(i==3)
                                    id = R.drawable.reg_hotel_gym;

                                image.setImageResource(id);
                               layout.addView(image);
                            }
                        }
                        if(hastrue==0)
                            layout.setVisibility(View.GONE);
                        dialog.dismiss();
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
            }
        });


        ac_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isPressed==true){
                    ImageView view = (ImageView)v;
                    view.setColorFilter(Color.parseColor("#009688"));
                    non_ac.setColorFilter(Color.GRAY);
                    isPressed=false;
                    ac_nonac="AC";
                    isPressed2=true;


                }

                else{
                    ImageView view = (ImageView)v;
                    view.setColorFilter(Color.GRAY);
                    ac_nonac="AC";
                    isPressed=true;
                }


            }
        });


        non_ac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isPressed2==true){
                    ImageView view = (ImageView)v;
                    view.setColorFilter(Color.parseColor("#009688"));
                    ac_btn.setColorFilter(Color.GRAY);
                    isPressed2=false;
                    ac_nonac="Non-AC";
                    isPressed=true;

                }

                else{
                    ImageView view = (ImageView)v;
                    view.setColorFilter(Color.GRAY);
                    ac_nonac="AC";
                    isPressed2=true;

                }

            }
        });

        if(ac_nonac.equals("AC"))
            ac = true;
        else
            ac = false;

        

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (update_me) {


                    // data.put(AddroomConstants1.HOTEL_ID,registered_hotels_ids.get(hotel_names_spin.getSelectedItemPosition()));
                    data.put(Release_constants.ROOM_ID, rid);
                    data.put(HotelRegistrationConstants.OWNER_ID, profile.getString(Constants.ID, "default"));
                    data.put(AddroomConstants1.HOTEL_NAME, hotelName.getText().toString());
                    data.put(AddroomConstants1.NO_OF_ROOMS, no_of_rooms.getText().toString());

                    String[] facilites_keys = new String[]{HotelRegistrationConstants.HAS_WIFI,
                            HotelRegistrationConstants.HAS_DINING,
                            HotelRegistrationConstants.HAS_TV,
                            HotelRegistrationConstants.HAS_GYM,
                    };

                    final JSONObject facilitesJSON=new JSONObject();
                    for(int i=0;i<4;i++)
                    {
                        try {
                            facilitesJSON.put(facilites_keys[i],extra_facilities_selected[i]);
                        } catch (JSONException e) {
                            Log.e("error",e.toString());
                        }
                    }

                    data.put(AddroomConstants1.FACILITIES, facilitesJSON.toString());
                    data.put(AddroomConstants1.ROOM_TYPE, room_type.getText().toString());
                    data.put(AddroomConstants1.PRICE_MRP, room_price.getText().toString());
                    data.put(AddroomConstants1.AC, ac.toString());
                    data.put(AddroomConstants1.ADULTS, adults.getText().toString());
                    data.put(AddroomConstants1.CHILDREN, children.getText().toString());
                    data.put(Constants.EMAIL, profile.getString(Constants.EMAIL, "default"));

                    final ProgressDialog dialog2 = new ProgressDialog(RoomDisplay.this);
                    dialog2.show();
                    register_rooms = new task(RoomDisplay.this, images_uris, Constants.ROOM_UPDATE, data) {
                        @Override
                        protected void onPostExecute(Response response) {
                            dialog2.dismiss();

                            Toast.makeText(RoomDisplay.this, "Room Details Updated", Toast.LENGTH_LONG).show();

//
                            HashSet<String> registered_rooms = new HashSet<String>();

                            try {
                                JSONObject resp = new JSONObject(response.body().string());
                                Log.e("respJSOn", resp.toString());
                                registered_rooms.add(resp.toString());

                            } catch (JSONException e) {
                                Log.e("err", e.toString());
                            } catch (IOException e) {
                                Log.e("err", e.toString());
                            }

                            if(profile.getStringSet(Constants.REGISTERED_ROOMS, new HashSet<String>()).size()>0){
                            HashSet<String> h = new HashSet<>(profile.getStringSet(Constants.REGISTERED_ROOMS, null));
                            Iterator iterator2 = h.iterator();
                            Log.e("h", h.toString());

                            while (iterator2.hasNext()) {

                                try {
                                    String s = iterator2.next().toString();
                                    JSONObject j = new JSONObject(s);
                                    if (j.getString("roomId").equals(rid))
                                        iterator2.remove();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                            registered_rooms.addAll(h);
                            Iterator iterator1 = registered_rooms.iterator();
                            while (iterator1.hasNext()) {
                                try {
                                    registered_rooms_jsons.add(new JSONObject(iterator1.next().toString()));
                                } catch (JSONException e) {
                                    Log.e("err", e.toString());
                                }
                            }

                            for (int j = 0; j < registered_rooms_jsons.size(); j++) {
                                try {

                                    if (rid.equals(registered_rooms_jsons.get(j).getString(Release_constants.ROOM_ID))) {
                                        editor.putStringSet(Constants.REGISTERED_ROOMS, registered_rooms);

                                        editor.commit();


                                    }


                                } catch (JSONException k) {
                                }
                            }
                        }
                        else

                        {
                            Log.e("storing", "first");
                        }

                        Log.e("ROOMS",registered_rooms.toString());

                        finish();
                            Intent it=new Intent(RoomDisplay.this,Home.class);
                            startActivity(it);

                        super.

                        onPostExecute(response);
                    }

                } ;


                register_rooms.execute();



                }
            }
        });




    }




    private void getRomdetails() throws JSONException {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.show();
        dialog.setTitle("Fetching details...");
        dialog.setMessage("Fetching details...");
        StringRequest request = new StringRequest(Request.Method.POST, Constants.GET_ROOM,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String respons) {
                        dialog.dismiss();
                        Log.e("response room",respons.toString());
                        JSONObject response = null;
                        HashSet<String> rooms1 = new HashSet<String>();
                        try {
                            response = new JSONObject(respons);
                            rooms1.add(response.toString());
                            if(profile.getStringSet(Constants.REGISTERED_ROOMS, new HashSet<String>()).size()>0){
                                HashSet<String> h2 = new HashSet<>(profile.getStringSet(Constants.REGISTERED_ROOMS, null));
                                Iterator iterator22 = h2.iterator();
                                Log.e("h", h2.toString());

                                while (iterator22.hasNext()) {

                                    try {
                                        String s2 = iterator22.next().toString();
                                        JSONObject j = new JSONObject(s2);
                                        if (j.getString("roomId").equals(rid))
                                            iterator22.remove();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                                Log.e("h log",h2.toString());
                                Log.e("room log",rooms1.toString());
                                rooms1.addAll(h2);
                                Iterator iterator12 = rooms1.iterator();
                                while (iterator12.hasNext()) {
                                    try {
                                        registered_rooms_jsons2.add(new JSONObject(iterator12.next().toString()));
                                    } catch (JSONException e) {
                                        Log.e("err", e.toString());
                                    }
                                }

                                for (int j = 0; j < registered_rooms_jsons2.size(); j++) {
                                    try {

                                        if (rid.equals(registered_rooms_jsons2.get(j).getString(Release_constants.ROOM_ID))) {
                                            editor.putStringSet(Constants.REGISTERED_ROOMS, rooms1);

                                            editor.commit();


                                        }


                                    } catch (JSONException k) {
                                    }
                                }
                            }
                            hotelName.setText(response.getString(Release_constants.HOTEL_NAME));
                            room_type.setText(response.getString(Release_constants.ROOM_TYPE));
                            room_price.setText(response.getString(Release_constants.PRICE_MRP));
                            no_of_rooms.setText(response.getString(Release_constants.NO_OF_ROOMS));
                            adults.setText(response.getString(AddroomConstants1.ADULTS));
                            children.setText(response.getString(AddroomConstants1.CHILDREN));

                            JSONObject json = response.getJSONObject(AddroomConstants1.FACILITIES);

                        Iterator<String> iter = json.keys();
                        int p=3;
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                                40,
                                40);
                        lp.setMargins(5,5,5,5);
                        while(iter.hasNext()) {
                            String key = iter.next();
                            extra_facilities_selected[p]=json.getBoolean(key);
                            if(p>=0)
                            p--;
                              else
                                p=3;
                            try {
                                if(json.getBoolean(key))
                                {

                                    ImageView image = new ImageView(RoomDisplay.this);
                                    image.setMaxHeight(50);
                                    image.setMaxWidth(50);
                                    image.setLayoutParams(lp);
                                    image.setImageDrawable(ContextCompat.getDrawable(RoomDisplay.this, facilites[k]));
//                                      image.setImageDrawable(getResources().(facilites[i]);
                                    layout.addView(image);
                                    facilites_images.add(facilites[k]);
                                }
                                k++;
                            } catch (JSONException e) {
                                // Something went wrong!
                            }
                        }
                        Log.e("images",facilites_images.toString());


                            if(response.getString(AddroomConstants1.AC).equals("true")){

                            ac_btn.setColorFilter(Color.parseColor("#009688"));
                            non_ac.setColorFilter(Color.GRAY);
                            isPressed=false;
                            ac_nonac="AC";
                            isPressed2=true;

                        }else{

                            non_ac.setColorFilter(Color.parseColor("#009688"));
                            ac_btn.setColorFilter(Color.GRAY);
                            isPressed2=false;
                            ac_nonac="Non-AC";
                            isPressed=true;
                        }


                            JSONArray image=response.getJSONObject("imagesS3").getJSONArray("name");
                            if(image.length()>0){
                                imgd.clearAll();
                                for(int i=0;i<image.length();i++) {

                                    imgd.addImage(new registeredHotels(image.get(i).toString()));
                                }
                            }
                            else{
                                imgd.addImage(new registeredHotels(""));
                            }
                        } catch (JSONException e) {
                            Log.e("error",e.toString());
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(RoomDisplay.this,"Internet Problem",Toast.LENGTH_LONG).show();
                Log.e("vollley error",error.toString());
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put(Release_constants.ROOM_ID,rid);

                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.room_display_appbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.edit_toolroom) {

                    if(edit_me.equals(true)) {
                        edit_me = false;
                        edit_me2 = true;
                        hotelName.setEnabled(false);
                        room_price.setEnabled(true);
                        room_type.setEnabled(true);
                        no_of_rooms.setEnabled(true);
                        facilities_edit.setEnabled(true);
                        a.setEnabled(true);
                        b.setEnabled(true);
                        c.setEnabled(true);
                        d.setEnabled(true);
                        adults.setEnabled(true);
                        children.setEnabled(true);
                        update_me=true;
                        update.setVisibility(View.VISIBLE);

//                        update.setText("Update Details");
                        ac_btn.setEnabled(true);
                        non_ac.setEnabled(true);
//                        update.setBackgroundColor(Color.GRAY);

                    }else{
                        hotelName.setEnabled(false);
                        room_price.setEnabled(false);
                        room_type.setEnabled(false);
                        no_of_rooms.setEnabled(false);
                        a.setEnabled(false);
                        b.setEnabled(false);
                        c.setEnabled(false);
                        d.setEnabled(false);
                        facilities_edit.setEnabled(false);
                        adults.setEnabled(false);
                        children.setEnabled(false);
                        edit_me=true;
                        edit_me2=false;
                        update.setVisibility(View.GONE);
                       // update.setText("");
                       // update.setBackgroundColor(Color.WHITE);
                        ac_btn.setEnabled(false);
                        non_ac.setEnabled(false);
                    }


            return true;
        }
        if (id == R.id.delete_toolbroom) {


            return true;
        }

        //for backbutton
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:

        }


        return super.onOptionsItemSelected(item);


    }
}
