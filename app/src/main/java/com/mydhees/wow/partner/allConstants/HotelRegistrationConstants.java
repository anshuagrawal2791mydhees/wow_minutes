package com.mydhees.wow.partner.allConstants;

/**
 * Created by Jashan Kochar on 6/17/2016.
 */
public class HotelRegistrationConstants {

    public static final String OWNER_ID="ownerId";
    public static final String HOTEL_NAME="hotelName";
    public static final String HOTEL_TYPE="hotelType";
    public static final String NO_OF_ROOMS="noOfRooms";
    public static final String PHONE_NUMBER="phoneNumber";
    public static final String MOBILE_NUMBER="mobileNumber";
    public static final String ADDRESS_LINE1 = "address_line1";
    public static final String ADDRESS_LINE2 = "address_line2";
    public static final String ADDRESS = "address";
    public static final String PIN = "pin";
    public static final String CITY = "city";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String FACILITIES = "facilities";
    public static final String HAS_WIFI = "hasWifi";
    public static final String HAS_GYM = "hasGym";
    public static final String HAS_DINING = "hasDining";
    public static final String HAS_TV = "hasTv";
    public static final String HAS_BUSINESS_SERVICE = "hasBusinessService";
    public static final String HAS_COFFEE_SHOP = "hasCoffeeShop";
    public static final String HAS_FRONT_DESK = "hasFrontDesk";
    public static final String HAS_SPA = "hasSpa";
    public static final String HAS_LAUNDRY = "hasLaundry";
    public static final String HAS_OUT_DOOR_GAMES = "hasOutDoorgames";
    public static final String HAS_PARKING = "hasParking";
    public static final String HAS_ROOM_SERVICE = "hasRoomService";
    public static final String HAS_SWIMMING = "hasSwimming";
    public static final String HAS_TRAVEL_ASSISTANCE = "hasTravelAssistance";
    public static final String STAR_HOTEL = "starHotel";
    public static final String POLICIES = "policies";
    public static final String EMAIL = "email";





}
