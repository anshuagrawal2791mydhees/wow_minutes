package com.mydhees.wow.partner.allConstants;

/**
 * Created by anshu on 16/05/16.
 */
public class Constants {
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String PROFILE_IMAGE_ID = "profile_image_id";
    public static final String PROFILE_IMAGE_URI = "profile_image_uri";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String GCM_TOKEN = "gcm_token";
    public static final String LAUNCH_TIME_PREFERENCE_FILE = "launch_time";
    public static final String FIRST_TIME = "first_time";
    public static final String PROFILE_PREFERENCE_FILE = "profile";
    public static final String REGISTERED_HOTELS_IDS = "registered_hotels_ids";
    public static final String REGISTERED_HOTELS = "registered_hotels";
    public static final String REGISTERED_ROOMS = "registered_rooms";
    public static final String REGISTERED_HOTELS_NAMES = "registered_hotels_names";
    public static final String REGISTERED_HOTELS_NO_OF_ROOMS = "registered_hotels_no_of_rooms";
    public static final String SERVICES = "services";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String PHONE = "phone";
    public static final String CITY = "city";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String ADDRESS_LINE1 = "address_line1";
    public static final String ADDRESS_LINE2 = "address_line2";
     public static final String ADDRESS = "address";
    public static final String PIN = "pin";
    public static final String RELEASED_OFFER = "ro";


    public static final String CODE = "code";
    public static final String NPASS = "npass";

    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;
    public static final String PACKAGE_NAME =
            "com.example.anshu.wow_minutes";
    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
    public static final String RESULT_DATA_KEY = PACKAGE_NAME +
            ".RESULT_DATA_KEY";
    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME +
            ".LOCATION_DATA_EXTRA";

    // Code updated by  Pranav
    public static final String MAIN_SERVER_URL = "http://wowserver2.ap-south-1.elasticbeanstalk.com";
    public static final String CHECK_USER_URL = MAIN_SERVER_URL+"/check_user";
    public static final String REGISTER_URL = MAIN_SERVER_URL+"/register";
    public static final String HOTEL_UPDATE_URL = MAIN_SERVER_URL+"/hotel/update";
    public static final String HOTEL_DELETE = MAIN_SERVER_URL+"/hotel/delete";
    public static final String ROOM_DELETE = MAIN_SERVER_URL+"/room/delete";
    public static final String ROOM_UPDATE = MAIN_SERVER_URL+"/room/update";
    public static final String GET_ROOM = MAIN_SERVER_URL+"/room/get";
    public static final String HOTEL_REGISTER_URL = MAIN_SERVER_URL+"/hotel/register";
    public static final String RELEASE_OFFER_URL = MAIN_SERVER_URL+"/release_offer";
    public static final String ROOM_REGISTER_URL = MAIN_SERVER_URL+"/room/add";
    public static final String LOGIN_URL = MAIN_SERVER_URL+"/login";
    public static final String SEND_RESET_CODE_URL = MAIN_SERVER_URL+"/api/resetpass";
    public static final String RESET_PASS_URL = MAIN_SERVER_URL+"/api/resetpass/chg";
    public static final String GET_CREDITS = MAIN_SERVER_URL+"/get_credits";
    public static final String CREATE_ORDER = MAIN_SERVER_URL+"/create_order";
    public static final String ADD_CREDITS = MAIN_SERVER_URL+"/add_points";
    public static final String ORDER_STATUS = MAIN_SERVER_URL+"/get_order_status";
    public static final String GET_OFFER = MAIN_SERVER_URL+"/get_offer_by_owner";
    public static final String GET_ORDERS = MAIN_SERVER_URL+"/list_orders";
    public static final String GET_BOOKED_OFFERS = MAIN_SERVER_URL+"/vendor/bookings";
    public static final String FETCH_WISH = MAIN_SERVER_URL+"/wish/vendor/get";
    public static final String REPLY_WISH = MAIN_SERVER_URL+"/wish/vendor/reply";
    public static final String CHECK_CREDIT = MAIN_SERVER_URL+"/partner/check_credit";
    public static final String DELETE_OFFER = MAIN_SERVER_URL+"/offer/delete";
    public static final String UPLOAD_IMAGE = MAIN_SERVER_URL+"/partner/upload_image";
    public static final String CONTACT_US = MAIN_SERVER_URL+"/contactus/send";
    public static final String FEEDBACK = MAIN_SERVER_URL+"/contactus/feedback/send";
    public static final String REDEEM_HISTORY = MAIN_SERVER_URL+"/redeem_history";
    public static final String GET_CITY_LIST = MAIN_SERVER_URL+"/get_city_list";
}
