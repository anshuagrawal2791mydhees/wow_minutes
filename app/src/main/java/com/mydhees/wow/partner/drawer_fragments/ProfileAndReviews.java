package com.mydhees.wow.partner.drawer_fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.VolleySingleton;
import com.mydhees.wow.partner.adapters.Hotel_view_Adapter;
import com.mydhees.wow.partner.allConstants.Constants;
import com.mydhees.wow.partner.allConstants.HotelRegistrationConstants;
import com.mydhees.wow.partner.objects.image;
import com.mydhees.wow.partner.objects.registeredHotels;
import com.mydhees.wow.partner.utils.ImageCompressionAsyncTask;
import com.mydhees.wow.partner.utils.LocationUtility;
import com.mydhees.wow.partner.utils.MapUtility;
import com.mydhees.wow.partner.utils.task;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProfileAndReviews.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProfileAndReviews#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileAndReviews extends Fragment implements OnMapReadyCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;

    // keep track of camera capture intent
    final int CAMERA_CAPTURE = 1;
    // keep track of cropping intent
    final int PIC_CROP = 2;


    // Provider authority string must match the one declared in AndroidManifest.xml
     Uri picUri;
    // captured picture uri
    ArrayList<JSONObject> registered_hotel_jsons = new ArrayList<>();
    HashSet<String> registered_hotels_strings = new HashSet<>();
    ArrayList<String> registered_hotels_names = new ArrayList<>();
    RecyclerView hotel_view;
    Hotel_view_Adapter hotel_adapter;
    ImageView imgview;
    CircleImageView profile_pic;
    EditText company_name;
    EditText address;
    EditText phone;
    EditText mobile;
    TextView referral_code;
    EditText email;
    EditText service;
    Button edit_button;
    SharedPreferences prefs;
    SharedPreferences profile;
    SharedPreferences.Editor editor;
    private GoogleMap mMap;
    MapUtility gps;
    Location location;
    MarkerOptions mo;
    Marker marker;
    int PLACE_PICKER_REQUEST = 1;
    EditText street_address;
    TextView street_address2;


    ArrayList<String> addressParts = new ArrayList<>();
    ArrayList<String> addressParts2 = new ArrayList<>();
    String locationAddress=null;
    String locality=null;
    String country=null;
    LocationUtility getAddress;
    LocationUtility getAddress2;
    VolleySingleton volleySingleton;
    RequestQueue requestqueue;
    ImageLoader imageLoader;




    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ProfileAndReviews() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter one.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileAndReviews.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileAndReviews newInstance(String param1, String param2) {
        ProfileAndReviews fragment = new ProfileAndReviews();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_profile, container, false);
        profile = getActivity().getSharedPreferences(
                Constants.PROFILE_PREFERENCE_FILE, Context.MODE_PRIVATE);
        editor = profile.edit();
        volleySingleton = VolleySingleton.getinstance();
        requestqueue = volleySingleton.getrequestqueue();
        imageLoader = volleySingleton.getimageloader();

       hideKeyboard(getContext());
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map44);
        mapFragment.getMapAsync(this);
        hotel_view= (RecyclerView) v.findViewById(R.id.registered_hotels_view);
        hotel_view.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        getAddress = new LocationUtility(getContext());
        edit_button = (Button)v.findViewById(R.id.edit_button);
        company_name = (EditText)v.findViewById(R.id.company_name1);
        address= (EditText)v.findViewById(R.id.street_address2);
       // phone=(EditText)v.findViewById(R.id.phone);
        mobile=(EditText)v.findViewById(R.id.mobile);
        email=(EditText)v.findViewById(R.id.email);
        service=(EditText)v.findViewById(R.id.service);
        service.setEnabled(false);
        address.setEnabled(false);
        imgview = (ImageView)v.findViewById(R.id.add_dp);
        profile_pic = (CircleImageView) v.findViewById(R.id.user_profile_photo);

        if(!profile.getString(Constants.PROFILE_IMAGE_URI,"default").equals("default")){
            profile_pic.setImageBitmap(BitmapFactory.decodeFile(profile.getString(Constants.PROFILE_IMAGE_URI,"default")));
        }
        if(!profile.getString(Constants.PROFILE_IMAGE_ID,"default").equals("default")){
            String url = "https://s3-us-west-2.amazonaws.com/wowimagesupload/resized/middle/"+profile.getString(Constants.PROFILE_IMAGE_ID,"default")+".jpg";
            imageLoader.get(url, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {

                    profile_pic.setImageBitmap(response.getBitmap());

                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("err",error.toString());

                }
            });
        }

        referral_code = (TextView)v.findViewById(R.id.referral_code);


       hotel_adapter= new Hotel_view_Adapter(getContext());
        hotel_view.setAdapter(hotel_adapter);
//
//        mFragmentManager = getActivity().getSupportFragmentManager();
//                            mFragmentTransaction = mFragmentManager.beginTransaction().setCustomAnimations(R.anim.card_flip_left_out,R.anim.card_flip_right_in);//R.anim.card_flip_right_in,R.anim.card_flip_right_out,R.anim.card_flip_left_in,R.anim.card_flip_left_out);
//                            mFragmentTransaction.replace(R.id.containerView,new ProfileAndReviews()).commit();

        if(profile.getStringSet(Constants.REGISTERED_HOTELS,new HashSet<String>()).size()>0) {
            registered_hotels_strings.addAll(profile.getStringSet(Constants.REGISTERED_HOTELS,new HashSet<String>()));

            Iterator iterator = registered_hotels_strings.iterator();
            while(iterator.hasNext())
            {
                try {
                    registered_hotel_jsons.add(new JSONObject(iterator.next().toString()));
                } catch (JSONException e) {
                    Log.e("err",e.toString());
                }
            }
            hotel_adapter.clearAll();
            for(int i=0;i<registered_hotel_jsons.size();i++)
            {
                try {
//                    registered_hotels_names.add(registered_hotel_jsons.get(i).getString(HotelRegistrationConstants.HOTEL_NAME));
                    if(registered_hotel_jsons.get(i).getJSONObject("imagesS3").getJSONArray("name").length()>0) {
                        hotel_adapter.regHotelsShow(new registeredHotels(registered_hotel_jsons.get(i).getString(HotelRegistrationConstants.HOTEL_NAME), registered_hotel_jsons.get(i).getJSONObject("imagesS3").getJSONArray("name").get(0).toString(), registered_hotel_jsons.get(i).getString("hotelId")));
                    }
                    else{

                        hotel_adapter.regHotelsShow(new registeredHotels(registered_hotel_jsons.get(i).getString(HotelRegistrationConstants.HOTEL_NAME), "", registered_hotel_jsons.get(i).getString("hotelId")));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }



        }
        else{


            Toast.makeText(getContext(),"No Hotels Registered",Toast.LENGTH_LONG).show();

        }


        imgview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, PIC_CROP);
//                    performCrop();
                    // use standard intent to capture an image
                   // Intent captureIntent = new Intent(
                            //MediaStore.ACTION_IMAGE_CAPTURE);
                    // we will handle the returned data in onActivityResult
                    //startActivityForResult(captureIntent, CAMERA_CAPTURE);
                } catch (ActivityNotFoundException anfe) {
                    // display an error message
                    String errorMessage = "Whoops - your device doesn't support capturing images!";
                    Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });



        company_name.setText(profile.getString(Constants.NAME,"Mydhees"));
        address.setText(profile.getString(Constants.ADDRESS,"Bangalore"));
       // phone.setText(profile.getString(Constants.PHONE,"123456789"));
        mobile.setText(profile.getString(Constants.PHONE,"9534032322"));
        email.setText(profile.getString(Constants.EMAIL,"example@example.com"));
       service.setText(profile.getStringSet(Constants.SERVICES,new HashSet<String>()).toString());
        referral_code.setText(profile.getString("referralCode","no code"));

        edit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(company_name.isEnabled()==false)
                {
                    company_name.setEnabled(true);
                    address.setEnabled(true);
                  //  phone.setEnabled(true);
                    mobile.setEnabled(true);
                    email.setEnabled(true);
                    service.setEnabled(true);

                    edit_button.setText("Save Details");
                    edit_button.setBackgroundColor(Color.GRAY);

                }
                else
                {

                    if(company_name.getText().toString().equals(""))
                        company_name.setError("Required");
                    else if (address.getText().toString().equals(""))
                        address.setError("Required");
                    else if (mobile.getText().toString().equals(""))
                        mobile.setError("Required");
                    else if (email.getText().toString().equals(""))
                        email.setError("Required");

                    else 
                    {
                        editor.putString("company_name",company_name.getText().toString());
                        editor.putString("address",address.getText().toString());
                        //editor.putString("phone",phone.getText().toString());
                        editor.putString("email",email.getText().toString());
                        editor.putString("mobile",mobile.getText().toString());
                       editor.putString("display_name",service.getText().toString());

                        editor.commit();

                        company_name.setEnabled(false);
                        address.setEnabled(false);
                      //  phone.setEnabled(false);
                        mobile.setEnabled(false);
                        email.setEnabled(false);
                        service.setEnabled(false);
                      //  display_name.setEnabled(false);
                        edit_button.setText("Edit Details");
                        edit_button.setBackgroundColor(Color.RED);
                        
                        
                        
                    }
                    
                }

            }

        });








        return v;



    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney and move the camera
        mo = new MarkerOptions().position(new LatLng(profile.getFloat(Constants.LATITUDE,0), profile.getFloat(Constants.LONGITUDE,0))).title("Your Location");

        LatLng sydney = new LatLng(profile.getFloat(Constants.LATITUDE,0), profile.getFloat(Constants.LONGITUDE,0));

        marker = mMap.addMarker(mo);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        marker.showInfoWindow();
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(sydney)      // Sets the center of the map to location user
                .zoom(13)                   // Sets the zoom
                .bearing(90)                // Sets the orientation of the camera to east
                // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


    }



    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == getActivity().RESULT_OK) {
            // user is returning from capturing an image using the camera
            if (requestCode == CAMERA_CAPTURE) {
                // get the Uri for the captured image
                //picUri = data.getData();
                // carry out the crop operation
                performCrop();
            }
            // user is returning from cropping the image
            else if (requestCode == PIC_CROP && data.getData() != null) {

                GraphicsUtil graphicUtil = new GraphicsUtil();

                picUri = data.getData();
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(),picUri);
                    //upload_image(bitmap);


                    final String filepath = new ImageCompressionAsyncTask(true,getContext()).execute(data.getDataString()).get();
                    Log.e("filepath",filepath);
//                                    Log.e("stringdata",data.getDataString());

                    image im = new image(profile.getString(Constants.NAME,"Wow"),filepath);

//                    images_uris.add(im);
                    ArrayList<image> images_uris = new ArrayList<>();
                    images_uris.add(im);
                    HashMap<String,String> extradata = new HashMap<>();
                    extradata.put("id",profile.getString(Constants.ID,"defualt"));

                    final ProgressDialog dialog = new ProgressDialog(getContext());
                    dialog.show();
                    dialog.setCancelable(false);
                    final Bitmap finalBitmap = bitmap;
                    task a = new task(getContext(),images_uris, Constants.UPLOAD_IMAGE,extradata){
                        @Override
                        protected void onPostExecute(Response response) {

                            dialog.dismiss();
                            try {
                                JSONObject resp = new JSONObject(response.body().string());
                                Log.e("resp",resp.toString());
                                if(resp.getString("res").equals("true")){
                                    profile_pic.setImageBitmap(BitmapFactory.decodeFile(filepath));
                                    editor.putString(Constants.PROFILE_IMAGE_ID,resp.getString("id"));
                                    editor.putString(Constants.PROFILE_IMAGE_URI,filepath);
                                    editor.commit();
                                    Toast.makeText(getContext(),"Updated",Toast.LENGTH_LONG).show();

                                }
                                else
                                    Toast.makeText(getContext(),"Failed",Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            super.onPostExecute(response);
                        }
                    };
                    a.execute();
                } catch (IOException e) {
                    Log.e("err",e.toString());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                // Log.d(TAG, String.valueOf(bitmap));

            }
            else {
                Log.e("here","here");
            }
        }
    }

    private void upload_image(Bitmap bitmap) {


    }

    /**
     * Helper method to carry out crop operation
     */
    private void performCrop() {
        // take care of exceptions
        try {
            // call the standard crop action intent (the user device may not
            // support it)
            Intent cropIntent = new Intent();
            // indicate image type and Uri
            cropIntent.setType("image/*");
            // set crop properties
            cropIntent.setAction(Intent.ACTION_GET_CONTENT);
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            // retrieve data on return
            cropIntent.putExtra("return-data", true);

            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PIC_CROP);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast
                    .makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }

    }


    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

}


