package com.mydhees.wow.partner.registration_and_login;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.VolleySingleton;
import com.mydhees.wow.partner.allConstants.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class user_details extends AppCompatActivity {

    EditText name;
    EditText email;
    EditText password;
    EditText repeat_password;
    EditText mobile_number;
    Button submit;
    boolean error=false;
    Spinner city;
    SharedPreferences profile;
    SharedPreferences.Editor editor;
    TextView tv_terms;
    TextView login_now;
    CheckBox check,referral_chk;
    EditText referral_code;
    LinearLayout referal_code_layout;
    Button apply_code;
    String [] cities;


    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        //getSupportActionBar().setTitle("Hello world App");
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        profile = getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE,MODE_PRIVATE);
        editor=profile.edit();

        name = (EditText)findViewById(R.id.name);
        email = (EditText)findViewById(R.id.email);
        password= (EditText)findViewById(R.id.password);
          repeat_password = (EditText)findViewById(R.id.repeat_password);
        mobile_number = (EditText)findViewById(R.id.mobile);
        check = (CheckBox)findViewById(R.id.check);
        city = (Spinner)findViewById(R.id.city);
        referral_code = (EditText)findViewById(R.id.referral_code);
        referral_chk = (CheckBox)findViewById(R.id.cb_refcode);
        apply_code = (Button)findViewById(R.id.apply_code);

        volleySingleton=VolleySingleton.getinstance();
        requestQueue = volleySingleton.getrequestqueue();

        String service = null;
        for(String aSiteId: profile.getStringSet(Constants.SERVICES,null)) {
            service = aSiteId;
            break;
        }
        getcities(service);

        tv_terms = (TextView)findViewById(R.id.tv_terms);
        tv_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(user_details.this);
                dialog.setTitle("Terms and Conditions");
                dialog.setContentView(R.layout.terms_conditions_dialog);
                dialog.show();

            }
        });

        referal_code_layout = (LinearLayout)findViewById(R.id.refferal_code_layout);
        referral_chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    referal_code_layout.setVisibility(View.VISIBLE);
                }
                else{
//                           referral_code = (EditText)findViewById(R.id.referral_code);
                    referal_code_layout.setVisibility(View.GONE);
                }
            }
        });




        submit = (Button)findViewById(R.id.submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                error=false;
                if(name.getText().toString().equals("")) {
                    name.setError("Enter Name");
                    error=true;
                }

                if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText()).matches())
                {
                   email.setError("Invalid Email");
                    //email.setText("");
                    error = true;
                }
                if(email.getText().toString().equals(""))
                {
                    email.setError("Required");
                    error = true;
                }
                if(!repeat_password.getText().toString().equals(password.getText().toString()))
                {
                    repeat_password.setError("Passwords don't match");
                    repeat_password.setText("");
                    error = true;
                }
                if(password.getText().length()<5)
                {
                    password.setError("Minimum 5 characters");
                    password.setText("");
                    repeat_password.setText("");
                }
                if(mobile_number.getText().length()<10)
                {
                    mobile_number.setError("Invalid Mobile Number");
                    error=true;
                }

                if(city.getSelectedItemPosition()==0)
                {
                    error=true;
                    Toast.makeText(getApplicationContext(),"Select City",Toast.LENGTH_SHORT).show();

                }
                if(!check.isChecked())
                {
                    check.setError("Required");
                    error=true;
                }
                if(error==false)
                {
                    //Toast.makeText(getApplicationContext(),city.getSelectedItem().toString(),Toast.LENGTH_LONG).show();

                    try {
                        checkUser();
                    } catch (JSONException e) {
                        Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
                    }



                }

            }
        });

        TextView login_now =(TextView)findViewById(R.id.login_now);
        login_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(user_details.this,login.class));
            }
        });

    }

    private void getcities(final String service) {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.show();
        final ArrayList<String> cities_list = new ArrayList<>();
        StringRequest request = new StringRequest(Request.Method.POST, Constants.GET_CITY_LIST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respons) {
                        dialog.dismiss();
                        Log.e("response",respons.toString());
                        JSONObject response = null;
                        try {
                            response = new JSONObject(respons);
                        } catch (JSONException e) {
                            Log.e("error",e.toString());
                        }
                        try {
                            if(response.getBoolean("res"))
                            {
                                cities_list.add("Select City");
                                JSONArray cities = response.getJSONArray("cities");
                                for(int i=0;i<cities.length();i++)
                                    cities_list.add(cities.getString(i));

//                                String[] x = cities_list.toArray(new String[cities_list.size()]);
                                ArrayAdapter<String> adapter = new ArrayAdapter<>(user_details.this,
                                        android.R.layout.simple_spinner_item,cities_list);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                city.setAdapter(adapter);


                            }
                            else{
                                Toast.makeText(getApplicationContext(),"This service is not operational yet",Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(user_details.this,select_service.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }

                        } catch (JSONException e) {
                            Log.e("volley error",e.toString());
                            Intent intent = new Intent(user_details.this,select_service.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_LONG).show();
                Log.e("vollley error",error.toString());
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
//                params.put(Constants.NAME,name.getText().toString());
                params.put("service",service);
//                params.put(Constants.PASSWORD, password.getText().toString());
//                params.put(Constants.PHONE, mobile_number.getText().toString());
//                params.put(Constants.CITY, city.getSelectedItem().toString());
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);


    }

    private void checkUser() throws JSONException {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.show();

        StringRequest request = new StringRequest(Request.Method.POST, Constants.CHECK_USER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respons) {
                        dialog.dismiss();
                        Log.e("response",respons.toString());
                        JSONObject response = null;
                        try {
                            response = new JSONObject(respons);
                        } catch (JSONException e) {
                            Log.e("error",e.toString());
                        }
                        try {
                            if(response.getBoolean("res"))
                            {
                                editor.putString(Constants.NAME,name.getText().toString());
                                editor.putString(Constants.EMAIL,email.getText().toString());
                                editor.putString(Constants.PASSWORD,password.getText().toString());
                                editor.putString(Constants.PHONE,mobile_number.getText().toString());
                                editor.putString(Constants.CITY,city.getSelectedItem().toString());
                                editor.commit();
                                Intent intent = new Intent(getApplicationContext(),location_selection.class);
                                if(!referral_code.getText().toString().matches(""))
                                intent.putExtra("referralCode",referral_code.getText().toString());
                                startActivity(intent);

                                //Toast.makeText(getApplicationContext(),"Successfully Registered",Toast.LENGTH_LONG).show();
                            }
                            else
                            {
                                Toast.makeText(getApplicationContext(),response.getString("response"),Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_LONG).show();
                Log.e("vollley error",error.toString());
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
//                params.put(Constants.NAME,name.getText().toString());
                params.put(Constants.EMAIL,email.getText().toString());
//                params.put(Constants.PASSWORD, password.getText().toString());
//                params.put(Constants.PHONE, mobile_number.getText().toString());
//                params.put(Constants.CITY, city.getSelectedItem().toString());
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }

    //For back button in toolbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
