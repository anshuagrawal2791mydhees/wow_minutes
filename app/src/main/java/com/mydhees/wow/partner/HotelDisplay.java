package com.mydhees.wow.partner;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.mydhees.wow.partner.adapters.ImageRecyclerAdapter;
import com.mydhees.wow.partner.adapters.PolicyAdapter;
import com.mydhees.wow.partner.adapters.RoomTypeDisplay;
import com.mydhees.wow.partner.allConstants.AddroomConstants1;
import com.mydhees.wow.partner.allConstants.Constants;
import com.mydhees.wow.partner.allConstants.HotelRegistrationConstants;
import com.mydhees.wow.partner.allConstants.Release_constants;
import com.mydhees.wow.partner.objects.image;
import com.mydhees.wow.partner.objects.registeredHotels;
import com.mydhees.wow.partner.objects.roomDisplay;
import com.mydhees.wow.partner.utils.ImageCompressionAsyncTask;
import com.mydhees.wow.partner.utils.LocationUtility;
import com.mydhees.wow.partner.utils.MapUtility2;
import com.mydhees.wow.partner.utils.task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.ExecutionException;

import okhttp3.Response;

public class HotelDisplay extends AppCompatActivity {
    MapUtility2 locationGetter;
    ArrayList<String> addressParts = new ArrayList<>();
    ArrayList<String> addressParts2 = new ArrayList<>();
    ArrayList<image> images_uris = new ArrayList<>();
    ArrayList<String> pol = new ArrayList<String>();
    CardView cv;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    String add1, add2, cty = "";
    ProgressDialog dialog;
    String[] extra_facilities;
    Boolean[] facilites_array = new Boolean[14];
    task register_hotel, register_hotel1, register_hotel44;
    boolean[] extra_facilities_selected;
    HashMap<String, String> data = new HashMap<>();
    HashMap<String, String> data1 = new HashMap<>();
    HashMap<String, String> data44 = new HashMap<>();
    ImageRecyclerAdapter adapter;
    Uri uri;
    LatLng hotel_location;
    String image_description;
    String locationAddress = "";
    private int PICK_IMAGE_REQUEST = 2;
    PolicyAdapter policy_Adapter;
    String locality = "";
    int number = 1;
    Button add_image, add_policy, addMoreFacilities;
    String country = "", postal_code = "", city = "";
    LocationUtility getAddress;
    LocationUtility getAddress2;
    int PLACE_PICKER_REQUEST = 1;
    com.mydhees.wow.partner.adapters.ImageDisplay imgd;
    boolean error;
    Button edit, delete;
    Boolean edit_me = true, edit_me2 = false, update_me = false, image_error;
    RoomTypeDisplay rtd;
    RecyclerView hotel_view, policy, recycler;
    EditText hotelName, hotelType, no_of_rooms, phone, mobile, address, hotelPolicy;
    RatingBar hotelRating;
    String hname, hid;
    TextView update;
    SharedPreferences profile;
    SharedPreferences.Editor editor;
    ArrayList<JSONObject> registered_hotel_jsons = new ArrayList<>();
    HashSet<String> registered_hotels_strings = new HashSet<>();
    ArrayList<String> registered_hotels_names = new ArrayList<>();
    ArrayList<JSONObject> registered_rooms_jsons = new ArrayList<>();
    HashSet<String> registered_rooms_strings = new HashSet<>();
    ArrayList<Integer> facilites_images = new ArrayList<>();
    int j = 0;
    int i,p;
    LinearLayout layout;
    int facilites[] = new int[]{R.drawable.travel_assistance, R.drawable.reg_hotel_swim, R.drawable.roomservice, R.drawable.parking, R.drawable.outdoor, R.drawable.laundryservice, R.drawable.spa, R.drawable.frontdesk, R.drawable.coffeeshop, R.drawable.businesservice, R.drawable.reg_hotel_gym, R.drawable.ic_live_tv_black_24dp, R.drawable.ic_restaurant_menu_black_24dp, R.drawable.ic_wifi_black_24dp1};

    private Toolbar toolbar;
    Button edit_toolbr, delete_toolbr;

    VolleySingleton volleySingleton;
    ImageLoader imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_display);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.primary_dark));
        }
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_back_white_icon);


        edit_toolbr = (Button) findViewById(R.id.edit_toolbr);
        delete_toolbr = (Button) findViewById(R.id.delete_toolbr);


        hideSoftKeyboard();
        hotel_view = (RecyclerView) findViewById(R.id.recycler333);
        hotel_view.setLayoutManager(new LinearLayoutManager(getApplication(), LinearLayoutManager.HORIZONTAL, false));
        hotelName = (EditText) findViewById(R.id.dis_hname);
        hotelType = (EditText) findViewById(R.id.dis_htype);
        no_of_rooms = (EditText) findViewById(R.id.dis_roomn);
        phone = (EditText) findViewById(R.id.dis_phone);
        mobile = (EditText) findViewById(R.id.dis_mobile);
        addMoreFacilities = (Button) findViewById(R.id.edit_dis_facility);
        address = (EditText) findViewById(R.id.dis_address);
        hotelPolicy = (EditText) findViewById(R.id.dis_policy);
        layout = (LinearLayout) findViewById(R.id.layout);
        update = (TextView) findViewById(R.id.update_info);
        recycler = (RecyclerView) findViewById(R.id.recycler_view_dis);
        recycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        adapter = new ImageRecyclerAdapter(this);
        adapter.disable();
        policy_Adapter = new PolicyAdapter(this);
        policy_Adapter.disable();
        imgd = new com.mydhees.wow.partner.adapters.ImageDisplay(this);
        recycler.setAdapter(adapter);

        volleySingleton = VolleySingleton.getinstance();
        imageLoader = volleySingleton.getimageloader();




        hotelRating = (RatingBar) findViewById(R.id.dis_ratingBar);
        profile = getSharedPreferences(
                Constants.PROFILE_PREFERENCE_FILE, Context.MODE_PRIVATE);
        editor = profile.edit();
        // edit = (Button)findViewById(R.id.edit_dis);
        cv = (CardView) findViewById(R.id.cv_dis);
        // delete = (Button)findViewById(R.id.delete_dis);
        add_policy = (Button) findViewById(R.id.add_policy_dis);
        add_image = (Button) findViewById(R.id.add_image_dis);

        policy = (RecyclerView) findViewById(R.id.policy_list_dis);
        policy.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        cv.setVisibility(View.VISIBLE);
        policy.setAdapter(policy_Adapter);


        delete_toolbr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog.Builder builder = new AlertDialog.Builder(HotelDisplay.this);

                builder.setMessage("Are you sure want to delete hotel?");
                builder.setPositiveButton("Yes, Sure", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, int which) {
                        data1.put(HotelRegistrationConstants.OWNER_ID, profile.getString(Constants.ID, "default"));
                        data1.put(AddroomConstants1.HOTEL_ID, hid);

                        ArrayList<image> temp = new ArrayList<image>(adapter.get_images());
                        Log.e("temp size",temp.size()+"");
                        register_hotel1 = new task(HotelDisplay.this, temp , Constants.HOTEL_DELETE, data1) {
                            @Override
                            protected void onPostExecute(Response response) {
                                //dialog.dismiss();
                                Boolean resp = false;
                                dialog.dismiss();
                                Log.e("resp delete", response.toString());
                                HashSet<String> registered_hotels1 = new HashSet<String>();
                                HashSet<String> registered_rooms1 = new HashSet<String>();

                                try {
                                    JSONObject resp1 = new JSONObject(response.body().string());
                                    Log.e("respJSOn", resp1.toString());
                                    //registered_hotels1.add(resp1.toString());
                                    resp = resp1.getBoolean("res");
                                } catch (JSONException e) {
                                    Log.e("err", e.toString());
                                } catch (IOException e) {
                                    Log.e("err", e.toString());
                                }
                                if (resp) {
                                    Toast.makeText(HotelDisplay.this, "Deleted Successfully", Toast.LENGTH_LONG).show();

                                     if (profile.getStringSet(Constants.REGISTERED_HOTELS, new HashSet<String>()).size() > 0) {

                                        //Log.e("data",profile.getStringSet(Constants.REGISTERED_HOTELS_NAMES,null).toString());

                                        HashSet<String> h = new HashSet<>(profile.getStringSet(Constants.REGISTERED_HOTELS, null));
                                        Iterator iterator2 = h.iterator();
                                        Log.e("h", h.toString());

                                        while (iterator2.hasNext()) {

                                            try {
                                                String s = iterator2.next().toString();
                                                JSONObject j = new JSONObject(s);
                                                if (j.getString("hotelId").equals(hid))
                                                    iterator2.remove();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }
                                        registered_hotels1.addAll(h);
                                        Iterator iterator1 = registered_hotels1.iterator();
                                        while (iterator1.hasNext()) {
                                            try {
                                                registered_hotel_jsons.add(new JSONObject(iterator1.next().toString()));
                                            } catch (JSONException e) {
                                                Log.e("err", e.toString());
                                            }
                                        }

                                        for (int j = 0; j < registered_hotel_jsons.size(); j++) {
                                            try {

                                                if (hid.equals(registered_hotel_jsons.get(j).getString(Release_constants.HOTEL_ID))) {
                                                    editor.putStringSet(Constants.REGISTERED_HOTELS, registered_hotels1);

                                                    editor.commit();


                                                }


                                            } catch (JSONException k) {
                                            }
                                        }
                                    } else {
                                        Log.e("storing", "first");
                                    }
                                    Log.e("hotels", registered_hotels1.toString());

                                    //rooms
                                    if (profile.getStringSet(Constants.REGISTERED_ROOMS, new HashSet<String>()).size() > 0) {
                                        HashSet<String> h2 = new HashSet<>(profile.getStringSet(Constants.REGISTERED_ROOMS, null));

                                        Iterator iterator22 = h2.iterator();
                                        Log.e("h", h2.toString());

                                        Log.e("h", h2.toString());

                                        while (iterator22.hasNext()) {

                                            try {
                                                String s = iterator22.next().toString();
                                                JSONObject j = new JSONObject(s);
                                                if (j.getString("hotelId").equals(hid))
                                                    iterator22.remove();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }
                                        registered_rooms1.addAll(h2);
                                        Log.e("h log", h2.toString());
                                        Log.e("rjosn log", registered_rooms_jsons.toString());
                                        Iterator iterator12 = h2.iterator();
                                        while (iterator12.hasNext()) {
                                            try {
                                                registered_rooms_jsons.add(new JSONObject(iterator12.next().toString()));
                                            } catch (JSONException e) {
                                                Log.e("err", e.toString());
                                            }
                                        }


                                        for (int l = 0; l < registered_rooms_jsons.size(); l++) {
                                            try {

                                                if (hid.equals(registered_rooms_jsons.get(l).getString(Release_constants.HOTEL_ID))) {


                                                    editor.putStringSet(Constants.REGISTERED_ROOMS, registered_rooms1);

                                                    editor.commit();


                                                }


                                            } catch (JSONException k) {
                                            }
                                        }
                                    }


                                    finish();
                                    Intent it = new Intent(HotelDisplay.this, Home.class);
                                    startActivity(it);
                                } else {
                                    Toast.makeText(HotelDisplay.this, "Hotel Not deleted!!!", Toast.LENGTH_SHORT).show();
                                }
                                super.onPostExecute(response);
                            }

                        };
                        register_hotel1.execute();

                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();


            }
        });


        add_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, PICK_IMAGE_REQUEST);


            }
        });

        add_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edit_me2) {
                    if (hotelPolicy.getText().toString().matches("")) {
                        hotelPolicy.setError("required");
                        error = true;
                    } else {

                        String a = hotelPolicy.getText().toString();
                        hotelPolicy.setError(null);
                        policy_Adapter.addpolicy("-" + hotelPolicy.getText().toString());
                        policy.scrollToPosition(policy_Adapter.getItemCount() - 1);
                        hotelPolicy.setText("");
                        error = false;

                    }
                }

            }
        });


        Intent intent = getIntent();
        hname = intent.getStringExtra("hname");
        hid = intent.getStringExtra("hid");
        // hotelName.setText(hname);
        rtd = new RoomTypeDisplay(getApplicationContext());
        hotel_view.setAdapter(rtd);
        hotelName.setEnabled(false);
        hotelType.setEnabled(false);
        no_of_rooms.setEnabled(false);
        phone.setEnabled(false);
        mobile.setEnabled(false);
        address.setEnabled(false);
        hotelPolicy.setEnabled(false);
        hotel_view.setEnabled(false);
        hotelRating.setEnabled(false);
        add_image.setEnabled(false);
        add_policy.setEnabled(false);
        policy.setEnabled(false);
        adapter.disable();
        policy_Adapter.disable();
        update.setEnabled(false);
        addMoreFacilities.setEnabled(false);

        update.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                Log.e("upate", update_me + "");
                if (update_me) {

                    data.put(HotelRegistrationConstants.OWNER_ID, profile.getString(Constants.ID, "default"));
                    data.put(AddroomConstants1.HOTEL_ID, hid);
                    data.put(HotelRegistrationConstants.HOTEL_NAME, hotelName.getText().toString());
                    data.put(HotelRegistrationConstants.HOTEL_TYPE, hotelType.getText().toString());
                    data.put(HotelRegistrationConstants.NO_OF_ROOMS, no_of_rooms.getText().toString());
                    data.put(HotelRegistrationConstants.PHONE_NUMBER, phone.getText().toString());
                    data.put(HotelRegistrationConstants.MOBILE_NUMBER, mobile.getText().toString());

                    if (locationAddress.equals("") || locality.equals("") || country.equals("") || city.equals("")) {

                        data.put(HotelRegistrationConstants.ADDRESS_LINE1, add1);
                        data.put(HotelRegistrationConstants.ADDRESS_LINE2, add2);
                        //data.put(HotelRegistrationConstants.CITY,cty);
                    } else {
                        data.put(HotelRegistrationConstants.ADDRESS_LINE1, locationAddress);
                        data.put(HotelRegistrationConstants.ADDRESS_LINE2, locality + "" + country);
                        //data.put(HotelRegistrationConstants.CITY,city);
                    }
                    data.put(HotelRegistrationConstants.ADDRESS, address.getText().toString());
                    data.put(HotelRegistrationConstants.PIN, postal_code);
                    //    data.put(HotelRegistrationConstants.LONGITUDE,hotel_location.longitude+"");
                    //  data.put(HotelRegistrationConstants.LATITUDE,hotel_location.latitude+"");

                    data.put(HotelRegistrationConstants.STAR_HOTEL, hotelRating.getRating() + "");
                    data.put(HotelRegistrationConstants.EMAIL, profile.getString(Constants.EMAIL, "default"));

                    for (int i = 0; i < facilites_array.length; i++) {
                        facilites_array[i] = false;
                        if (i >= 0) {
                            facilites_array[i] = extra_facilities_selected[i];
                        }
                    }

                    String[] facilites_keys = new String[]{HotelRegistrationConstants.HAS_WIFI,
                            HotelRegistrationConstants.HAS_DINING,
                            HotelRegistrationConstants.HAS_TV,
                            HotelRegistrationConstants.HAS_GYM,
                            HotelRegistrationConstants.HAS_BUSINESS_SERVICE,
                            HotelRegistrationConstants.HAS_COFFEE_SHOP,
                            HotelRegistrationConstants.HAS_FRONT_DESK,
                            HotelRegistrationConstants.HAS_SPA,
                            HotelRegistrationConstants.HAS_LAUNDRY,
                            HotelRegistrationConstants.HAS_OUT_DOOR_GAMES,
                            HotelRegistrationConstants.HAS_PARKING,
                            HotelRegistrationConstants.HAS_ROOM_SERVICE,
                            HotelRegistrationConstants.HAS_SWIMMING,
                            HotelRegistrationConstants.HAS_TRAVEL_ASSISTANCE};
                    JSONObject facilitesJSON = new JSONObject();
                    for (int i = 0; i < 14; i++) {
                        try {
                            facilitesJSON.put(facilites_keys[i], facilites_array[i]);
                        } catch (JSONException e) {
                            Log.e("error", e.toString());
                        }
                    }
                    data.put(HotelRegistrationConstants.FACILITIES, facilitesJSON.toString());
                    ArrayList<String> pol = new ArrayList<String>();
                    pol.addAll(policy_Adapter.getPolicies());
                    String[] policies = pol.toArray(new String[pol.size()]);
                    try {
                        JSONArray policiesJSON = new JSONArray(policies);
                        data.put(HotelRegistrationConstants.POLICIES, policiesJSON.toString());
                    } catch (JSONException e) {
                        Log.e("err", e.toString());
                    }


                    //data.put(HotelRegistrationConstants.POLICIES,hotel_name.getText().toString());
                    //                   dialog = new ProgressDialog(getApplicationContext());
                    //
                    //                    dialog.show();

                    Log.e("data", data.toString());
                    final ProgressDialog dialog23 = new ProgressDialog(HotelDisplay.this);
                    dialog23.show();
                    register_hotel = new task(HotelDisplay.this, adapter.get_images(), Constants.HOTEL_UPDATE_URL, data) {
                        @Override
                        protected void onPostExecute(Response response) {
                            // dialog.dismiss();
                            Toast.makeText(HotelDisplay.this, "Updated Successfully", Toast.LENGTH_LONG).show();
                            //                            dialog.dismiss();
                            dialog23.dismiss();
                            HashSet<String> registered_hotels = new HashSet<String>();

                            try {
                                JSONObject resp = new JSONObject(response.body().string());
                                Log.e("respJSOn", resp.toString());
                                registered_hotels.add(resp.toString());

                            } catch (JSONException e) {
                                Log.e("err", e.toString());
                            } catch (IOException e) {
                                Log.e("err", e.toString());
                            }

                            if (profile.getStringSet(Constants.REGISTERED_HOTELS, new HashSet<String>()).size() > 0) {
                                Log.e("storing", "not_first");
                                //Log.e("data",profile.getStringSet(Constants.REGISTERED_HOTELS_NAMES,null).toString());

                                HashSet<String> h = new HashSet<>(profile.getStringSet(Constants.REGISTERED_HOTELS, null));
                                Iterator iterator2 = h.iterator();
                                Log.e("h", h.toString());
                                //                                for (String s : h) {
                                //                                    try {
                                //                                        JSONObject j = new JSONObject(s);
                                //                                        if(j.getString("hotelId").equals(hid))
                                //                                            h.remove(s);
                                //                                    } catch (JSONException e) {
                                //                                        e.printStackTrace();
                                //                                    }
                                //
                                //                                }
                                while (iterator2.hasNext()) {

                                    try {
                                        String s = iterator2.next().toString();
                                        JSONObject j = new JSONObject(s);
                                        if (j.getString("hotelId").equals(hid))
                                            iterator2.remove();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                                registered_hotels.addAll(h);
                                Iterator iterator1 = registered_hotels.iterator();
                                while (iterator1.hasNext()) {
                                    try {
                                        registered_hotel_jsons.add(new JSONObject(iterator1.next().toString()));
                                    } catch (JSONException e) {
                                        Log.e("err", e.toString());
                                    }
                                }

                                for (int j = 0; j < registered_hotel_jsons.size(); j++) {
                                    try {

                                        if (hid.equals(registered_hotel_jsons.get(j).getString(Release_constants.HOTEL_ID))) {
                                            editor.putStringSet(Constants.REGISTERED_HOTELS, registered_hotels);

                                            editor.commit();


                                        }


                                    } catch (JSONException k) {
                                    }
                                }
                            } else {
                                Log.e("storing", "first");
                            }
                            Log.e("hotels", registered_hotels.toString());

                            /////
                            if (profile.getStringSet(Constants.REGISTERED_ROOMS, new HashSet<String>()).size() > 0) {
                                HashSet<String> h2 = new HashSet<>(profile.getStringSet(Constants.REGISTERED_ROOMS, null));
                                JSONObject j;
                                Iterator iterator22 = h2.iterator();
                                Log.e("h", h2.toString());

                                //                                while (iterator22.hasNext()) {
                                LinkedList<String> strsList = new LinkedList<String>();
                                strsList.addAll(h2);
                                for (int i = 0; i < strsList.size(); i++) {


                                    try {


                                        j = new JSONObject(strsList.get(i));

                                        if (j.getString("hotelId").equals(hid)) {
                                            synchronized (j) {


                                                j.put("hotelName", hotelName.getText().toString());

                                                j.notify();
                                                strsList.set(i, j.toString());

                                            }


                                        }

                                        Log.e("j", j.toString());

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }
                                h2.clear();
                                h2.addAll(strsList);
                                Log.e("h log", h2.toString());
                                Log.e("rjosn log", registered_rooms_jsons.toString());
                                Iterator iterator12 = h2.iterator();
                                while (iterator12.hasNext()) {
                                    try {
                                        registered_rooms_jsons.add(new JSONObject(iterator12.next().toString()));
                                    } catch (JSONException e) {
                                        Log.e("err", e.toString());
                                    }
                                }


                                for (int l = 0; l < registered_rooms_jsons.size(); l++) {
                                    try {

                                        if (hid.equals(registered_rooms_jsons.get(l).getString(Release_constants.HOTEL_ID))) {


                                            editor.putStringSet(Constants.REGISTERED_ROOMS, h2);

                                            editor.commit();


                                        }


                                    } catch (JSONException k) {
                                    }
                                }
                            }
                            ////


                            finish();

                            Intent it = new Intent(HotelDisplay.this, Home.class);
                            startActivity(it);
                            super.onPostExecute(response);
                        }
                    };


                    register_hotel.execute();


                } else {
                    Toast.makeText(HotelDisplay.this, "Nothing to update", Toast.LENGTH_SHORT).show();
                }


            }
        });


        extra_facilities = new String[]{"wifi", "food", "tv", "gym", "buisness services", "coffee shop", "front desk", "spa", "laundry", "outdoor games", "parking", "room services", "swimming", "travel assistance"};
        extra_facilities_selected = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false};

        addMoreFacilities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(HotelDisplay.this);
                builder.setMultiChoiceItems(extra_facilities, extra_facilities_selected, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        if (isChecked) {
                            // If the user checked the item, add it to the selected items
                            extra_facilities_selected[which] = true;

                        } else if (extra_facilities_selected[which] == true) {
                            // Else, if the item is already in the array, remove it
                            extra_facilities_selected[which] = false;
                        }
                    }
                }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int hastrue = 0;
                        layout.removeAllViews();

                        for (int i = 0; i < extra_facilities_selected.length; i++) {
                            if (extra_facilities_selected[i] == true) {
                                hastrue = 1;

                                layout.setVisibility(View.VISIBLE);
                                ImageView image = new ImageView(HotelDisplay.this);
                                int id = R.drawable.ic_launcher;
                                if (i == 0)
                                    id = R.drawable.ic_wifi_black_24dp1;

                                else if (i == 1)
                                    id = R.drawable.ic_restaurant_menu_black_24dp;

                                else if (i == 2)
                                    id = R.drawable.ic_live_tv_black_24dp;

                                else if (i == 3)
                                    id = R.drawable.reg_hotel_gym;
                                else if (i == 4)
                                    id = R.drawable.businesservice;
                                else if (i == 5)
                                    id = R.drawable.coffeeshop;

                                else if (i == 6)
                                    id = R.drawable.frontdesk;
                                else if (i == 7)
                                    id = R.drawable.spa;
                                else if (i == 8)
                                    id = R.drawable.laundryservice;
                                else if (i == 9)
                                    id = R.drawable.outdoor;
                                else if (i == 10)
                                    id = R.drawable.parking;
                                else if (i == 11)
                                    id = R.drawable.roomservice;
                                else if (i == 12)
                                    id = R.drawable.reg_hotel_swim;
                                else if (i == 13)
                                    id = R.drawable.travel_assistance;

                                image.setImageResource(id);
                                layout.addView(image);
                            }
                        }
                        if (hastrue == 0)
                            layout.setVisibility(View.GONE);
                        dialog.dismiss();
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
            }
        });

        address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();
                if (edit_me2.equals(true)) {

                    Toast.makeText(HotelDisplay.this, "Please Wait...changing your hotel address", Toast.LENGTH_SHORT).show();
                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                    try {
                        startActivityForResult(builder.build(HotelDisplay.this), PLACE_PICKER_REQUEST);
                    } catch (GooglePlayServicesRepairableException e) {
                        e.printStackTrace();
                    } catch (GooglePlayServicesNotAvailableException e) {
                        e.printStackTrace();
                    }

                }
            }
        });

        edit_toolbr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit_me.equals(true)) {
                    edit_me = false;
                    edit_me2 = true;
                    hotelName.setEnabled(true);
                    hotelType.setEnabled(true);
                    no_of_rooms.setEnabled(true);
                    phone.setEnabled(true);
                    mobile.setEnabled(true);
                    address.setEnabled(true);
                    hotelPolicy.setEnabled(true);
                    hotel_view.setEnabled(true);
                    hotelRating.setEnabled(true);
                    update.setEnabled(true);
                    add_image.setEnabled(true);
                    add_policy.setEnabled(true);
                    adapter.enable();
                    policy_Adapter.enable();
                    addMoreFacilities.setEnabled(true);
                    update_me = true;
                    update.setVisibility(View.VISIBLE);
                    //update.setText("Update Details");
                    policy.setEnabled(true);
                    //update.setBackgroundColor(Color.GRAY);


                } else {
                    hotelName.setEnabled(false);
                    hotelType.setEnabled(false);
                    no_of_rooms.setEnabled(false);
                    phone.setEnabled(false);
                    mobile.setEnabled(false);
                    address.setEnabled(false);
                    hotelPolicy.setEnabled(false);
                    hotel_view.setEnabled(false);
                    hotelRating.setEnabled(false);
                    add_image.setEnabled(false);
                    add_policy.setEnabled(false);
                    addMoreFacilities.setEnabled(false);
                    edit_me = true;
                    edit_me2 = false;
                    adapter.disable();
                    policy_Adapter.disable();
                    update.setVisibility(View.GONE);
                    edit_toolbr.setText("EDIT");

                    // update.setText("");
                    // update.setBackgroundColor(Color.WHITE);
                    policy.setEnabled(false);
                    // update.setEnabled(false);
                }
            }
        });

        if (profile.getStringSet(Constants.REGISTERED_HOTELS, new HashSet<String>()).size() > 0) {
            registered_hotels_strings.addAll(profile.getStringSet(Constants.REGISTERED_HOTELS, new HashSet<String>()));

            Iterator iterator = registered_hotels_strings.iterator();
            while (iterator.hasNext()) {
                try {
                    registered_hotel_jsons.add(new JSONObject(iterator.next().toString()));
                } catch (JSONException e) {
                    Log.e("err", e.toString());
                }
            }


            for (int  i = 0; i < registered_hotel_jsons.size(); i++) {

                try {
                    Log.e("*****",i+"++++"+registered_hotel_jsons.get(i).getString(HotelRegistrationConstants.HOTEL_NAME));


                    if (hid.equals(registered_hotel_jsons.get(i).getString(AddroomConstants1.HOTEL_ID))) {
                        Log.e("res", registered_hotel_jsons.toString());
                        hotelName.setText(registered_hotel_jsons.get(i).getString(HotelRegistrationConstants.HOTEL_NAME));
                        hotelType.setText(registered_hotel_jsons.get(i).getString(HotelRegistrationConstants.HOTEL_TYPE));
                        no_of_rooms.setText(registered_hotel_jsons.get(i).getString(HotelRegistrationConstants.NO_OF_ROOMS));
                        phone.setText(registered_hotel_jsons.get(i).getString(HotelRegistrationConstants.PHONE_NUMBER));
                        mobile.setText(registered_hotel_jsons.get(i).getString(HotelRegistrationConstants.MOBILE_NUMBER));
                        address.setText(registered_hotel_jsons.get(i).getString(HotelRegistrationConstants.ADDRESS));
                        hotelRating.setRating(Integer.parseInt(registered_hotel_jsons.get(i).getString(HotelRegistrationConstants.STAR_HOTEL)));
                        add1 = registered_hotel_jsons.get(i).getString(HotelRegistrationConstants.ADDRESS_LINE1);
                        add2 = registered_hotel_jsons.get(i).getString(HotelRegistrationConstants.ADDRESS_LINE2);
                        // cty=registered_hotel_jsons.get(i).getString(HotelRegistrationConstants.CITY);

                        if (registered_hotel_jsons.get(i).getJSONObject("imagesS3").getJSONArray("name").length() > 0) {
                            for (int p = 0; p < registered_hotel_jsons.get(i).getJSONObject("imagesS3").getJSONArray("name").length(); p++) {

                               // imgd.addImage(new registeredHotels());

                                final int finalI = i;
                                final int finalP = p;
                                imageLoader.get("https://s3-us-west-2.amazonaws.com/wowimagesupload/resized/middle/"+registered_hotel_jsons.get(i).getJSONObject("imagesS3").getJSONArray("name").get(p).toString()+".jpg", new ImageLoader.ImageListener() {
                                    @Override
                                    public void onResponse(ImageLoader.ImageContainer response2, boolean isImmediate) {
//                                        holder.imageview.setImageDrawable(null);
//                                        holder.imageview.setImageBitmap(response.getBitmap());

                                        if(response2.getBitmap()!=null) {
                                            File file = new File(Environment.getExternalStorageDirectory().getPath(), "Mydhees/Images");
                                            if (!file.exists()) {
                                                file.mkdirs();
                                            }
                                            String filename = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
                                            FileOutputStream out;
                                            try {
                                                out = new FileOutputStream(filename);
                                                response2.getBitmap().compress(Bitmap.CompressFormat.JPEG, 80, out);
                                                adapter.addImage(new image(registered_hotel_jsons.get(finalI).getJSONObject("imagesS3").getJSONArray("description").get(finalP).toString(), filename));
                                                images_uris.add(new image(registered_hotel_jsons.get(finalI).getJSONObject("imagesS3").getJSONArray("description").get(finalP).toString(), filename));

                                            } catch (FileNotFoundException e) {
                                                e.printStackTrace();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            //adapter.addImage(new image(,));
                                        }
                                    }

                                    @Override
                                    public void onErrorResponse(com.android.volley.VolleyError error) {


                                    }
                                });
                                Log.e("ima", registered_hotel_jsons.get(i).getJSONObject("imagesS3").getJSONArray("name").get(p).toString());
                            }

                        } else {
                            imgd.addImage(new registeredHotels(""));
                            Toast.makeText(HotelDisplay.this, "here", Toast.LENGTH_SHORT).show();


                        }


                        JSONObject json = registered_hotel_jsons.get(i).getJSONObject(HotelRegistrationConstants.FACILITIES);

                        Iterator<String> iter = json.keys();

                        int o = 13;


                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                                40,
                                40);
                        lp.setMargins(5, 5, 5, 5);
                        while (iter.hasNext()) {
                            String key = iter.next();

                            extra_facilities_selected[o] = json.getBoolean(key);
                            // if(o>=0)
                            o--;
                            // else
                            // o=13;

                            try {


                                if (json.getBoolean(key)) {

                                    ImageView image = new ImageView(HotelDisplay.this);
                                    image.setMaxHeight(80);
                                    image.setMaxWidth(80);
                                    image.setLayoutParams(lp);
                                    image.setImageDrawable(ContextCompat.getDrawable(HotelDisplay.this, facilites[j]));
                                    //                                      image.setImageDrawable(getResources().(facilites[i]);
                                    layout.addView(image);
                                    facilites_images.add(facilites[j]);
                                }
                                j++;
                            } catch (JSONException e) {
                                // Something went wrong!
                            }
                        }
                        Log.e("images", facilites_images.toString());
                        Log.e("ectra fac", extra_facilities_selected.toString());


                        for (int a = 0; a < registered_hotel_jsons.get(i).getJSONArray(HotelRegistrationConstants.POLICIES).length(); a++) {
                            Log.e("length", registered_hotel_jsons.get(i).getJSONArray(HotelRegistrationConstants.POLICIES).length() + "");
                            policy_Adapter.addpolicy(registered_hotel_jsons.get(i).getJSONArray(HotelRegistrationConstants.POLICIES).get(a) + "");
                            Log.e("policy", registered_hotel_jsons.get(i).getJSONArray(HotelRegistrationConstants.POLICIES).get(a) + "");
                        }


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }

        if (profile.getStringSet(Constants.REGISTERED_ROOMS, new HashSet<String>()).size() > 0) {
            registered_rooms_strings.addAll(profile.getStringSet(Constants.REGISTERED_ROOMS, new HashSet<String>()));

            Iterator iterator1 = registered_rooms_strings.iterator();
            while (iterator1.hasNext()) {
                try {
                    registered_rooms_jsons.add(new JSONObject(iterator1.next().toString()));
                } catch (JSONException e) {
                    Log.e("err", e.toString());
                }
            }
            rtd.clearAll();
            for (int j = 0; j < registered_rooms_jsons.size(); j++) {
                try {

                    if (hid.equals(registered_rooms_jsons.get(j).getString(Release_constants.HOTEL_ID))) {
                        Log.e("roomstype", registered_rooms_jsons.get(j).getString(Release_constants.NO_OF_ROOMS) + registered_rooms_jsons.get(j).getString(Release_constants.ROOM_TYPE));

                        if (registered_rooms_jsons.get(j).getJSONObject("imagesS3").getJSONArray("name").length() > 0) {
                            rtd.displayroomType(new roomDisplay(registered_rooms_jsons.get(j).getString(Release_constants.NO_OF_ROOMS), registered_rooms_jsons.get(j).getString(Release_constants.ROOM_TYPE), registered_rooms_jsons.get(j).getString(Release_constants.ROOM_ID), registered_rooms_jsons.get(j).getJSONObject("imagesS3").getJSONArray("name").get(0).toString()));
                        } else {

                            rtd.displayroomType(new roomDisplay(registered_rooms_jsons.get(j).getString(Release_constants.NO_OF_ROOMS), registered_rooms_jsons.get(j).getString(Release_constants.ROOM_TYPE), registered_rooms_jsons.get(j).getString(Release_constants.ROOM_ID), ""));

                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }

    }


    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        getAddress2 = new LocationUtility(HotelDisplay.this);
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, HotelDisplay.this);
                String toastMsg = String.format("Place: %s", place.getName());
                Toast.makeText(HotelDisplay.this, toastMsg, Toast.LENGTH_LONG).show();


                editor.putFloat(Constants.LATITUDE, (float) place.getLatLng().latitude);
                editor.putFloat(Constants.LONGITUDE, (float) place.getLatLng().longitude);
                editor.commit();
                //street_address.setText(place.getAddress());

                try {
                    addressParts2.clear();
                    addressParts2.addAll(getAddress2.execute(place.getLatLng()).get());

                } catch (InterruptedException e) {
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                } catch (ExecutionException e) {
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                } finally {
                    //Toast.makeText(getApplicationContext(),addressParts2.toString(),Toast.LENGTH_SHORT).show();

                    locationAddress = addressParts2.get(0);
                    if (addressParts2.size() > 1) {
                        Log.e("a", addressParts2.toString());
                        locality = addressParts2.get(1);
                        country = addressParts2.get(2);
                        postal_code = addressParts2.get(3);
                        city = addressParts2.get(4);
                        //Toast.makeText(getApplicationContext(),postal_code,Toast.LENGTH_LONG).show();

                        locationAddress = locationAddress.replaceAll("null", "");
                        locationAddress = locationAddress.replaceAll(locality, "");
                    }
                    hotel_location = new LatLng(place.getLatLng().latitude, place.getLatLng().longitude);
                    if (!locationAddress.contains("Unable")) {

                        address.setText(locationAddress + "\n" + locality + " " + country);
                    }


                }


                //street_address2.setText(place.getLocale);
            }
        }


        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {


            uri = data.getData();


            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(HotelDisplay.this);
            alertDialog.setTitle("Description");
            alertDialog.setMessage("Enter Description");

            final EditText input = new EditText(HotelDisplay.this);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            input.setLayoutParams(lp);
            alertDialog.setView(input);
            alertDialog.setIcon(R.drawable.description);
            alertDialog.setCancelable(false);


            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            image_error = false;
                            image_description = input.getText().toString();
                            if (image_description.matches("")) {
                                Toast.makeText(HotelDisplay.this, "Description Required", Toast.LENGTH_SHORT).show();
                                image_error = true;
                            }
                            if (image_error == false) {
                                try {
                                    String filepath = new ImageCompressionAsyncTask(true, getApplicationContext()).execute(data.getDataString()).get();
                                    Log.e("filepath", filepath);
                                    //                                    Log.e("stringdata",data.getDataString());
                                    Log.e("uri", uri.toString());
                                    image im = new image(image_description, filepath);
                                    adapter.addImage(im);
                                    images_uris.add(im);
                                    Log.e("image_uris",images_uris.size()+"");

                                    dialog.dismiss();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                } catch (ExecutionException e) {
                                    e.printStackTrace();
                                }

                            }
                        }

                    });
            alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    hideSoftKeyboard();

                }
            });


            alertDialog.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
            alertDialog.show();


            //            }
        }
    }


    //For back button in toolbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    //
    //    @Override
    //    public boolean onCreateOptionsMenu(Menu menu) {
    //        // Inflate the menu; this adds items to the action bar if it is present.
    //        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
    //        return true;
    //    }
    //
    //    @Override
    //    public boolean onOptionsItemSelected(MenuItem item) {
    //        // Handle action bar item clicks here. The action bar will
    //        // automatically handle clicks on the Home/Up button, so long
    //        // as you specify a parent activity in AndroidManifest.xml.
    //        int id = item.getItemId();
    //
    //        //noinspection SimplifiableIfStatement
    //        if (id == R.id.share) {
    //
    //            return true;
    //        }
    //
    //        return super.onOptionsItemSelected(item);
    //    }


    private void hideSoftKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

        }


    }

}
