package com.mydhees.wow.partner.credits_tab;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.VolleySingleton;
import com.mydhees.wow.partner.allConstants.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jashan on 06-06-2016.
 */
public class Balance extends Fragment {

    public Balance() {
        // Required empty public constructor
    }
Button buy_points;
    SharedPreferences profile;
    SharedPreferences.Editor editor;
    TextView balance_points,promo_points,purchased_points,refer;

    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_balance, container, false);
        profile = getActivity().getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE, Context.MODE_PRIVATE);
        editor= profile.edit();
        volleySingleton=VolleySingleton.getinstance();
        requestQueue = volleySingleton.getrequestqueue();
    buy_points= (Button) v.findViewById(R.id.recharge);
        balance_points = (TextView)v.findViewById(R.id.tv_setpoints);
       promo_points = (TextView)v.findViewById(R.id.tv_set_promo_points);
       purchased_points = (TextView)v.findViewById(R.id.tv_setpointsp);
        refer= (TextView) v.findViewById(R.id.tv_refer);
hideKeyboard(getContext());
        refer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "Hey, am sending you WOW Money worth Rs. 100. Use my code "+profile.getString("referralCode","no code")+"  Download Now & enjoy Last minute deals. Download link http://www.mydhees.com/index.php ";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });


        try {
            checkPoints();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        buy_points.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(getActivity().getApplicationContext(),Recharge.class);
                                getActivity().startActivity(intent);
            }
        });





        return v;
    }


    private void checkPoints() throws JSONException {
        final ProgressDialog dialog = new ProgressDialog(getContext());
//        dialog.show();
        dialog.setTitle("Fetching details...");
        dialog.setMessage("Fetching details...");

        StringRequest request = new StringRequest(Request.Method.POST, Constants.GET_CREDITS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respons) {
//                        dialog.dismiss();
                        Log.e("response",respons.toString());
                        JSONObject response = null;
                        try {
                            response = new JSONObject(respons);
                           promo_points.setText(response.get("promotionalPoints").toString());
                           balance_points.setText(response.get("totalPoints").toString());
                           purchased_points.setText(response.get("purchasedPoints").toString());


                        } catch (JSONException e) {
                            Log.e("error",e.toString());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                dialog.dismiss();
                Toast.makeText(getContext(),"Internet Problem",Toast.LENGTH_LONG).show();
                Log.e("vollley error",error.toString());
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put(Constants.ID,profile.getString(Constants.ID,"default"));

                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }
    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
}
