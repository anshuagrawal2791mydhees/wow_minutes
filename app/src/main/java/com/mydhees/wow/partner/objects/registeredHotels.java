package com.mydhees.wow.partner.objects;

/**
 * Created by Jashan Kochar on 6/24/2016.
 */
public class registeredHotels {

    String hname,image_link,hid;
    public registeredHotels(String name,String image_link, String hid) {
        this.hname = name;
        this.hid=hid;
        this.image_link = image_link;
    }

    public registeredHotels(String image_link) {
        this.image_link = image_link;
    }

    public String getImage_link() {

        return image_link;
    }


    public String getName() {
        return hname;
    }
    public String getId() {
        return hid;
    }



}
