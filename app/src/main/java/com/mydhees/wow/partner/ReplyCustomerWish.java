package com.mydhees.wow.partner;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mydhees.wow.partner.allConstants.Constants;
import com.mydhees.wow.partner.allConstants.HotelRegistrationConstants;
import com.mydhees.wow.partner.allConstants.Release_constants;
import com.mydhees.wow.partner.drawer_fragments.AddRoom;
import com.mydhees.wow.partner.drawer_fragments.LeadsDashboard;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

public class ReplyCustomerWish extends AppCompatActivity {


    EditText checkInDate,checkInTime,checkOutDate,checkOutTime;

    Button send;
    EditText message,price,maxPrice,minPrice;
    Spinner hotelNameSpr,roomTypeSpr;
    String wid,cin,ctime,odate,otime,minp,mxp;
    VolleySingleton volleySingleton;
    RequestQueue requestQueue;
    SharedPreferences profile;
    SharedPreferences.Editor editor;
    HashMap<String,String> data = new HashMap<>();
    ArrayList<JSONObject> registered_rooms_jsons = new ArrayList<>();
    HashSet<String> registered_rooms_strings = new HashSet<>();

    int index;
    ArrayList<String> registered_hotels_names = new ArrayList<>();
    ArrayList<String> registered_hotels_ids = new ArrayList<>();
    ArrayList<String> registered_room_noofrooms = new ArrayList<>();
    ArrayList<String> registered_room_id = new ArrayList<>();
    ArrayList<String> registered_room_type = new ArrayList<>();
    ArrayList<String> registered_room_priceMRP = new ArrayList<>();
    ArrayList<Integer> hotel_index = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reply_customer_wish);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        checkInDate = (EditText)findViewById(R.id.checkin_dateWishCR);
        checkInTime = (EditText)findViewById(R.id.checkin_timeWishCR);
        checkOutDate = (EditText)findViewById(R.id.checkout_dateWishCR);
        checkOutTime = (EditText)findViewById(R.id.checkout_timeWishCR);



        profile = getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE, Context.MODE_PRIVATE);
        editor= profile.edit();
        volleySingleton = VolleySingleton.getinstance();
        requestQueue = volleySingleton.getrequestqueue();
        price = (EditText)findViewById(R.id.priceWish);
        message = (EditText)findViewById(R.id.wishMessage);
        send = (Button) findViewById(R.id.sendWishConfirm);
        hotelNameSpr = (Spinner)findViewById(R.id.hotelNameRC);
        roomTypeSpr = (Spinner)findViewById(R.id.roomTypeRC);

        maxPrice = (EditText)findViewById(R.id.maxPriceWR);
        minPrice = (EditText)findViewById(R.id.minPriceWR);

        //city spinner
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.hotel_type, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        //city spinner
        final ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
                R.array.room_types, android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

//hotels nd rooms
        if(profile.getStringSet(Constants.REGISTERED_ROOMS,new HashSet<String>()).size()>0) {
            registered_rooms_strings.addAll(profile.getStringSet(Constants.REGISTERED_ROOMS,new HashSet<String>()));

            Iterator iterator = registered_rooms_strings.iterator();
            while(iterator.hasNext())
            {
                try {
                    registered_rooms_jsons.add(new JSONObject(iterator.next().toString()));
                } catch (JSONException e) {
                    Log.e("err",e.toString());
                }
            }
            for(int i=0;i<registered_rooms_jsons.size();i++)
            {
                try {
                    registered_hotels_names.add(registered_rooms_jsons.get(i).getString(Release_constants.HOTEL_NAME));
                    registered_hotels_ids.add(registered_rooms_jsons.get(i).getString(Release_constants.HOTEL_ID));
                    registered_room_noofrooms.add(registered_rooms_jsons.get(i).getString(Release_constants.NO_OF_ROOMS));
                    registered_room_id.add(registered_rooms_jsons.get(i).getString(Release_constants.ROOM_ID));
                    registered_room_type.add(registered_rooms_jsons.get(i).getString(Release_constants.ROOM_TYPE));
                    registered_room_priceMRP.add(registered_rooms_jsons.get(i).getString(Release_constants.PRICE_MRP));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            Log.e("rid",registered_room_id.toString());
            Log.e("rt",registered_room_type.toString());
            Log.e("rprc",registered_room_priceMRP.toString());
            Log.e("hid",registered_hotels_ids.toString());
            Log.e("names",registered_hotels_names.toString());
            Log.e("nroom",registered_room_noofrooms.toString());

            HashSet<String> registered_hotels_set_spinner = new HashSet<>(registered_hotels_names);
            ArrayList<String> hotel_list = new ArrayList<>(registered_hotels_set_spinner);
            ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(ReplyCustomerWish.this, android.R.layout.simple_spinner_item,hotel_list
            );
            hotelNameSpr.setAdapter(adapter1);


        }
        else{
            AlertDialog.Builder builder = new AlertDialog.Builder(ReplyCustomerWish.this);
            builder.setCancelable(false);
            builder.setMessage("Add Rooms First.");
            builder.setPositiveButton("Add Rooms", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    FragmentManager mFragmentManager;
                    FragmentTransaction mFragmentTransaction;
                    mFragmentManager = getSupportFragmentManager();
                    mFragmentTransaction = mFragmentManager.beginTransaction().setCustomAnimations(R.anim.card_flip_left_out, R.anim.card_flip_right_in);//R.anim.card_flip_right_in,R.anim.card_flip_right_out,R.anim.card_flip_left_in,R.anim.card_flip_left_out);
                    mFragmentTransaction.replace(R.id.containerView,new AddRoom()).commit();

                }
            });

            builder.setNegativeButton("Home", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    FragmentManager mFragmentManager;
                    FragmentTransaction mFragmentTransaction;
                    mFragmentManager = getSupportFragmentManager();
                    mFragmentTransaction = mFragmentManager.beginTransaction().setCustomAnimations(R.anim.card_flip_left_out, R.anim.card_flip_right_in);//R.anim.card_flip_right_in,R.anim.card_flip_right_out,R.anim.card_flip_left_in,R.anim.card_flip_left_out);
                    mFragmentTransaction.replace(R.id.containerView,new LeadsDashboard()).commit();

                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();


        }
        hotelNameSpr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                hotel_index.clear();
                Log.e("abc",parent.getItemAtPosition(position)+"");
                for(int i=0;i<registered_hotels_names.size();i++){

                    if(registered_hotels_names.get(i).equals(parent.getItemAtPosition(position))){

                        hotel_index.add(i);

                    }


                }

                Log.e("abc11",hotel_index.toString());
                ArrayList<String> room_type=new ArrayList<String>();
                for(int i=0; i< hotel_index.size(); i++){

                    room_type.add(registered_room_type.get(hotel_index.get(i)));

                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(ReplyCustomerWish.this, android.R.layout.simple_spinner_item, room_type
                );

                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                roomTypeSpr.setAdapter(adapter);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        roomTypeSpr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                index=hotel_index.get(position);
                Log.e("indexx",index+"");




            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        Intent intent=getIntent();
        wid=intent.getStringExtra("wishId");
        minp=intent.getStringExtra("minp");
        mxp=intent.getStringExtra("maxp");
        cin=intent.getStringExtra("chkIn");
        ctime=intent.getStringExtra("chkInTime");
        odate=intent.getStringExtra("chkOut");
        otime=intent.getStringExtra("chkOutTime");

minPrice.setText(minp);
maxPrice.setText(mxp);
        checkInDate.setText(cin);
        checkInTime.setText(ctime);
        checkOutDate.setText(odate);
        checkOutTime.setText(otime);



        send.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        if(!(price.getText().toString().equals("") || message.getText().toString().equals(""))){

            try {
                reply();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else{
            Toast.makeText(ReplyCustomerWish.this, "Fill Details", Toast.LENGTH_SHORT).show();

        }


    }
});



    }


//for reply
private void reply() throws JSONException {
    final ProgressDialog dialog = new ProgressDialog(this);
    dialog.show();
    StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.REPLY_WISH,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String respons) {
                    JSONObject response;
                    dialog.dismiss();

                    try {
                        response = new JSONObject(respons);
                        Log.e("reply",response.toString());
                        Log.e("reply",registered_room_id.get(index));
                        Toast.makeText(ReplyCustomerWish.this, "Replied Successfully ", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(ReplyCustomerWish.this, Home.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                        startActivity(intent);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_LONG).show();
            Log.e("error",error.toString());
        }
    }
    ){
        @Override
        protected Map<String, String> getParams() throws AuthFailureError {
            Map<String,String> params = new HashMap<>();
            params.put("wishId",wid);
            params.put(HotelRegistrationConstants.OWNER_ID,profile.getString(Constants.ID,"default"));
            params.put("price",price.getText().toString());
            params.put("comment",message.getText().toString());
            params.put(Release_constants.ROOM_ID,registered_room_id.get(index));
            return params;
        }
    };

    requestQueue.add(stringRequest);

}




    //For back button in toolbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
