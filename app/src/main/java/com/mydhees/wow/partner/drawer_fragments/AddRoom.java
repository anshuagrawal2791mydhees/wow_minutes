package com.mydhees.wow.partner.drawer_fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.adapters.ImageRecyclerAdapter;
import com.mydhees.wow.partner.adapters.list_adapter;
import com.mydhees.wow.partner.allConstants.AddroomConstants1;
import com.mydhees.wow.partner.allConstants.Constants;
import com.mydhees.wow.partner.allConstants.HotelRegistrationConstants;
import com.mydhees.wow.partner.objects.image;
import com.mydhees.wow.partner.objects.room;
import com.mydhees.wow.partner.utils.ImageCompressionAsyncTask;
import com.mydhees.wow.partner.utils.task;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;

import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddRoom extends Fragment {


    public AddRoom() {
        // Required empty public constructor
    }

    ImageRecyclerAdapter adapter;

    private int PICK_IMAGE_REQUEST = 2;
    String image_description,ac_nonac;
    Uri uri;
    ArrayList<String> faciliites1 = new ArrayList<>();

    ImageView wifi,food,parking,pool,tv,gym,business_services,coffeeshop,frontdesk,laundary_services,outdoor,health_spa,room_service,spa,travel_asst;

    RecyclerView rv,rv2;
    Button add;
    EditText rtype,rprice,rnumbers;

    Button a,b,c,d;
    TextView adults, children;
    Button  add_image;
    ImageButton add_room;
    ImageView ac_btn,non_ac;
    TextView tx_ac_nonac;
    boolean isPressed=true;
    boolean isPressed2=true;
    AlertDialog.Builder builder;
    Spinner hotel_names_spin;
    list_adapter la;
    LinearLayoutManager mlinearlayoutmanager;

    boolean error = false,image_error=false;
    RecyclerView recycler2;
    AlertDialog alert;
    ScrollView scrollView;
    SharedPreferences profile;
    SharedPreferences.Editor editor;
    ArrayList<image> images_uris = new ArrayList<>();
//    Boolean[] facilites_array = new Boolean[14];
    task register_rooms;
    HashMap<String,String> data = new HashMap<>();
    ArrayList<JSONObject> registered_hotel_jsons = new ArrayList<>();
    HashSet<String> registered_hotels_strings = new HashSet<>();
    Boolean[] facilities_array = new Boolean[]{false,false,false,false};
    ArrayList<String> registered_hotels_ids = new ArrayList<>();
    ArrayList<String> registered_hotels_names = new ArrayList<>();

    ArrayList<String> registered_hotels_noofrooms = new ArrayList<>();
    Boolean ac = false;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =inflater.inflate(R.layout.fragment_add_room, container, false);
        profile = getActivity().getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE,Context.MODE_PRIVATE);
        editor= profile.edit();
      hideKeyboard(getContext());
        rv= (RecyclerView) v.findViewById(R.id.rv1);
        rv2=(RecyclerView) v.findViewById(R.id.rv2);
        hotel_names_spin= (Spinner) v.findViewById(R.id.spinner_hotel);
        scrollView = (ScrollView)v.findViewById(R.id.add_room_scroll);
        add= (Button) v.findViewById(R.id.addroom);
        add_room=(ImageButton) v.findViewById(R.id.add_room);
        add_image = (Button)v.findViewById(R.id.add_image);
        rprice = (EditText)v.findViewById(R.id.price);
        rtype = (EditText) v.findViewById(R.id.rtype1);
        rnumbers = (EditText) v.findViewById(R.id.room_no_addroom);
        adults=(TextView) v.findViewById(R.id.editText2);
        children=(TextView) v.findViewById(R.id.editText3);
        a=(Button)v.findViewById(R.id.imageButton);            //plus
        b=(Button)v.findViewById(R.id.imageButton2);           //mminus
        c=(Button)v.findViewById(R.id.imageButton3);           //plus
        d=(Button)v.findViewById(R.id.imageButton4);
        wifi= (ImageView) v.findViewById(R.id.wifi_room);
        wifi.setTag("wifi");
        food= (ImageView) v.findViewById(R.id.fa22);
        food.setTag("food");
        tv= (ImageView) v.findViewById(R.id.fa33);
        tv.setTag("tv");
        gym= (ImageView) v.findViewById(R.id.fa44);
        gym.setTag("gym");
        //minus
        rv.setHasFixedSize(false);
        mlinearlayoutmanager = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);


        rv2.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        adapter = new ImageRecyclerAdapter(getContext());
        rv2.setAdapter(adapter);

        if(profile.getStringSet(Constants.REGISTERED_HOTELS,new HashSet<String>()).size()>0) {
            registered_hotels_strings.addAll(profile.getStringSet(Constants.REGISTERED_HOTELS,new HashSet<String>()));
//            hotel_names.addAll(profile.getStringSet(Constants.REGISTERED_HOTELS, new HashSet<String>()));
//            for(int i=0;i<registered_hotels.size();i++)
//                hotel_jsons.add(new JSONObject(registered_hotels))
            Iterator iterator = registered_hotels_strings.iterator();
            while(iterator.hasNext())
            {
                try {
                    registered_hotel_jsons.add(new JSONObject(iterator.next().toString()));
                } catch (JSONException e) {
                    Log.e("err",e.toString());
                }
            }
            for(int i=0;i<registered_hotel_jsons.size();i++)
            {
                try {
                    registered_hotels_names.add(registered_hotel_jsons.get(i).getString(HotelRegistrationConstants.HOTEL_NAME));
                    registered_hotels_ids.add(registered_hotel_jsons.get(i).getString(AddroomConstants1.HOTEL_ID));
                    registered_hotels_noofrooms.add(registered_hotel_jsons.get(i).getString(HotelRegistrationConstants.NO_OF_ROOMS));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item,registered_hotels_names
                    );
            hotel_names_spin.setAdapter(adapter);
        }
        else{
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setCancelable(false);
            builder.setMessage("Register Hotel First Please.");
            builder.setPositiveButton("Go to Hotel Register", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    FragmentManager mFragmentManager;
                    FragmentTransaction mFragmentTransaction;
                    mFragmentManager = getActivity().getSupportFragmentManager();
                    mFragmentTransaction = mFragmentManager.beginTransaction().setCustomAnimations(R.anim.card_flip_left_out,R.anim.card_flip_right_in);//R.anim.card_flip_right_in,R.anim.card_flip_right_out,R.anim.card_flip_left_in,R.anim.card_flip_left_out);
                    mFragmentTransaction.replace(R.id.containerView,new RegisterHotel()).commit();

                }
            });
            builder.setNegativeButton("Home", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    FragmentManager mFragmentManager;
                    FragmentTransaction mFragmentTransaction;
                    mFragmentManager = getActivity().getSupportFragmentManager();
                    mFragmentTransaction = mFragmentManager.beginTransaction().setCustomAnimations(R.anim.card_flip_left_out,R.anim.card_flip_right_in);//R.anim.card_flip_right_in,R.anim.card_flip_right_out,R.anim.card_flip_left_in,R.anim.card_flip_left_out);
                    mFragmentTransaction.replace(R.id.containerView,new LeadsDashboard()).commit();

                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();


        }

        add_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(adapter.getImageUris().size()<=2) {
//                    Intent intent = new Intent();
//                    intent.setType("image/*");
//                    intent.setAction(Intent.ACTION_GET_CONTENT);
//                    startActivityForResult(intent, PICK_IMAGE_REQUEST);
                    Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i,PICK_IMAGE_REQUEST);
                }
                else
                    Toast.makeText(getContext(),"Limit is 3",Toast.LENGTH_LONG).show();

            }
        });



        // Ac nonAc Work
        ac_btn = (ImageView) v.findViewById(R.id.ac_btn);
        non_ac = (ImageView)v.findViewById(R.id.nonac_btn);

        ac_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isPressed==true){
                    ImageView view = (ImageView)v;
                    view.setColorFilter(Color.parseColor("#009688"));
                    non_ac.setColorFilter(Color.GRAY);
                    isPressed=false;
                    ac_nonac="AC";
                    isPressed2=true;


                }

                else{
                    ImageView view = (ImageView)v;
                    view.setColorFilter(Color.GRAY);
                    ac_nonac="";
                    isPressed=true;
                }


            }
        });


        non_ac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isPressed2==true){
                    ImageView view = (ImageView)v;
                    view.setColorFilter(Color.parseColor("#009688"));
                    ac_btn.setColorFilter(Color.GRAY);
                    isPressed2=false;
                    ac_nonac="Non-AC";
                    isPressed=true;

                }

                else{
                    ImageView view = (ImageView)v;
                    view.setColorFilter(Color.GRAY);
                    ac_nonac="";
                    isPressed2=true;

                }

            }
        });


        ImageView.OnClickListener onClick = new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!faciliites1.contains(v.getTag().toString()))
                {
                    faciliites1.add(v.getTag().toString());
                    ImageView view = (ImageView)v;
                    view.setColorFilter(Color.parseColor("#009688"));

                }
                else {
                    faciliites1.remove(v.getTag().toString());
                    ImageView view = (ImageView)v;
                    view.setColorFilter(Color.GRAY);
                }

            }
        };
        wifi.setOnClickListener(onClick);
        food.setOnClickListener(onClick);
        tv.setOnClickListener(onClick);
        gym.setOnClickListener(onClick);
        rv.setLayoutManager(mlinearlayoutmanager);

        la=new list_adapter(getContext());
        rv.setAdapter(la);

        add_room.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                error = false;

                if(rtype.getText().toString().equals("")){

                    rtype.setError("Required");
                    error=true;
                }
                if(rprice.getText().toString().equals(""))
                {
                    rprice.setError("Required");
                    error = true;
                }
                if(rnumbers.getText().toString().equals(""))
                {
                    rnumbers.setError("Required");
                    error = true;
                }
                if(adults.getText().toString().equals("0")&&children.getText().toString().equals("0"))
                {
                    Toast.makeText(getContext(),"Select at least one person",Toast.LENGTH_LONG).show();
                    error=true;
                }
                if(error == false)
                {
//                    if(la.getItemCount()==0)
//                        rv.setVisibility(View.VISIBLE);





                }


            }
        });



        a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count= Integer.parseInt(adults.getText().toString());
                int count1 = count + 1;
                String a= Integer.toString(count1);
                adults.setText(a);


            }
        });

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count= Integer.parseInt(adults.getText().toString());
                if(count==0){

                    Toast.makeText(getContext(), "Already 0!!! Can't be less.", Toast.LENGTH_SHORT).show();

                }
                else {
                    int count1 = count - 1;
                    String a= Integer.toString(count1);
                    adults.setText(a);
                }
            }
        });

        c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count= Integer.parseInt(children.getText().toString());

                int count1=count+1;
                String a= Integer.toString(count1);
                children.setText(a);

            }
        });

        d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count= Integer.parseInt(children.getText().toString());
                if(count==0){

                    Toast.makeText(getContext(), "Already 0!!! Can't be less.", Toast.LENGTH_SHORT).show();

                }
                else {
                    int count1 = count - 1;
                    String a= Integer.toString(count1);
                    children.setText(a);

                }
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(faciliites1.contains("wifi"))
                    facilities_array[0]=true;
                if(faciliites1.contains("food"))
                    facilities_array[1]=true;
                if(faciliites1.contains("tv"))
                    facilities_array[2]=true;
                if(faciliites1.contains("gym"))
                    facilities_array[3]=true;

                String[] facilites_keys = new String[]{HotelRegistrationConstants.HAS_WIFI,
                        HotelRegistrationConstants.HAS_DINING,
                        HotelRegistrationConstants.HAS_TV,
                        HotelRegistrationConstants.HAS_GYM,
                       };

                final JSONObject facilitesJSON=new JSONObject();
                for(int i=0;i<4;i++)
                {
                    try {
                        facilitesJSON.put(facilites_keys[i],facilities_array[i]);
                    } catch (JSONException e) {
                        Log.e("error",e.toString());
                    }
                }
                if(ac_nonac.equals("AC"))
                    ac = true;
                else
                ac = false;

                la.addroom(new room(rtype.getText().toString(),rprice.getText().toString(),registered_hotels_names.get(hotel_names_spin.getSelectedItemPosition()),rnumbers.getText().toString(),ac_nonac,adults.getText().toString(),children.getText().toString(),adapter.getImageUris(),facilities_array));
//                    rv.smoothScrollToPosition(0);


                builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Preview");
                //    builder.setMessage("Release Offers?");

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                //lp.setMargins(5,100,5,100);
                lp.setMargins(0,100,5,100);
                recycler2 = new RecyclerView(getContext());
                recycler2.setLayoutManager(new LinearLayoutManager(getContext()));
                recycler2.setAdapter(la);
                //recycler2.setLayoutParams(lp);
                LinearLayout layout = new LinearLayout(getContext());
                layout.setOrientation(LinearLayout.VERTICAL);
                layout.setLayoutParams(lp);
                //layout.setLayoutParams(lp);
                layout.addView(recycler2);



                builder.setView(layout);
                builder.setCancelable(false);

                builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        data.put(AddroomConstants1.HOTEL_ID,registered_hotels_ids.get(hotel_names_spin.getSelectedItemPosition()));
                        data.put(HotelRegistrationConstants.OWNER_ID,profile.getString(Constants.ID,"default"));
                        data.put(AddroomConstants1.HOTEL_NAME,registered_hotels_names.get(hotel_names_spin.getSelectedItemPosition()));
                        data.put(AddroomConstants1.NO_OF_ROOMS,rnumbers.getText().toString());
                        data.put(AddroomConstants1.FACILITIES,facilitesJSON.toString());
                        data.put(AddroomConstants1.ROOM_TYPE,rtype.getText().toString());
                        data.put(AddroomConstants1.PRICE_MRP,rprice.getText().toString());
                        data.put(AddroomConstants1.AC,ac.toString());
                        data.put(AddroomConstants1.ADULTS,adults.getText().toString());
                        data.put(AddroomConstants1.CHILDREN,children.getText().toString());
                        data.put(Constants.EMAIL,profile.getString(Constants.EMAIL,"default"));

                        final ProgressDialog dialog2 = new ProgressDialog(getContext());
                        dialog2.show();
                        register_rooms = new task(getContext(),images_uris, Constants.ROOM_REGISTER_URL,data){
                            @Override
                            protected void onPostExecute(Response response) {
                                dialog2.dismiss();
                                rtype.setText("");
                                rprice.setText("");
                                rnumbers.setText("");
                                Toast.makeText(getContext(),"Congratulations! Mentioned  rooms are registered.",Toast.LENGTH_LONG).show();

//
                                HashSet<String> registered_rooms = new HashSet<String>();
//                            HashSet<String> registered_hotels_names = new HashSet<String>();
//                            HashSet<String> registered_hotels_no_of_rooms = new HashSet<String>();

                                try {
                                    JSONObject resp = new JSONObject(response.body().string());
                                    Log.e("respJSOn",resp.toString());
                                    registered_rooms.add(resp.toString());
//                                registered_hotels_ids.add(resp.getString("hotelId"));
//                                registered_hotels_names.add(resp.getString("hotelName"));
//                                registered_hotels_no_of_rooms.add(resp.getString("noOfRooms"));
                                } catch (JSONException e) {
                                    Log.e("err",e.toString());
                                } catch (IOException e) {
                                    Log.e("err",e.toString());
                                }

                                if(profile.getStringSet(Constants.REGISTERED_ROOMS, new HashSet<String>()).size()>0){
                                    Log.e("storing","not_first");
                                    //Log.e("data",profile.getStringSet(Constants.REGISTERED_HOTELS_NAMES,null).toString());
                                    registered_rooms.addAll(profile.getStringSet(Constants.REGISTERED_ROOMS,null));

//                                registered_hotels_ids.addAll(profile.getStringSet(Constants.REGISTERED_HOTELS_IDS,null));
//                                registered_hotels_names.addAll(profile.getStringSet(Constants.REGISTERED_HOTELS_NAMES,null));
//                                registered_hotels_no_of_rooms.addAll(profile.getStringSet(Constants.REGISTERED_HOTELS_NO_OF_ROOMS,null));
                                }
                                else {
                                    Log.e("storing","first");
                                }
                                Log.e("rooms",registered_rooms.toString());

                                editor.putStringSet(Constants.REGISTERED_ROOMS, registered_rooms);
//                            editor.putStringSet(Constants.REGISTERED_HOTELS_NAMES, registered_hotels_names);
//                            editor.putStringSet(Constants.REGISTERED_HOTELS_NO_OF_ROOMS, registered_hotels_no_of_rooms);
                                editor.commit();
                                mFragmentManager = getActivity().getSupportFragmentManager();
                                mFragmentTransaction = mFragmentManager.beginTransaction().setCustomAnimations(R.anim.card_flip_left_out,R.anim.card_flip_right_in);//R.anim.card_flip_right_in,R.anim.card_flip_right_out,R.anim.card_flip_left_in,R.anim.card_flip_left_out);
                                mFragmentTransaction.replace(R.id.containerView,new LeadsDashboard()).commit();
                                super.onPostExecute(response);
                            }
                        };


                        register_rooms.execute();




                        alert.dismiss();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        rv.scrollToPosition(0);
                        la.clearAll();
                        alert.dismiss();
                    }
                });

                alert = builder.create();
                alert.show();

                rv.scrollToPosition(0);
                rv2.scrollToPosition(0);
                adapter.clearAll();
            }
        });

        return v;
    }
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == getActivity().RESULT_OK && data != null && data.getData() != null) {


            uri = data.getData();


            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("Description");
            alertDialog.setMessage("Enter Description");

            final EditText input = new EditText(getContext());
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            input.setLayoutParams(lp);
            alertDialog.setView(input);
            alertDialog.setIcon(R.drawable.description);
            alertDialog.setCancelable(false);


            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            image_error=false;
                            image_description = input.getText().toString();
                            if (image_description.matches("")) {
                                Toast.makeText(getContext(), "Description Required", Toast.LENGTH_SHORT).show();
                                image_error  = true;
                            }
                            if(image_error==false){
                                try {
                                    String filepath = new ImageCompressionAsyncTask(true,getContext()).execute(data.getDataString()).get();
                                    image im = new image(image_description,filepath);
                                    adapter.addImage(im);
                                    images_uris.add(im);
//                                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
//                                View view = getActivity().getCurrentFocus();
//                                imm.hideSoftInputFromWindow(view.getWindowToken(),
//                                        InputMethodManager.RESULT_UNCHANGED_SHOWN);
                                    dialog.dismiss();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                } catch (ExecutionException e) {
                                    e.printStackTrace();
                                }


                            }
                        }

                    });
            alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    focusOnView(add);
                    hideSoftKeyboard();

                }
            });


            alertDialog.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
            alertDialog.show();



        }
    }

    @Override
    public void onResume() {
        super.onResume();
        hideSoftKeyboard();
    }
    private final void focusOnView(final View view) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0, view.getBottom());
            }
        });
    }
    private void hideSoftKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
//            focusOnView(policy1);
        }


    }

    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

}
