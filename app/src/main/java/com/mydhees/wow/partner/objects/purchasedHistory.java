package com.mydhees.wow.partner.objects;

import android.media.Image;

import java.util.Date;

/**
 * Created by Abhishek on 11-Jul-16.
 */

public class purchasedHistory {
    String amount,orderId,transationID,status,paymentType;
    Date date;
    Image image;

    public purchasedHistory(Date date, String amount, String orderId, String transationID, String status, String paymentType) {
        this.date = date;
        this.amount = amount;
        this.orderId = orderId;
        this.transationID = transationID;
        this.status = status;
        this.paymentType = paymentType;

    }

    public Date getDate() {
        return date;
    }

    public String getAmount() {
        return amount;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getTransationID() {
        return transationID;
    }

    public String getStatus() {
        return status;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public Image getImage() {
        return image;
    }
}
