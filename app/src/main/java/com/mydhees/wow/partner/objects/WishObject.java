package com.mydhees.wow.partner.objects;

import java.util.Date;

/**
 * Created by Abhishek on 18-Aug-16.
 */

public class WishObject {
    String address,minPrice,maxPrice,serious,rooms,adults,child,wishid;
    Date date_checkin;
    Date date_check_out;
    public String getRooms() {
        return rooms;
    }

    public String getAdults() {
        return adults;
    }



    public String getChild() {
        return child;
    }

    public String getWishid() {
        return wishid;
    }

    public WishObject(String serious, String address, String minPrice, String maxPrice, String room, String adults, String child, String WishId,Date chkIn,Date chkOut) {
        this.serious = serious;
        this.address = address;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.rooms = room;
        this.adults = adults;
        this.child = child;
        this.wishid = WishId;
        this.date_checkin=chkIn;
        this.date_check_out=chkOut;
    }

    public String getSerious() {
        return serious;
    }

    public String getAddress() {
        return address;
    }

    public String getMinPrice() {
        return minPrice;
    }

    public Date getDate_checkin() {
        return date_checkin;
    }

    public Date getDate_check_out() {
        return date_check_out;
    }
    public String getMaxPrice() {
        return maxPrice;
    }
}
