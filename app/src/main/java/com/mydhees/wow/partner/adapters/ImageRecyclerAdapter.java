package com.mydhees.wow.partner.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.objects.image;
import com.mydhees.wow.partner.utils.ImageDimensionUtility;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by anshu on 19/05/16.
 */
public class  ImageRecyclerAdapter extends RecyclerView.Adapter<ImageRecyclerAdapter.myviewholder> {
    Context context;
    LayoutInflater inflater;
    ArrayList<image> images = new ArrayList<>();
    int flag=1;

    public ImageRecyclerAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public void addImage(image recieved_image)
    {
        images.add(recieved_image);
        notifyItemInserted(images.size());
    }
    public void clearAll()
    {
        images.clear();
    }
    public ArrayList<String> getImageUris(){
        ArrayList<String> imageuris = new ArrayList<>();
        for(int i=0;i<images.size();i++)
        imageuris.add(images.get(i).getUrl());

        return imageuris;
    }

    @Override
    public ImageRecyclerAdapter.myviewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.image_recycler_row,parent,false);
        myviewholder viewholder = new myviewholder(v);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(ImageRecyclerAdapter.myviewholder holder, final int position) {
        image current_image = images.get(position);
        //            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(),current_image.getUrl());
//        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(current_image.getUrl());
//            Bitmap bitmap =
        // Log.d(TAG, String.valueOf(bitmap));
        holder.imageview.setImageBitmap(bitmap);
        holder.title.setText(current_image.getTitle());
        holder.del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                images.remove(position);
                notifyDataSetChanged();
            }
        });
        if(flag==0)
            holder.del.setVisibility(View.INVISIBLE);
        else
            holder.del.setVisibility(View.VISIBLE);

    }

    @Override
    public int getItemCount() {
        return images.size();
    }


    public void disable() {
        flag=0;
        notifyDataSetChanged();
        Log.e("flag",flag+"");
    }
    public void enable(){
        flag=1;
        notifyDataSetChanged();
        Log.e("flag",flag+"");
    }
    public ArrayList<image> get_images(){
        Log.e("im size",images.size()+"");
        return images;
    }

    static class myviewholder extends RecyclerView.ViewHolder{
         TextView title;
        ImageView imageview;
        Button del;

        public myviewholder(View itemView) {
            super(itemView);
            title = (TextView)itemView
                    .findViewById(R.id.title);
            imageview = (ImageView)itemView
                    .findViewById(R.id.image);
            del = (Button)itemView.findViewById(R.id.del_image);
        }
    }
}
