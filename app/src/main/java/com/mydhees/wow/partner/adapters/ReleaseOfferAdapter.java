package com.mydhees.wow.partner.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.mydhees.wow.partner.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import com.mydhees.wow.partner.objects.offer;

/**
 * Created by anshu on 19/05/16.
 */
public class ReleaseOfferAdapter extends RecyclerView.Adapter<ReleaseOfferAdapter.myviewholder> {
    Context context;
    LayoutInflater inflater;
    ArrayList<offer> offers = new ArrayList<>();
    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    DateFormat timeFormat = new SimpleDateFormat("HH:mm");


    public ReleaseOfferAdapter(Context context) {
        this.context=context;
        inflater=LayoutInflater.from(context);
    }

    public void addoffer(offer recievedoffer)
    {
        offers.add(0,recievedoffer);
        notifyItemInserted(0);
    }
    public ArrayList<offer> getofferlist()
    {
        return offers;
    }
    public void clearAll()
    {
        offers.clear();
    }

    @Override
    public myviewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.release_offer_recycler_row,parent,false);
        myviewholder viewholder = new myviewholder(v);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(myviewholder holder, final int position) {
        String[] rooms ={"Luxury","King","Single","Double"};
        offer current_offer = offers.get(position);
        holder.no_rooms_tv.setText(current_offer.getRoomno()+"");
        holder.sales_price_tv.setText(current_offer.getPrice()+"");
        holder.offername.setText(current_offer.getOffer_name()+"");
        holder.hname.setText(current_offer.getHotel_name()+"");
        holder.check_in_time_tv.setText(timeFormat.format(current_offer.getDate_checkin()));
        holder.check_in_tv.setText(dateFormat.format(current_offer.getDate_checkin()));
        holder.check_out_time_tv.setText(timeFormat.format(current_offer.getDate_check_out()));
        holder.check_out_tv.setText(dateFormat.format(current_offer.getDate_check_out()));
        holder.schedule.setText(dateFormat.format(current_offer.getSchedule()));
        holder.stime.setText(timeFormat.format(current_offer.getSchedule()));
        holder.exp.setText(timeFormat.format(current_offer.getExpire_date()));
        holder.expirytime.setText(current_offer.getExpire());
        holder.room_type_tv.setText(current_offer.getRoomtype());


    }



    @Override
    public int getItemCount() {
        return offers.size();
    }

    static class myviewholder extends RecyclerView.ViewHolder
    {
        EditText room_type_tv,no_rooms_tv,sales_price_tv,hname;
        EditText check_in_tv,check_in_time_tv,offername;
        EditText check_out_tv,check_out_time_tv,schedule,stime,expirytime,exp;
        // Button delete;


        public myviewholder(View itemView) {
            super(itemView);
            room_type_tv = (EditText)itemView.findViewById(R.id.room_type_tv);
            offername = (EditText)itemView.findViewById(R.id.offername_tv_rel);
            expirytime = (EditText)itemView.findViewById(R.id.expireTime_tv);
            exp = (EditText)itemView.findViewById(R.id.expireTime2_tv);
            hname = (EditText)itemView.findViewById(R.id.hotelname1_tv_rel);
            no_rooms_tv = (EditText)itemView.findViewById(R.id.no_rooms_tv);
            sales_price_tv = (EditText)itemView.findViewById(R.id.sales_price_tv);
            check_in_tv = (EditText)itemView.findViewById(R.id.check_in_tv);
            check_in_time_tv = (EditText)itemView.findViewById(R.id.check_in_time_tv);
            check_out_tv = (EditText)itemView.findViewById(R.id.check_out_tv);
            check_out_time_tv = (EditText)itemView.findViewById(R.id.check_out_time_tv);
            schedule = (EditText)itemView.findViewById(R.id.scheduledate_tv_rel);
            stime = (EditText)itemView.findViewById(R.id.schedule_time_tv_rel);
            //  delete = (Button)itemView.findViewById(R.id.delete);
        }
    }
}
