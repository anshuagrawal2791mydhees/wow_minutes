package com.mydhees.wow.partner.objects;

/**
 * Created by anshu on 13/05/16.
 */
public class service {
    String name;
    boolean selected;
    int image_id;

    public void setImage_id(int image_id) {
        this.image_id = image_id;
    }

    public int getImage_id() {

        return image_id;
    }

    public service(String name, boolean selected, int image_id) {
        this.name = name;
        this.selected = selected;
        this.image_id = image_id;
    }

    public service(String name, boolean selected) {
        this.name = name;
        this.selected = selected;
    }

    public String getName() {
        return name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public String toString() {
        return "service{" +
                "name='" + name + '\'' +
                ", selected=" + selected +
                ", image_id=" + image_id +
                '}';
    }
}
