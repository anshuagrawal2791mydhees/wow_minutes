package com.mydhees.wow.partner.objects;

import java.util.Date;

/**
 * Created by Abhishek on 01-Aug-16.
 */

public class convertedObject {
    String offerName,wowPrice,name,email,phone,status,bookingId,roomType,hotelName;
    Date checkIn,checkOut,bookingDate;

    public convertedObject(String hotelName) {
        this.hotelName = hotelName;
    }

    public convertedObject(String bookingId, String roomType, Date bookingDate) {
        this.bookingId = bookingId;
        this.roomType = roomType;
        this.bookingDate = bookingDate;

    }

    public convertedObject(String offerName, String wowPrice, String name, String email, String phone, String status, Date checkIn, Date checkOut,String bookingId, String roomType, Date bookingDate,String hotelName) {
        this.offerName = offerName;
        this.wowPrice = wowPrice;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.status = status;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.bookingId = bookingId;
        this.roomType = roomType;
        this.bookingDate = bookingDate;
        this.hotelName = hotelName;
    }

    public String getOfferName() {
        return offerName;
    }

    public String getWowPrice() {
        return wowPrice;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getStatus() {
        return status;
    }

    public Date getCheckIn() {
        return checkIn;
    }

    public String getHotelName() {
        return hotelName;
    }

    public String getBookingId() {
        return bookingId;
    }

    public String getRoomType() {
        return roomType;
    }

    public Date getBookingDate() {
        return bookingDate;
    }

    public Date getCheckOut() {
        return checkOut;

    }
}
