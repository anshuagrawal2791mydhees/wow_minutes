package com.mydhees.wow.partner.allConstants;

/**
 * Created by Jashan Kochar on 6/17/2016.
 */
public class AddroomConstants1 {

    public static final String HOTEL_ID="hotelId";
    public static final String HOTEL_NAME="hotelName";
    public static final String NO_OF_ROOMS="noOfRooms";
    public static final String FACILITIES="facilities";
    public static final String STAR_HOTEL="starHotel";
    public static final String ROOM_TYPE="roomType";
    public static final String PRICE_MRP="priceMRP";
    public static final String SALE_PRICE="salePrice";
    public static final String WOW_PRICE="wowPrice";
    public static final String AC="ac";
    public static final String ADULTS="adults";
    public static final String CHILDREN="children";



}
