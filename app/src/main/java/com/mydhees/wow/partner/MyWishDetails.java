package com.mydhees.wow.partner;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MyWishDetails extends AppCompatActivity {

    TextView name,checkInDate,checkInTime,checkOutDate,checkOutTime,minPrice,maxPrice,adults,children,seriousness,rooms;
    String minp,maxp,serious,noOfrooms,child,adult,city,wishid,chkIn,chkInTime,chkOut,chkOutTime;
    Button next;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wish_details);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        name = (TextView)findViewById(R.id.nameWish);
        rooms = (TextView)findViewById(R.id.roomWish);
        checkInDate = (TextView) findViewById(R.id.checkin_dateWish);
        checkInTime = (TextView) findViewById(R.id.checkin_timeWish);
        checkOutDate = (TextView) findViewById(R.id.checkout_dateWish);
        checkOutTime = (TextView) findViewById(R.id.checkout_timeWish);
        minPrice = (TextView) findViewById(R.id.minPriceWish);
        maxPrice = (TextView) findViewById(R.id.maxPriceWish);
        adults = (TextView) findViewById(R.id.no_adults);
        children = (TextView) findViewById(R.id.no_children);
        seriousness = (TextView)findViewById(R.id.seriousWish);
        next = (Button)findViewById(R.id.next);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent send=new Intent(MyWishDetails.this,ReplyCustomerWish.class);

                send.putExtra("wishId",wishid);
                send.putExtra("minp",minp);
                send.putExtra("maxp",maxp);
                send.putExtra("chkIn",chkIn);
                send.putExtra("chkInTime",chkInTime);
                send.putExtra("chkOut",chkOut);
                send.putExtra("chkOutTime",chkOutTime);

                startActivity(send);
            }
        });

        Intent intent=getIntent();
        minp=intent.getStringExtra("minPrice");
        maxp=intent.getStringExtra("maxPrice");
        serious=intent.getStringExtra("serious");
        noOfrooms=intent.getStringExtra("rooms");
        child=intent.getStringExtra("child");
        adult=intent.getStringExtra("adults");
        city=intent.getStringExtra("city");
        wishid=intent.getStringExtra("wishid");
       chkIn= intent.getStringExtra("cin");
        chkInTime=intent.getStringExtra("cinTime");
        chkOut=intent.getStringExtra("cout");
        chkOutTime=intent.getStringExtra("coutTime");


        name.setText(city);
        rooms.setText(noOfrooms);
        minPrice.setText(minp);
        maxPrice.setText(maxp);
        adults.setText(adult);
        children.setText(child);
        seriousness.setText(serious);
        checkInDate.setText(chkIn);
        checkInTime.setText(chkInTime);
        checkOutDate.setText(chkOut);
        checkOutTime.setText(chkOutTime);



    }
    //For back button in toolbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
