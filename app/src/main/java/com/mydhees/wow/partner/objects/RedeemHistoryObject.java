package com.mydhees.wow.partner.objects;

import java.util.Date;

/**
 * Created by anshu on 06/10/16.
 */

public class RedeemHistoryObject {
    private int amount;
    private Date date;
    private String type;  //credit or debit
    private String order_id; //for credit
    private String offer_name,hotel_name,offer_id; //for debit

    public RedeemHistoryObject(int amount, Date date, String type, String order_id) {
        this.amount = amount;
        this.date = date;
        this.type = type;
        this.order_id = order_id;
    }

    public RedeemHistoryObject(int amount, Date date, String type, String offer_name, String hotel_name,String offer_id) {
        this.amount = amount;
        this.date = date;
        this.type = type;
        this.offer_name = offer_name;
        this.hotel_name = hotel_name;
        this.offer_id=offer_id;
    }

    public String getOffer_id() {
        return offer_id;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public void setOffer_name(String offer_name) {
        this.offer_name = offer_name;
    }

    public void setHotel_name(String hotel_name) {
        this.hotel_name = hotel_name;
    }

    public int getAmount() {
        return amount;
    }

    public Date getDate() {
        return date;
    }

    public String getType() {
        return type;
    }

    public String getOrder_id() {
        return order_id;
    }

    public String getOffer_name() {
        return offer_name;
    }

    public String getHotel_name() {
        return hotel_name;
    }
}
