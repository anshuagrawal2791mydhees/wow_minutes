package com.mydhees.wow.partner.leads_dashboard_tabs;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.VolleySingleton;
import com.mydhees.wow.partner.adapters.ReleaseOfferAdapterJ;
import com.mydhees.wow.partner.allConstants.Constants;
import com.mydhees.wow.partner.allConstants.HotelRegistrationConstants;
import com.mydhees.wow.partner.allConstants.Release_constants;
import com.mydhees.wow.partner.objects.offer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.TimeZone;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Released.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Released#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Released extends Fragment {

    RecyclerView recycler ;
    VolleySingleton volleySingleton;
    RequestQueue requestQueue;
    ReleaseOfferAdapterJ myadapter;

    ArrayList<JSONObject> registered_offers_jsons = new ArrayList<>();
    HashSet<String> registered_offers_strings = new HashSet<>();
    CardView sortByCard;
    Spinner sortBy;



    DateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");


    SharedPreferences profile;
    SharedPreferences.Editor editor;
    ArrayList<offer> offerlist;





    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private OnFragmentInteractionListener mListener;

    public Released() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter one.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Released.
     */
    // TODO: Rename and change types and number of parameters
    public static Released newInstance(String param1, String param2) {
        Released fragment = new Released();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_new_leads, container, false);
hideKeyboard(getContext());
        volleySingleton = VolleySingleton.getinstance();
        requestQueue = volleySingleton.getrequestqueue();
        profile = getActivity().getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE, Context.MODE_PRIVATE);
        editor= profile.edit();
        recycler = (RecyclerView)v.findViewById(R.id.recycler);
        myadapter=new ReleaseOfferAdapterJ(getContext());
        recycler.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        recycler.setAdapter(myadapter);
        recycler.setVisibility(View.VISIBLE);
        recycler.setNestedScrollingEnabled(false);
//        setRetainInstance(true);
        parser.setTimeZone(TimeZone.getTimeZone("IST"));
        myadapter.clearAll();
        registered_offers_jsons.clear();
        registered_offers_strings.clear();

        offerlist = new ArrayList<>();
        sortBy = (Spinner)v.findViewById(R.id.sortBySpinnerReleased);
        sortByCard = (CardView)v.findViewById(R.id.card4);

        final ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getContext(),
                R.array.sortBy2, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sortBy.setAdapter(adapter2);

        sortBy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position ==0){

                    Collections.sort(offerlist, new Comparator<offer>() {
                        @Override
                        public int compare(offer lhs, offer rhs) {
                            return rhs.getSchedule().compareTo(lhs.getSchedule());
                        }
                    });
                    Collections.sort(offerlist, new Comparator<offer>() {
                        @Override
                        public int compare(offer lhs, offer rhs) {
                            int a =0,b=0;
                            if(lhs.getStatus().equals("Deleted"))
                                a=3;
                            else if (lhs.getStatus().equals("Expired"))
                                a=2;
                            else if(lhs.getStatus().equals("Placed"))
                                a=1;
                            else
                             a=0;

                            if(rhs.getStatus().equals("Deleted"))
                                b=3;
                            else if (rhs.getStatus().equals("Expired"))
                                b=2;
                            else if(rhs.getStatus().equals("Placed"))
                                b=1;
                            else
                                b=0;

                            if(a<b)
                                return -1;
                            else if (a==b)
                                return 0;
                            else
                                return 1;

                        }
                    });
//                    Collections.sort(offerlist, new Comparator<offer>() {
//                        @Override
//                        public int compare(offer lhs, offer rhs) {
//                            return rhs.getExpire_date().compareTo(lhs.getExpire_date());
//                        }
//                    });
                    myadapter.addAll(offerlist);
                }
                if(position==1){
                    Collections.sort(offerlist, new Comparator<offer>() {
                        @Override
                        public int compare(offer lhs, offer rhs) {

                            return lhs.getHotel_name().compareTo(rhs.getHotel_name());

                        }
                    });
                    myadapter.addAll(offerlist);
                }
                if(position==2){
                    Collections.sort(offerlist, new Comparator<offer>() {
                        @Override
                        public int compare(offer lhs, offer rhs) {
                            return lhs.getWowprice().compareTo(rhs.getWowprice());
                        }
                    });
                    myadapter.addAll(offerlist);
                }
                if(position==3){
                    Collections.sort(offerlist, new Comparator<offer>() {
                        @Override
                        public int compare(offer lhs, offer rhs) {
                           return rhs.getWowprice().compareTo(lhs.getWowprice());
                        }
                    });
                    myadapter.addAll(offerlist);
                }
                if(position==4){
                    Collections.sort(offerlist, new Comparator<offer>() {
                        @Override
                        public int compare(offer lhs, offer rhs) {

                            return (lhs.getDate_checkin().compareTo(rhs.getDate_checkin()));
                        }
                    });
                    myadapter.addAll(offerlist);
                }
                if(position==5){
                    Collections.sort(offerlist, new Comparator<offer>() {
                        @Override
                        public int compare(offer lhs, offer rhs) {

                            return (lhs.getDate_check_out().compareTo(rhs.getDate_check_out()));
                        }
                    });
                    myadapter.addAll(offerlist);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        try {
            fetchOffers();
        } catch (JSONException e) {
            e.printStackTrace();
        }



        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    private void fetchOffers() throws JSONException {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.GET_OFFER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respons) {
                        JSONObject response;


                        try {
                            response = new JSONObject(respons);


                            JSONArray data=response.getJSONArray("offers");
                            Log.e("offers log",data.toString());
                           myadapter.clearAll();
                           if (data.length()>0) {
                               sortByCard.setVisibility(View.VISIBLE);
                               for (int i = 0; i < data.length(); i++) {
                                   JSONObject rec = data.getJSONObject(i);
                                   Date checkin = parser.parse(rec.getString(Release_constants.CHECK_IN));
                                   Date checkout = parser.parse(rec.getString(Release_constants.CHECK_OUT));
                                   Date schedule = parser.parse(rec.getString(Release_constants.SCHEDULE));
                                   Date expiry = parser.parse(rec.getString(Release_constants.EXPIRY));
//                                   Log.e("sched",schedule.toString());
//                                   Log.e("xhkout",checkout.toString());
//                                   Log.e("chkin",checkin.toString());
                                   offerlist.add(new offer(rec.getString(Release_constants.HOTEL_NAME), rec.getString(Release_constants.OFFER_NAME), Integer.parseInt(rec.getString(Release_constants.NO_OF_ROOMS)), Integer.parseInt(rec.getString(Release_constants.PRICE_MRP)), rec.getString(Release_constants.WOW_PRICE),
                                           rec.getString(Release_constants.ROOM_TYPE), checkin, checkout, schedule,expiry,rec.getString("hotelId"),rec.getString("roomId"),rec.getString("isDeleted"),rec.getString(Release_constants.OFFER_ID)));




                               }
                               myadapter.addAll(offerlist);

                           }
                            else {
                                sortByCard.setVisibility(View.GONE);
                               Toast.makeText(getContext(), "NO offers Released by you.", Toast.LENGTH_SHORT).show();
                           }
                            Log.e("resp of offers",response.toString());
                        } catch (JSONException e) {
                            Log.e("error",e.toString());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sortByCard.setVisibility(View.GONE);
                Toast.makeText(getContext(),"Internet Problem",Toast.LENGTH_LONG).show();
                Log.e("error",error.toString());
            }
        }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put(HotelRegistrationConstants.OWNER_ID,profile.getString(Constants.ID,"default"));

                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
}
