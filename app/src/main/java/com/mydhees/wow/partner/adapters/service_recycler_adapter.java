package com.mydhees.wow.partner.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.objects.service;


import java.net.URL;
import java.util.ArrayList;

/**
 * Created by anshu on 13/05/16.
 */
public class service_recycler_adapter extends RecyclerView.Adapter<service_recycler_adapter.myviewholder>{

    static Context context;
    LayoutInflater inflater;
    static ArrayList<service> services = new ArrayList<>();
    static ArrayList<service> all_services = new ArrayList<>();
    static ArrayList<service> current = new ArrayList<>();
    int n=0;


    static service current_service;

    public service_recycler_adapter(Context context) {
        this.context = context;
        inflater=LayoutInflater.from(context);
    }

    public void addservices(ArrayList<service> services)
    {
        this.services.clear();
        this.services.addAll(services);
        if(n==0){
            all_services.addAll(services);
            n++;
        }

    }


    public ArrayList<service> getlist()
    {
        return services;
    }



    @Override
    public service_recycler_adapter.myviewholder onCreateViewHolder(ViewGroup parent, int viewType) {
       View view = inflater.inflate(R.layout.service_list_item,parent,false);
        myviewholder viewholder = new myviewholder(view);
        return  viewholder;


    }

    @Override
    public void onBindViewHolder(service_recycler_adapter.myviewholder holder, final int position) {
        final int pos = position;
        current_service = services.get(position);
        holder.service_name.setText(current_service.getName());
        holder.service_image.setImageDrawable(context.getResources().getDrawable(current_service.getImage_id()));


        for(int i=0;i<all_services.size();i++){
            if(all_services.get(i).getName().matches(current_service.getName())){
                holder.service_checkbox.setChecked(all_services.get(i).isSelected());
            }
        }


        holder.service_checkbox.setTag(services.get(position));


        holder.service_checkbox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                service ser = (service)cb.getTag();
                ser.setSelected(cb.isChecked());
                for(int i=0;i<all_services.size();i++){

                    if(!all_services.get(i).getName().matches(ser.getName())){
                        all_services.get(i).setSelected(false);
                    }
                    else{
                        all_services.get(i).setSelected(cb.isChecked());
                    }
                }
                Log.e("serv",all_services.toString());
                notifyDataSetChanged();

            }
        });

//





    }

    @Override
    public int getItemCount() {
        return services.size();
    }


    static class myviewholder extends RecyclerView.ViewHolder {
        private TextView service_name;
        private CheckBox service_checkbox;
        private ImageView service_image;
        private View v;

        public myviewholder(View itemview) {
            super(itemview);
            service_name = (TextView) itemview.findViewById(R.id.service_name);
            service_checkbox = (CheckBox) itemview.findViewById(R.id.service_checkbox);
            service_image = (ImageView)itemview.findViewById(R.id.service_image);
            v = (View)itemview.findViewById(R.id.v);


        }
    }
}
