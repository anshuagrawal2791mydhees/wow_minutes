package com.mydhees.wow.partner.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.mydhees.wow.partner.R;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pranav on 19-01-2017.
 */

public class PolicyAdapter_Update extends RecyclerView.Adapter<PolicyAdapter_Update.MyViewHolder> {

    private List<String> policies = new ArrayList<>();
    private Context mContext;

    public PolicyAdapter_Update (List policies, Context context){
        this.policies = policies;
        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hotel_policy_row_list_updated,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.hotelPolicy.setText(policies.get(position).toString());
        // set a Listener for the delete button
        holder.delete_button_updaated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // delete the hotel poluicy on this click
                removeAt(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return policies.size();
    }

    public void removeAt (int position) {
        policies.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, policies.size());
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageButton delete_button_updaated;
        public TextView hotelPolicy;
        public ImageView hotelPolicyListTag;

        public MyViewHolder(View itemView) {
            super(itemView);
            delete_button_updaated = (ImageButton) itemView.findViewById(R.id.imageButton_delete_policy);
            hotelPolicy = (TextView) itemView.findViewById(R.id.textView_hotel_policy_description);
        }

    }
}
