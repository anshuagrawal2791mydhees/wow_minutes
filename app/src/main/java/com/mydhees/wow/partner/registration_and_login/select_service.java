package com.mydhees.wow.partner.registration_and_login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.adapters.service_recycler_adapter;
import com.mydhees.wow.partner.allConstants.Constants;
import com.mydhees.wow.partner.objects.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class select_service extends AppCompatActivity {

    EditText search;
    RecyclerView recycler;
    Button next;
    ArrayList<service> services = new ArrayList<>();
    service_recycler_adapter adapter;
    LinearLayoutManager mlinearlayoutmanager;
    ArrayList<service> selected_services = new ArrayList<>();
    ArrayList<String> selected_services_names = new ArrayList<>();
    SharedPreferences profile;
    SharedPreferences.Editor editor;
    Set<String> set = new HashSet<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_service);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        profile = getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE,MODE_PRIVATE);
        editor = profile.edit();


        search = (EditText)findViewById(R.id.search);


        recycler = (RecyclerView)findViewById(R.id.service_recycler);
        next = (Button)findViewById(R.id.next);

        adapter = new service_recycler_adapter(this);

        services.add(new service("hotel",false,R.drawable.hotel_pic));
        services.add(new service("Bus",false,R.drawable.bus_pic));
        services.add(new service("restaurant",false,R.drawable.resturant_pic));
        services.add(new service("PG",false,R.drawable.pg_pic));
        services.add(new service("Movie",false,R.drawable.movies_pic));
        services.add(new service("Other",false,R.drawable.others_pic));
        adapter.addservices(services);


        mlinearlayoutmanager = new LinearLayoutManager(this);

        recycler.setLayoutManager(mlinearlayoutmanager);
        recycler.setAdapter(adapter);



        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {



            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.e("search", s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>0) {
                    final ArrayList<service> filteredlList = filter(services, s.toString());
                    adapter.addservices(filteredlList);
                    adapter.notifyDataSetChanged();
                    next.setVisibility(View.GONE);
                }
                else {
                    final ArrayList<service> filteredlList = filter(services, s.toString());
                    adapter.addservices(filteredlList);
                    adapter.notifyDataSetChanged();
                    next.setVisibility(View.VISIBLE);
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String data = "";
                selected_services.clear();
                selected_services_names.clear();
                selected_services.addAll(adapter.getlist());




                    for (int i = 0; i < selected_services.size(); i++) {
                        service single_service = selected_services.get(i);
                        if (single_service.isSelected() == true) {
                            data = data + "\n" + single_service.getName().toString();
                            selected_services_names.add(single_service.getName());
                        }
                    }

                if(!selected_services_names.isEmpty()) {
                    //Toast.makeText(getApplicationContext(),selected_services_names.toString(), Toast.LENGTH_LONG).show();
                    set.clear();
                    set.addAll(selected_services_names);
                    //editor.remove(Constants.SERVICES);
                    editor.putStringSet(Constants.SERVICES, set);
                    editor.commit();
                    Toast.makeText(getApplicationContext(), profile.getStringSet(Constants.SERVICES, null).toString(), Toast.LENGTH_LONG).show();
                    startActivity(new Intent(getApplicationContext(), user_details.class));
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Select at least one",Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    //For back button in toolbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public ArrayList<service> filter(ArrayList<service> models, String query) {
        query = query.toLowerCase();

        final ArrayList<service> filteredModelList = new ArrayList<>();
        for (service model : models) {
            final String text = model.getName().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }


}
