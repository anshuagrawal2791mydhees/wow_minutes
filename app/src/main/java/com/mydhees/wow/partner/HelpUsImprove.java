package com.mydhees.wow.partner;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mydhees.wow.partner.allConstants.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class HelpUsImprove extends AppCompatActivity {
    private Toolbar toolbar;
    Button send_btn;
    EditText feebk_txt;
    SharedPreferences profile;
    VolleySingleton volleySingleton;
    RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_us_improve);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //send_btn = (Button)findViewById(R.id.send_feedback);
        feebk_txt = (EditText)findViewById(R.id.feedbk_txt);

        toolbar = (Toolbar)findViewById(R.id.toolbar_help);
        setSupportActionBar(toolbar);
        volleySingleton = VolleySingleton.getinstance();
        requestQueue = volleySingleton.getrequestqueue();
        profile= getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE, Context.MODE_PRIVATE);

//        send_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });



        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_back_white_icon);




    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.help_menu_appbar, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.send_feedback) {

            if(feebk_txt.getText().toString().matches(""))
                Toast.makeText(HelpUsImprove.this,"Fill in the missing details",Toast.LENGTH_LONG).show();
            else{
                final ProgressDialog dialog = new ProgressDialog(HelpUsImprove.this);
                dialog.show();
                dialog.setCancelable(false);
                StringRequest request = new StringRequest(Request.Method.POST, Constants.FEEDBACK, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        dialog.dismiss();
                        Log.e("resp",response);
                        try {
                            JSONObject resp = new JSONObject(response);

                            Toast.makeText(HelpUsImprove.this,resp.getString("response"),Toast.LENGTH_LONG).show();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        dialog.dismiss();
                        Log.e("Err",error.toString());
                        Toast.makeText(HelpUsImprove.this,"Error",Toast.LENGTH_LONG).show();
                    }
                }){
                    @Override
                    protected Map<String,String> getParams(){
                        Map<String,String> params = new HashMap<String, String>();
                        params.put(Constants.EMAIL,profile.getString(Constants.EMAIL,"default"));
                        params.put(Constants.NAME,profile.getString(Constants.NAME,"default"));
                        params.put("ownerId",profile.getString(Constants.ID,"default"));
                        params.put("message",feebk_txt.getText().toString());
                        return params;
                    }
                };
                request.setRetryPolicy(new DefaultRetryPolicy(
                        10000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(request);
            }


            return true;
        }

        //for backbutton
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:

        }


        return super.onOptionsItemSelected(item);


    }

}
