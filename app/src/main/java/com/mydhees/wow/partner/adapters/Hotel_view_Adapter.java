package com.mydhees.wow.partner.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.mydhees.wow.partner.HotelDisplay;
import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.VolleySingleton;
import com.mydhees.wow.partner.credits_tab.Recharge;
import com.mydhees.wow.partner.objects.registeredHotels;


import java.util.ArrayList;

/**
 * Created by Jashan Kochar on 6/24/2016.
 */
public class Hotel_view_Adapter extends RecyclerView.Adapter<Hotel_view_Adapter.myviewholder> {

    static Context context;
    LayoutInflater inflater;
    static ArrayList<registeredHotels> htl = new ArrayList<>();
    static registeredHotels reg_hotl;
    VolleySingleton volleySingleton;
    ImageLoader imageLoader;
    String imageurl;


    public Hotel_view_Adapter(Context context) {
        volleySingleton = VolleySingleton.getinstance();
        imageLoader = volleySingleton.getimageloader();
        this.context = context;
        inflater=LayoutInflater.from(context);
    }

    public void regHotelsShow(registeredHotels reg)
    {
        htl.add(0,reg);
        notifyItemInserted(0);
    }


    public ArrayList<registeredHotels> getlist()
    {
        return htl;
    }

    @Override
    public Hotel_view_Adapter.myviewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.registered_hotels_row_view,parent,false);
        myviewholder viewholder = new myviewholder(view);

        return  viewholder;
    }

    public void clearAll(){
        htl.clear();
    }
    @Override
    public void onBindViewHolder(final myviewholder holder, final int position) {


        final int pos = position;
        reg_hotl = htl.get(position);
        holder.hname.setText(reg_hotl.getName());
        Log.e("url",reg_hotl.getImage_link());
        if(!reg_hotl.getImage_link().matches(""))
            {
                imageurl = "https://s3-us-west-2.amazonaws.com/wowimagesupload/resized/middle/"+reg_hotl.getImage_link()+".jpg";
                imageLoader.get(imageurl, new ImageLoader.ImageListener() {
                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                        holder.himage.setImageDrawable(null);
                        holder.himage.setImageBitmap(response.getBitmap());
                    }

                     @Override
                    public void onErrorResponse(VolleyError error) {


                }
                });
            }
        else
        holder.himage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_launcher));

   holder.cv_hotel.setOnClickListener(new View.OnClickListener() {
       @Override
       public void onClick(View v) {
           Toast.makeText(context, htl.get(position).getName(), Toast.LENGTH_SHORT).show();
           Intent intent= new Intent(context,HotelDisplay.class);
           intent.putExtra("hname",htl.get(position).getName());
           intent.putExtra("hid",htl.get(position).getId());
        context.startActivity(intent);

       }
   });


    }

    @Override
    public int getItemCount() {
        return htl.size();
    }

    static class myviewholder extends RecyclerView.ViewHolder {
        private TextView hname;
        private ImageView himage;
        CardView cv_hotel;

        public myviewholder(View itemview) {
            super(itemview);
            hname = (TextView) itemview.findViewById(R.id.hname_profile);
            himage = (ImageView)itemview.findViewById(R.id.htl1_image);
            cv_hotel= (CardView) itemview.findViewById(R.id.cv_hotel);

        }
    }

}
