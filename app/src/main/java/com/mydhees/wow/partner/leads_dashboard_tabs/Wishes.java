package com.mydhees.wow.partner.leads_dashboard_tabs;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.VolleySingleton;
import com.mydhees.wow.partner.adapters.WishesCustAdapter;
import com.mydhees.wow.partner.allConstants.Constants;
import com.mydhees.wow.partner.objects.WishObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * A simple {@link Fragment} subclass.
 */
public class Wishes extends Fragment {
    Spinner cityWish;
    Spinner sortBy;
    RecyclerView recycler;
    ArrayList<WishesCustAdapter> custWishes = new ArrayList<>();
    WishesCustAdapter myadapter2;
    LinearLayoutManager manager;
    VolleySingleton volleySingleton;
    RequestQueue requestQueue;
    DateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
    SharedPreferences profile;
    SharedPreferences.Editor editor;
    ArrayList<WishObject> wisheslist;
    CardView sortByCard;


    public Wishes() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_wishes, container, false);
        volleySingleton = VolleySingleton.getinstance();
        requestQueue = volleySingleton.getrequestqueue();
        profile = getActivity().getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE, Context.MODE_PRIVATE);
        editor= profile.edit();
        myadapter2 = new WishesCustAdapter(getContext());
        recycler = (RecyclerView)v.findViewById(R.id.recyclerWishList);

        recycler.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        recycler.setAdapter(myadapter2);
        recycler.setVisibility(View.VISIBLE);
        recycler.setNestedScrollingEnabled(false);

        cityWish = (Spinner)v.findViewById(R.id.cityWishSpnnr);
        sortBy = (Spinner)v.findViewById(R.id.sortBySpinner);
        sortByCard = (CardView)v.findViewById(R.id.card3);


        wisheslist = new ArrayList<>();

        parser.setTimeZone(TimeZone.getTimeZone("IST"));
        //city spinner
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.citiWish, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cityWish.setAdapter(adapter);
final ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getContext(),
                R.array.sortBy, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sortBy.setAdapter(adapter2);

      //  myadapter2.clearAll();

        sortBy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position ==0){
                    Collections.sort(wisheslist, new Comparator<WishObject>() {
                        @Override
                        public int compare(WishObject lhs, WishObject rhs) {
                            int a =0,b=0;
                            if(lhs.getSerious().equals("Testing"))
                                a=3;
                            else if (lhs.getSerious().equals("Casual"))
                                a=2;
                            else if(lhs.getSerious().equals("Serious"))
                                a=1;
                            else
                                a=0;

                            if(rhs.getSerious().equals("Testing"))
                                b=3;
                            else if (rhs.getSerious().equals("Casual"))
                                b=2;
                            else if(rhs.getSerious().equals("Serious"))
                                b=1;
                            else
                                b=0;
                            if(a<b)
                                return -1;
                            else if (a==b)
                                return 0;
                            else
                                return 1;
                            
                        }
                    });
                    myadapter2.addAll(wisheslist);
                }
                if(position==1){
                    Collections.sort(wisheslist, new Comparator<WishObject>() {
                        @Override
                        public int compare(WishObject lhs, WishObject rhs) {
                            int a = Integer.parseInt(lhs.getMinPrice());
                            int b = Integer.parseInt(rhs.getMinPrice());
                            if(a<b)
                                return -1;
                            else if (a==b)
                                return 0;
                            else
                                return 1;

                        }
                    });
                    myadapter2.addAll(wisheslist);
                }
                if(position==2){
                    Collections.sort(wisheslist, new Comparator<WishObject>() {
                        @Override
                        public int compare(WishObject lhs, WishObject rhs) {
                            int a = Integer.parseInt(lhs.getMaxPrice());
                            int b = Integer.parseInt(rhs.getMaxPrice());
                            if(a<b)
                                return -1;
                            else if (a==b)
                                return 0;
                            else
                                return 1;
                        }
                    });
                    myadapter2.addAll(wisheslist);
                }
                if(position==3){
                    Collections.sort(wisheslist, new Comparator<WishObject>() {
                        @Override
                        public int compare(WishObject lhs, WishObject rhs) {
                            return (lhs.getDate_checkin().compareTo(rhs.getDate_checkin()));
                        }
                    });
                    myadapter2.addAll(wisheslist);
                }
                if(position==4){
                    Collections.sort(wisheslist, new Comparator<WishObject>() {
                        @Override
                        public int compare(WishObject lhs, WishObject rhs) {

                            return (lhs.getDate_check_out().compareTo(rhs.getDate_check_out()));
                        }
                    });
                    myadapter2.addAll(wisheslist);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        cityWish.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                myadapter2.clearAll();
                wisheslist.clear();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.FETCH_WISH,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String respons) {
                                JSONObject response;


                                try {
                                    response = new JSONObject(respons);

                                    Log.e("resp of wish",response.toString());
                                    // JSONArray data=response.getJSONArray("offers");
                                    // Log.e("offers log",data.toString());

                                    if ((response.getBoolean("res"))){
                                        JSONArray data=response.getJSONArray("wishes");
                                        Log.e("offers log",data.toString());
                                        myadapter2.clearAll();

                                        if (data.length()>0) {

                                            for (int i = 0; i < data.length(); i++) {
                                                JSONObject rec = data.getJSONObject(i);
                                                Date cin=parser.parse(rec.getString("checkIn"));
                                                Date cout=parser.parse(rec.getString("checkOut"));
                                                if(System.currentTimeMillis() < cin.getTime()) {
                                                    sortByCard.setVisibility(View.VISIBLE);
                                                    wisheslist.add(new WishObject(rec.getString("seriousness"), rec.getString("city"), rec.getJSONObject("range").getString("start"), rec.getJSONObject("range").getString("end"),rec.getString("noOfRooms"),rec.getString("adults"),rec.getString("children"),rec.getString("wishId"),cin,cout));
                                            }
                                            }
                                            myadapter2.addAll(wisheslist);
                                        }else{
                                            sortByCard.setVisibility(View.GONE);
                                            Toast.makeText(getContext(), "No Wishes", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                } catch (JSONException e) {
                                    Log.e("error",e.toString());
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }


                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(),"Internet Problem",Toast.LENGTH_LONG).show();
                        Log.e("error",error.toString());
                    }
                }
                ){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> params = new HashMap<>();
                        params.put("city",cityWish.getSelectedItem().toString());

                        return params;
                    }
                };
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        5000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(stringRequest);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        return v;


    }


    private void fetchWish(final String city) throws JSONException {

    }



}
