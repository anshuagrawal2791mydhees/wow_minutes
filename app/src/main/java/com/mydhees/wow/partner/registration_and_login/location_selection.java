package com.mydhees.wow.partner.registration_and_login;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mydhees.wow.partner.Home;
import com.mydhees.wow.partner.RegistrationIntentService;
import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.VolleySingleton;
import com.mydhees.wow.partner.allConstants.Constants;
import com.mydhees.wow.partner.utils.LocationUtility;
import com.mydhees.wow.partner.utils.MapUtility2;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class location_selection extends AppCompatActivity implements OnMapReadyCallback{

    private GoogleMap mMap;
    Location location;
    MarkerOptions mo;
    MapUtility2 locationGetter;

    Marker marker;
    int PLACE_PICKER_REQUEST = 1;
    EditText street_address;
    TextView street_address2;
    Button finish;
    SharedPreferences profile;
    SharedPreferences.Editor editor;
    SharedPreferences launch_time;
    SharedPreferences.Editor editor2;
    ArrayList<String> addressParts = new ArrayList<>();
    ArrayList<String> addressParts2 = new ArrayList<>();
    String locationAddress=null;
    String locality=null;
    String country=null;
    String postal_code=null;
    LocationUtility getAddress;
    LocationUtility getAddress2;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    String referralCode="";


    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ProgressBar mRegistrationProgressBar;
    private TextView mInformationTextView;
    private boolean isReceiverRegistered;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "HomeActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_selection);
        Intent intent = getIntent();
        referralCode = intent.getStringExtra("referralCode");
        Toast.makeText(this,referralCode,Toast.LENGTH_LONG).show();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //dialog.dismiss();
                //                mRegistrationProgressBar.setVisibility(ProgressBar.GONE);
                Log.e("onreceive","received");
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(Constants.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
//                    mInformationTextView.setText(getString(R.string.gcm_send_message));
                    Log.e("onreceive","success");
                    Log.e("---intent token",intent.getStringExtra("token"));

                    try {
                        registerUser(intent.getStringExtra("token"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Log.e("onreceive","error");
                    Toast.makeText(location_selection.this,"gcm token error",Toast.LENGTH_LONG).show();

//                    mInformationTextView.setText(getString(R.string.token_error_message));
                }
            }
        };

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        volleySingleton=VolleySingleton.getinstance();
        requestQueue = volleySingleton.getrequestqueue();

        getAddress = new LocationUtility(this);
        street_address = (EditText)findViewById(R.id.street_address);
        street_address2 = (TextView) findViewById(R.id.street_address2);
        finish = (Button)findViewById(R.id.finish);

        profile = getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE,MODE_PRIVATE);
        editor = profile.edit();

        launch_time = getSharedPreferences(Constants.LAUNCH_TIME_PREFERENCE_FILE,MODE_PRIVATE);
        editor2 = launch_time.edit();


       street_address.setEnabled(false);

            finish.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(street_address.getText().toString().matches(""))
                    {
                        Toast.makeText(getApplicationContext(),"Enter Business Location",Toast.LENGTH_SHORT).show();

                    }
                    else
                    {

                        Log.e("gcm", String.valueOf(isReceiverRegistered));


                            registerReceiver();
                         Log.e("gcm2", String.valueOf(isReceiverRegistered));

                            if (checkPlayServices()) {
                                // Start IntentService to register this application with GCM.
                                Intent intent = new Intent(location_selection.this, RegistrationIntentService.class);
                                startService(intent);
                            }





                    }
                }
            });



    }

    private void registerUser(final String token) throws JSONException{
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.show();

        StringRequest request = new StringRequest(Request.Method.POST, Constants.REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respons) {
                        dialog.dismiss();
                        JSONObject response = null;
                        try {
                            response = new JSONObject(respons);
                        } catch (JSONException e) {
                            Log.e("error",e.toString());
                        }
                        try {
                            if(response.getBoolean("res"))
                            {
                                HashSet<String> registered_rooms = new HashSet<String>(new ArrayList<String>());
                                HashSet<String> registered_hotels = new HashSet<String>(new ArrayList<String>());
                                editor.putStringSet(Constants.REGISTERED_ROOMS,registered_rooms);
                                editor.putStringSet(Constants.REGISTERED_HOTELS,registered_hotels);
                                editor.putString(Constants.ADDRESS,street_address.getText().toString().concat(street_address2.getText().toString()));
                                editor.putString(Constants.ADDRESS_LINE1,street_address.getText().toString());
                                editor.putString(Constants.ADDRESS_LINE2,street_address2.getText().toString());
                                editor.putString(Constants.PIN,postal_code);
                                editor.putString("referralCode",response.getString("referralCode"));
                                editor.putString(Constants.ID,response.getString(Constants.ID));
                                Log.e("id",response.getString(Constants.ID));
                                editor.commit();
                                editor2.putBoolean(Constants.FIRST_TIME,true);
                                editor2.commit();
                                // Toast.makeText(getApplicationContext(),street_address.getText().toString().concat(street_address2.getText().toString()),Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), Home.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
//                                editor.putString(Constants.NAME,name.getText().toString());
//                                editor.putString(Constants.EMAIL,email.getText().toString());
//                                editor.putString(Constants.PASSWORD,password.getText().toString());
//                                editor.putString(Constants.PHONE,mobile_number.getText().toString());
//                                editor.putString(Constants.CITY,city.getSelectedItem().toString());
//                                editor.commit();
//                                startActivity(new Intent(getApplicationContext(),location_selection.class));

                                Toast.makeText(getApplicationContext(),response.getString("response"),Toast.LENGTH_LONG).show();
                            }
                            else
                            {
                                Toast.makeText(getApplicationContext(),response.getString("response"),Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_LONG).show();
                Log.e("vollley error",error.toString());
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put(Constants.NAME,profile.getString(Constants.NAME,null));
                params.put(Constants.EMAIL,profile.getString(Constants.EMAIL,null));
                params.put(Constants.PASSWORD,profile.getString(Constants.PASSWORD,null));
                params.put(Constants.PHONE, profile.getString(Constants.PHONE,null));
                params.put(Constants.CITY, profile.getString(Constants.CITY,null));
                params.put(Constants.ADDRESS,street_address.getText().toString().concat(street_address2.getText().toString()));
                params.put(Constants.ADDRESS_LINE1,street_address.getText().toString());
                params.put(Constants.GCM_TOKEN,token);
                params.put(Constants.ADDRESS_LINE2,street_address2.getText().toString());
                if(referralCode!= null){
                Log.e("code",referralCode);
                params.put("referralCode",referralCode);}
                Log.e("postal",postal_code);
                params.put(Constants.PIN,postal_code);

                params.put(Constants.LONGITUDE,profile.getFloat(Constants.LONGITUDE, (float) 0.0)+"");
                params.put(Constants.LATITUDE,profile.getFloat(Constants.LATITUDE, (float) 0.0)+"");
                ArrayList<String> selected_services = new ArrayList<>();
                selected_services.addAll(profile.getStringSet(Constants.SERVICES,null));
                JSONObject services=new JSONObject();
                for(int i=0;i<selected_services.size();i++)
                {
                    try {
                        services.put("params_"+i,selected_services.get(i));
                    } catch (JSONException e) {
                        Log.e("error",e.toString());
                    }
                }
                params.put(Constants.SERVICES,services.toString());
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Toast.makeText(getApplicationContext(), "Please Wait...", Toast.LENGTH_SHORT).show();
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(location_selection.this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }


            }
        });
        locationGetter = new MapUtility2(this){
            @Override
            protected void onPostExecute(Location location) {
                if(location == null)
                {
                    Toast.makeText(getApplicationContext(),"can't locate you!",Toast.LENGTH_LONG).show();
                    mo = new MarkerOptions().position(new LatLng(0,0)).title("Tap to edit your business location");
                    marker = mMap.addMarker(mo);
                    marker.showInfoWindow();


                }
                else {
                    LatLng sydney = new LatLng(location.getLatitude(), location.getLongitude());
                    mo = new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).title("Tap to edit your business location");

                    marker = mMap.addMarker(mo);
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                    //marker.setSnippet("YOU");
                    marker.showInfoWindow();

                    if (location.getLatitude() != 0.0) {

                        try {
                            addressParts.clear();
                            addressParts.addAll(getAddress.execute(new LatLng(location.getLatitude(), location.getLongitude())).get());
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        } finally {
                            locationAddress = addressParts.get(0);
                            if (addressParts.size() > 1) {
                                locality = addressParts.get(1);
                                country = addressParts.get(2);
                                postal_code = addressParts.get(3);
//                                Toast.makeText(getApplicationContext(),postal_code,Toast.LENGTH_LONG).show();
//                                Toast.makeText(getApplicationContext(),locality,Toast.LENGTH_LONG).show();

                                locationAddress = locationAddress.replaceAll("null", "");
                                locationAddress = locationAddress.replaceAll(locality, "");
                            }


                            if (!locationAddress.contains("Unable")) {
                                street_address.setText(locationAddress);
                                street_address2.setText(locality + ", " + country);
                            }

                            marker.setSnippet(locationAddress);
                            marker.showInfoWindow();
                        }


                        //Toast.makeText(getApplicationContext(),postal_code,Toast.LENGTH_LONG).show();


                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                                new LatLng(location.getLatitude(), location.getLongitude()), 13));

                        editor.putFloat(Constants.LATITUDE, (float) location.getLatitude());
                        editor.putFloat(Constants.LONGITUDE,(float)location.getLongitude());
                        editor.commit();

                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(new LatLng(location.getLatitude(), location.getLongitude()))      // Sets the center of the map to location user
                                .zoom(13)                   // Sets the zoom
                                .bearing(90)                // Sets the orientation of the camera to east
                                // Sets the tilt of the camera to 30 degrees
                                .build();                   // Creates a CameraPosition from the builder
                        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    }


                }

            }
        };
        locationGetter.execute();

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        getAddress2 = new LocationUtility(this);
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                String toastMsg = String.format("Place: %s", place.getName());
                Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
                marker.setPosition(place.getLatLng());
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                        place.getLatLng(), 13));

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(place.getLatLng())      // Sets the center of the map to location user
                        .zoom(13)                   // Sets the zoom
                        .bearing(90)                // Sets the orientation of the camera to east
                                    // Sets the tilt of the camera to 30 degrees
                        .build();                   // Creates a CameraPosition from the builder
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//                marker.setSnippet(place.getAddress().toString());
//                marker.showInfoWindow();
                editor.putFloat(Constants.LATITUDE, (float) place.getLatLng().latitude);
                editor.putFloat(Constants.LONGITUDE,(float)place.getLatLng().longitude);
                editor.commit();
                //street_address.setText(place.getAddress());

                try {
                    addressParts2.clear();
                    addressParts2.addAll(getAddress2.execute(place.getLatLng()).get());

                } catch (InterruptedException e) {
                    Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_SHORT).show();
                } catch (ExecutionException e) {
                    Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_SHORT).show();
                }finally {
                    //Toast.makeText(getApplicationContext(),addressParts2.toString(),Toast.LENGTH_SHORT).show();

                    locationAddress=addressParts2.get(0);
                    if(addressParts2.size()>1) {
                        Log.e("a",addressParts2.toString());
                        locality = addressParts2.get(1);
                        country = addressParts2.get(2);
                        postal_code = addressParts2.get(3);
                        //Toast.makeText(getApplicationContext(),postal_code,Toast.LENGTH_LONG).show();

                        locationAddress = locationAddress.replaceAll("null", "");
                        locationAddress = locationAddress.replaceAll(locality, "");
                    }

                    if(!locationAddress.contains("Unable")) {
                        street_address.setText(locationAddress);
                        street_address2.setText(locality + ", "+country);
                    }
                    marker.setSnippet(locationAddress);
                    marker.showInfoWindow();

                }


                //street_address2.setText(place.getLocale);
            }
        }
    }

    //For back button in toolbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void registerReceiver(){
        if(!isReceiverRegistered) {
            Log.e("registerReceiver","here");
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(Constants.REGISTRATION_COMPLETE));
            isReceiverRegistered = true;
        }
    }
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.e(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        registerReceiver();

        super.onResume();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        isReceiverRegistered = false;
        super.onPause();
    }
}
