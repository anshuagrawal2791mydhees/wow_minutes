package com.mydhees.wow.partner.registration_and_login;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mydhees.wow.partner.Home;
import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.VolleySingleton;
import com.mydhees.wow.partner.allConstants.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static android.R.attr.filter;

public class login extends AppCompatActivity {
    CheckBox checkbox;
    EditText login_pswd, email;
    Button button_login;
    boolean error = false;
    TextView forgot_pwd;
    AlertDialog alertDialog, alertDialog2;
    EditText ed_email, code, new_pwd;
    SharedPreferences profile;
    SharedPreferences.Editor editor;
    VolleySingleton volleySingleton;
    RequestQueue requestQueue;
    Set<String> service_set = new HashSet<>();
    SharedPreferences launch_time;
    SharedPreferences.Editor editor2;
    String ed_email_text;
    ArrayList<String> registered_rooms_list = new ArrayList<>();
    ArrayList<String> registered_hotels_list = new ArrayList<>();
    JSONArray registered_rooms_json = new JSONArray();
    JSONArray registered_hotels_json = new JSONArray();
    private String blockCharacterSet = "~#^|$%&*!/;'<>()-=`{}[]:+";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        email = (EditText) findViewById(R.id.email);
        checkbox = (CheckBox) findViewById(R.id.checkBox);
        login_pswd = (EditText) findViewById(R.id.login_pswd);
        button_login = (Button) findViewById(R.id.button_login);
        profile = getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE, MODE_PRIVATE);
        editor = profile.edit();
        volleySingleton = VolleySingleton.getinstance();
        requestQueue = volleySingleton.getrequestqueue();

        launch_time = getSharedPreferences(Constants.LAUNCH_TIME_PREFERENCE_FILE, MODE_PRIVATE);
        editor2 = launch_time.edit();

        email.setFilters(new InputFilter[]{filter});


        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                error = false;
                if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText()).matches()) {
                    email.setError("Invalid Email");
                    //email.setText("");
                    error = true;
                }
                if (email.getText().toString().equals("")) {
                    email.setError("Required");
                    error = true;
                }
                if (login_pswd.getText().toString().equals("")) {
                    login_pswd.setError("Required");
                    error = true;
                }

                if (error == false) {
                    try {
                        loginUser();
                    } catch (JSONException e) {
                        Log.e("error", e.toString());
                    }

                }

            }
        });

        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    login_pswd.setInputType(129);
                    login_pswd.setSelection(login_pswd.getText().length());

                } else {
                    login_pswd.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    login_pswd.setSelection(login_pswd.getText().length());


                }
            }
        });

        //Forgot Password Dialogbox
        forgot_pwd = (TextView) findViewById(R.id.forgot_pwd);
        ed_email = new EditText(login.this);
        code = new EditText(login.this);
        new_pwd = new EditText(login.this);


        alertDialog = new AlertDialog.Builder(login.this).create();
        alertDialog2 = new AlertDialog.Builder(login.this).create();

        //Dialog box one
        alertDialog.setTitle("Forgot Password");
//        alertDialog.setMessage("Enter your email");
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        LinearLayout layout1 = new LinearLayout(getApplicationContext());
        layout1.setOrientation(LinearLayout.VERTICAL);
        lp.setMargins(50, 25, 50, 25);
        layout1.addView(ed_email);
        ed_email.setLayoutParams(lp);
        ed_email.setHint("Your Email");
        alertDialog.setView(ed_email);
        alertDialog.setIcon(R.drawable.lock_img);
        alertDialog.setCancelable(false);
        alertDialog.setView(layout1);


        //Dialog box two
        alertDialog2.setTitle("Reset Password");
        LinearLayout layout = new LinearLayout(getApplicationContext());
        layout.setOrientation(LinearLayout.VERTICAL);

        layout.addView(code);
        code.setLayoutParams(lp);
        alertDialog2.setView(code);
        code.setHint("Code");

        layout.addView(new_pwd);
        new_pwd.setLayoutParams(lp);
        alertDialog2.setView(new_pwd);
        new_pwd.setHint("New Password");

        alertDialog2.setView(layout);
        alertDialog2.setIcon(R.drawable.lock_img);
        alertDialog2.setCancelable(false);


        // Setting OK Button
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
                //Toast.makeText(getApplicationContext(), "You clicked on CENCEL", Toast.LENGTH_SHORT).show();
            }
        });

        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "CONTINUE", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed

                try {
                    sendResetCode();
                } catch (JSONException e) {
                    Log.e("error", e.toString());
                }


                // Toast.makeText(getApplicationContext(), "Check Your email", Toast.LENGTH_SHORT).show();
               // Toast.makeText(getApplicationContext(), "Check Your email", Toast.LENGTH_SHORT).show();

            }
        });

        // Setting OK Button
        alertDialog2.setButton(DialogInterface.BUTTON_NEGATIVE, "CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
                //Toast.makeText(getApplicationContext(), "Cancel", Toast.LENGTH_SHORT).show();
            }
        });

        alertDialog2.setButton(DialogInterface.BUTTON_POSITIVE, "CONTINUE", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
                try {
                    resetPass();
                } catch (JSONException e) {
                    Log.e("error", e.toString());
                }
                //Toast.makeText(getApplicationContext(), "Continue...", Toast.LENGTH_SHORT).show();
            }
        });


        forgot_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.show();

            }
        });


    }


    //For back button in toolbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void resetPass() throws JSONException {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.RESET_PASS_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respons) {
                        JSONObject response = null;
                        try {
                            response = new JSONObject(respons);
                        } catch (JSONException e) {
                            Log.e("error",e.toString());
                        }
                        try {
                            Toast.makeText(getApplicationContext(),response.getString("response"),Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            Log.e("error",e.toString());
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Error!", Toast.LENGTH_LONG).show();
                Log.e("error", error.toString());
            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(Constants.EMAIL, ed_email_text);
                params.put(Constants.CODE, code.getText().toString());
                params.put(Constants.NPASS, new_pwd.getText().toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    private void sendResetCode() throws JSONException {
        StringRequest stringrequest = new StringRequest(Request.Method.POST, Constants.SEND_RESET_CODE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respons) {
                        Log.e("response",respons.toString());
                        JSONObject response = null;
                        try {
                            response = new JSONObject(respons);
                        } catch (JSONException e) {
                            Log.e("error",e.toString());
                        }
                        try {
                            if(response.getBoolean("res"))
                            {
                                ed_email_text=ed_email.getText().toString();
                                alertDialog2.show();

                                Toast.makeText(getApplicationContext(),response.getString("response"),Toast.LENGTH_LONG).show();
                            }
                            else
                            {
                                Toast.makeText(getApplicationContext(),response.getString("response"),Toast.LENGTH_LONG).show();

                            }
                        } catch (JSONException e) {
                            Log.e("error",e.toString());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("err",error.toString());

            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(Constants.EMAIL, ed_email.getText().toString());
                return params;
            }
        };
        stringrequest.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringrequest);
    }

    private void loginUser() throws JSONException {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.LOGIN_URL,
                new Response.Listener<String>() {
                    @TargetApi(Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(String respons) {
                        dialog.dismiss();
                        Log.e("response", respons.toString());
                        JSONObject response = null;
                        try {
                            response = new JSONObject(respons);
                        } catch (JSONException e) {
                            Log.e("error", e.toString());
                        }
                        try {
                            if (response.getBoolean("res")) {

                                if (response.getJSONObject("results").getJSONArray("rooms").length() > 0)
                                    registered_rooms_json = (response.getJSONObject("results").getJSONArray("rooms"));
                                if (response.getJSONObject("results").getJSONArray("hotels").length() > 0)
                                    registered_hotels_json = (response.getJSONObject("results").getJSONArray("hotels"));
                                for (int i = 0; i < registered_rooms_json.length(); i++) {
                                    registered_rooms_list.add(registered_rooms_json.getString(i));
                                }
                                for (int i = 0; i < registered_hotels_json.length(); i++) {
                                    registered_hotels_list.add(registered_hotels_json.getString(i));
                                }

                                HashSet<String> registered_rooms = new HashSet<String>(registered_rooms_list);
                                HashSet<String> registered_hotels = new HashSet<String>(registered_hotels_list);
                                editor.putString("referralCode", response.getString("referralCode"));
                                editor.putStringSet(Constants.REGISTERED_ROOMS, registered_rooms);
                                editor.putStringSet(Constants.REGISTERED_HOTELS, registered_hotels);
                                editor.putString(Constants.ID, response.getString(Constants.ID));
                                editor.putString(Constants.NAME, response.getString(Constants.NAME));
                                editor.putString(Constants.EMAIL, response.getString(Constants.EMAIL));
                                editor.putString(Constants.PHONE, response.getString(Constants.PHONE));
                                editor.putString(Constants.ADDRESS, response.getString(Constants.ADDRESS));
                                editor.putString(Constants.ADDRESS_LINE1, response.getString(Constants.ADDRESS_LINE1));
                                editor.putString(Constants.ADDRESS_LINE2, response.getString(Constants.ADDRESS_LINE2));
                                editor.putString(Constants.CITY, response.getString(Constants.CITY));

                                JSONArray images = response.getJSONObject("imagesS3").getJSONArray("name");
                                if(images.length()>0)
                                editor.putString(Constants.PROFILE_IMAGE_ID,images.getString(images.length()-1));

                                editor.putFloat(Constants.LONGITUDE, Float.parseFloat(response.getJSONArray("location").get(0).toString()));
                                editor.putFloat(Constants.LATITUDE, Float.parseFloat(response.getJSONArray("location").get(1).toString()));
                                JSONArray servicesJsonArray = response.getJSONArray(Constants.SERVICES);
                                for (int i = 0; i < servicesJsonArray.length(); i++)
                                    service_set.add(servicesJsonArray.getString(i));
                                editor.putStringSet(Constants.SERVICES, service_set);
                                editor.commit();
                                editor2.putBoolean(Constants.FIRST_TIME, true);
                                editor2.commit();
                                Toast.makeText(getApplicationContext(), response.getString("response"), Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getApplicationContext(), Home.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                Log.e("hh", "hghg");
                                startActivity(intent);


                            } else {
                                Toast.makeText(getApplicationContext(), response.getString("response"), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                Log.e("vollley error", error.toString());

            }
        }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(Constants.EMAIL, email.getText().toString());
                params.put(Constants.PASSWORD, login_pswd.getText().toString());

                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    InputFilter filter = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };


}