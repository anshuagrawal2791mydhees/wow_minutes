package com.mydhees.wow.partner.objects;

/**
 * Created by Abhishek on 25-Aug-16.
 */

public class ChatObject {
    String wishId,wishCity;

    public ChatObject(String wishId, String wishCity) {
        this.wishId = wishId;
        this.wishCity = wishCity;
    }

    public String getWishId() {
        return wishId;
    }

    public void setWishId(String wishId) {
        this.wishId = wishId;
    }

    public String getWishCity() {
        return wishCity;
    }

    public void setWishCity(String wishCity) {
        this.wishCity = wishCity;
    }
}
