package com.mydhees.wow.partner.leads_dashboard_tabs;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.VolleySingleton;
import com.mydhees.wow.partner.adapters.ConvertedAdapter;
import com.mydhees.wow.partner.allConstants.Constants;
import com.mydhees.wow.partner.allConstants.HotelRegistrationConstants;
import com.mydhees.wow.partner.allConstants.Release_constants;
import com.mydhees.wow.partner.objects.convertedObject;
import com.mydhees.wow.partner.objects.offer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class Converted extends Fragment {
    RecyclerView recycler ;
    ConvertedAdapter myadapter;
    DateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");

    VolleySingleton volleySingleton;
    RequestQueue requestQueue;

    SharedPreferences profile;
    SharedPreferences.Editor editor;

    public Converted() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_converted, container, false);
        volleySingleton = VolleySingleton.getinstance();
        requestQueue = volleySingleton.getrequestqueue();
        profile = getActivity().getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE, Context.MODE_PRIVATE);
        editor= profile.edit();
        recycler = (RecyclerView)v.findViewById(R.id.converted_recycler);
        myadapter=new ConvertedAdapter(getContext());
        recycler.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        recycler.setAdapter(myadapter);
        recycler.setVisibility(View.VISIBLE);


        try {
            fetchOffers();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return  v;
    }

    private void fetchOffers() throws JSONException {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.GET_BOOKED_OFFERS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respons) {
                        JSONObject response;


                        try {
                            response = new JSONObject(respons);

                                Log.e("rsp",response.toString());
                            JSONArray data=response.getJSONArray("orders");
                            Log.e("offers log",data.toString());
                            myadapter.clearAll();

                            if (data.length()>0) {
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject rec = data.getJSONObject(i);
                                    Date checkin = parser.parse(rec.getJSONObject("booking").getString(Release_constants.CHECK_IN));
                                    Date checkout = parser.parse(rec.getJSONObject("booking").getString(Release_constants.CHECK_OUT));

                                    Date booking = parser.parse(rec.getString("createdAt"));
                                    myadapter.addoffer(new convertedObject(rec.getString("offerName"),rec.getString("amount"),rec.getJSONObject("booking").getString("name"),rec.getString("status"),rec.getJSONObject("booking").getString("contact"),"Paid",checkin,checkout,rec.getString("order_id"),rec.getJSONObject("booking").getString("roomType"),booking,rec.getJSONObject("booking").getString("hotelName")));

                                   // Log.e("sched",schedule.toString());
                                    Log.e("xhkout",checkout.toString());
                                    Log.e("chkin",checkin.toString());



                                }
                            }
                            else {

                                Toast.makeText(getContext(), "NO Bookings for your offer!!!", Toast.LENGTH_SHORT).show();
                            }
                            Log.e("resp of offers",response.toString());
                        } catch (JSONException e) {
                            Log.e("error",e.toString());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),"Internet Problem",Toast.LENGTH_LONG).show();
                Log.e("error",error.toString());
            }
        }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put(HotelRegistrationConstants.OWNER_ID,profile.getString(Constants.ID,"default"));

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
}
