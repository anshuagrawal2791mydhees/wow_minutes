package com.mydhees.wow.partner.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.VolleySingleton;
import com.mydhees.wow.partner.objects.image;
import com.mydhees.wow.partner.objects.registeredHotels;

import java.util.ArrayList;

/**
 * Created by anshu on 19/05/16.
 */
public class ImageDisplay extends RecyclerView.Adapter<ImageDisplay.myviewholder> {
    Context context;
    LayoutInflater inflater;
    ArrayList<registeredHotels> images = new ArrayList<>();
 VolleySingleton volleySingleton;
   ImageLoader imageLoader;
    String imageurl;
    static registeredHotels reg_hotl;

    public ImageDisplay(Context context) {
        volleySingleton = VolleySingleton.getinstance();
        imageLoader = volleySingleton.getimageloader();
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public void addImage(registeredHotels recieved_image)
    {
        images.add(recieved_image);
        notifyItemInserted(images.size());
    }
    public void clearAll()
    {
        images.clear();
    }
    public ArrayList<registeredHotels> getImageUris(){


        return images;
    }

    @Override
    public ImageDisplay.myviewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.imag_display,parent,false);
        myviewholder viewholder = new myviewholder(v);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(final ImageDisplay.myviewholder holder, int position) {
        reg_hotl = images.get(position);
        Log.e("url",reg_hotl.getImage_link());
        if(!reg_hotl.getImage_link().matches(""))
            {
                imageurl = "https://s3-us-west-2.amazonaws.com/wowimagesupload/resized/middle/"+reg_hotl.getImage_link()+".jpg";
                imageLoader.get(imageurl, new ImageLoader.ImageListener() {
                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                        holder.imageview.setImageDrawable(null);
                        holder.imageview.setImageBitmap(response.getBitmap());
                    }

                     @Override
                    public void onErrorResponse(com.android.volley.VolleyError error) {


                }
                });
            }
        else
        holder.imageview.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_launcher));



    }

    @Override
    public int getItemCount() {
        return images.size();
    }
    static class myviewholder extends RecyclerView.ViewHolder{

        ImageView imageview;

        public myviewholder(View itemView) {
            super(itemView);

            imageview = (ImageView)itemView
                    .findViewById(R.id.rm1_image_dis1);
        }
    }
}
