package com.mydhees.wow.partner.objects;

import android.net.Uri;

import java.util.ArrayList;

public class room {
    String price,adults,chld;
    String room_type,ac,no_room,hotel_name1;
    ArrayList<String> image_uris=new ArrayList<>();
    Boolean[] facilites = new Boolean[4];

    public room(String room_type,String price,String hotel_name1,String no_room, String ac,String ad,String chd,ArrayList<String> image_uris,Boolean[] facilites) {
        this.price = price;
        this.ac = ac;
        this.hotel_name1 = hotel_name1;
        this.no_room = no_room;
        this.adults = ad;
        this.chld = chd;
        this.room_type = room_type;
        this.image_uris.clear();
        this.image_uris.addAll(image_uris);
        this.facilites = facilites;
    }

    public String getPrice() {
        return price;
    }
    public String getHotel_name1() {
        return hotel_name1;
    }



    public String getRoom_type() {
        return room_type;
    }
    public String getNo_room() {
        return no_room;
    }

    public String getac() {
        return ac;
    }

    public String getadults() {
        return adults;
    }

    public String getchildren() {
        return chld;
    }

    public ArrayList<String> getImage_uris() {
        return image_uris;
    }

    public Boolean[] getFacilites() {
        return facilites;
    }

    public void setFacilites(Boolean[] facilites) {
        this.facilites = facilites;
    }
}
