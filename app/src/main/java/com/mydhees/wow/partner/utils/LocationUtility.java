package com.mydhees.wow.partner.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by anshu on 18/05/16.
 */
public class LocationUtility extends AsyncTask<LatLng,Void,ArrayList<String>> {
    private static final String TAG = "LocationAddress";
    ArrayList<String> addressParts = new ArrayList<>();
    Context context;
    String postal_code = null;
    public LocationUtility(Context context) {
        this.context = context;
    }
    //    public static ArrayList<String> getAddressFromLocation(final double latitude, final double longitude,
//                                                    final Context context ){
//
//        Thread thread = new Thread() {
//            @Override
//            public void run() {
//
//            }
//        };
//        thread.start();
//        return addressParts;
//    }
    @Override
    protected ArrayList<String> doInBackground(LatLng... params) {

        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        String result = null;
        Address address = null;
        String postal_code = null;
        try {
            addressParts.clear();
            List<Address> addressList = geocoder.getFromLocation(
                    params[0].latitude, params[0].longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                address = addressList.get(0);
                Log.e("address",address.toString());
                Log.e("addresslist",addressList.toString());
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i <address.getMaxAddressLineIndex()-1; i++) {
                    sb.append(address.getAddressLine(i)).append("\n");
                }
//                            sb.append(address.getLocality()).append("\n");
//                            sb.append(address.getPostalCode()).append("\n");
//                            sb.append(address.getCountryName());
                Pattern pattern = Pattern.compile("\\s\\d{6}\\s");
                result = sb.toString();
//                Matcher matcher = pattern.matcher(result);
//               // Log.e("q1",postal_code);
//                if(matcher.find())
//                {
//                    postal_code = matcher.group();
//                    Log.e("q",postal_code);
//                    result = result.replaceAll(postal_code,"");
//                }
              //  Log.e("q2",postal_code);
            }
        } catch (IOException e) {
            Log.e(TAG, "Unable connect to Geocoder", e);
        } finally {

            if (result != null) {



                result=result.replaceAll("\\n"," ");
                //bundle.putString("address", result);
                addressParts.add(result);
                addressParts.add(address.getAddressLine(address.getMaxAddressLineIndex()-1));
                addressParts.add(address.getCountryName());

                postal_code = address.getAddressLine(address.getMaxAddressLineIndex()-1).replaceAll("[^0-9]", "");
               // Log.e("q",postal_code);
                //Toast.makeText(context,postal_code,Toast.LENGTH_LONG).show();

                addressParts.add(postal_code);
                addressParts.add(address.getLocality());

//                Pattern pattern = Pattern.compile("\\s\\d{6}\\s");
//                Matcher matcher = pattern.matcher(address.getCountryName());
//                // Log.e("q1",postal_code);
//                if(matcher.find())
//                {
//                    postal_code = matcher.group();
//                    Log.e("q",postal_code);
//                    result = result.replaceAll(postal_code,"");
//                }


            } else {

                result = "Latitude: " + params[0].latitude + " Longitude: " + params[0].longitude +
                        "\n Unable to get address for this lat-long.";
                addressParts.add(result);

            }

        }
        return addressParts;
    }

    @Override
    protected void onPostExecute(ArrayList<String> strings) {
        super.onPostExecute(strings);
    }
//    public static ArrayList<String> getAddressParts()
//    {
//        if(addressParts.isEmpty())
//            addressParts=getAddressParts();
//        return getAddressParts();
//    }
}
