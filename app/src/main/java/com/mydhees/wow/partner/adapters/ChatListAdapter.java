package com.mydhees.wow.partner.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.objects.ChatObject;

import java.util.ArrayList;

/**
 * Created by Abhishek on 25-Aug-16.
 */

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater layoutInflater;
    ArrayList<ChatObject> ChatList = new ArrayList<>();


    public ChatListAdapter(Context context){
        this.context=context;
        layoutInflater = layoutInflater.from(context);
    }

    public void history(ChatObject history){
        ChatList.add(history);
        notifyItemInserted(ChatList.size());
    }

    public ArrayList<ChatObject> getDetaillist()
    {
        return ChatList;
    }
    public void clearAll(){
        ChatList.clear();
    }
    public void addAll(ArrayList<ChatObject> list){
        ChatList.clear();
        ChatList.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chats_row,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        ChatObject chatList = ChatList.get(position);
        holder.wishId.setText(chatList.getWishId()+"");
        holder.wishCity.setText(chatList.getWishCity()+"");




    }

    @Override
    public int getItemCount() {
        return ChatList.size();
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder{

       EditText wishId,wishCity;

        public RecyclerViewHolder(View itemView) {
            super(itemView);

            wishId = (EditText)itemView.findViewById(R.id.chatWishId);
            wishCity= (EditText)itemView.findViewById(R.id.chatWishCity);

        }
    }
}
