package com.mydhees.wow.partner.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.objects.purchasedHistory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by anshu on 19/05/16.
 */
public class PurchasedPointsadapter extends RecyclerView.Adapter<PurchasedPointsadapter.myviewholder> {
    Context context;
    LayoutInflater inflater;
    ArrayList<purchasedHistory> offers = new ArrayList<>();
    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public PurchasedPointsadapter(Context context) {
        this.context=context;
        inflater=LayoutInflater.from(context);
    }

    public void addoffer(purchasedHistory recievedoffer)
    {
        offers.add(0,recievedoffer);
        notifyItemInserted(offers.size());
    }
    public ArrayList<purchasedHistory> getofferlist()
    {
        return offers;
    }
    public void clearAll()
    {
        offers.clear();
    }

    @Override
    public myviewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.purchased_history_row,parent,false);
        myviewholder viewholder = new myviewholder(v);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(myviewholder holder, final int position) {

        purchasedHistory purchase_his = offers.get(position);
      // holder.room_type_tv.setText(current_offer.getRoomtype());



        holder.date.setText(dateFormat.format(purchase_his.getDate()));





        holder.amount.setText(purchase_his.getAmount());
        holder.orderId.setText(purchase_his.getOrderId());
        holder.transationID.setText(purchase_his.getTransationID());
        holder.status.setText(purchase_his.getStatus());
        holder.paymentType.setText(purchase_his.getPaymentType());
        if(purchase_his.getStatus().toString().equals("CHARGED")) {

            holder.imageView.setBackgroundResource(R.drawable.successful);
        }
        else{

            holder.imageView.setBackgroundResource(R.drawable.crossout);
        }

    }



    @Override
    public int getItemCount() {
        return offers.size();
    }

    static class myviewholder extends RecyclerView.ViewHolder
    {
        ImageView imageView;
        TextView date,amount,orderId,transationID,status,paymentType;


        public myviewholder(View itemView) {
            super(itemView);

            imageView = (ImageView)itemView.findViewById(R.id.tick);
            date = (TextView) itemView.findViewById(R.id.date_ph);
            amount = (TextView) itemView.findViewById(R.id.amount_ph);
            orderId = (TextView) itemView.findViewById(R.id.order_id_ph);
            transationID = (TextView) itemView.findViewById(R.id.tran_id_ph);
            status = (TextView) itemView.findViewById(R.id.status_ph);
            paymentType = (TextView) itemView.findViewById(R.id.payment_type_ph);

        }
    }
}
