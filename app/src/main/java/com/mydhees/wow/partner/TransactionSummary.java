package com.mydhees.wow.partner;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mydhees.wow.partner.allConstants.Constants;
import com.mydhees.wow.partner.credits_tab.Recharge;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class TransactionSummary extends AppCompatActivity {

    TextView pname,torder_id,pemail,tamount_paid,status,orderProcessedNot;
    Button backhome;
    ImageView successFailImg;
    String id,aborted;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    ArrayList<JSONObject> order_jsons = new ArrayList<>();
    HashSet<String> order_strings = new HashSet<>();

    VolleySingleton volleySingleton;
    RequestQueue requestQueue;
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = getIntent();
       id= intent.getStringExtra("order_id");
       aborted= intent.getStringExtra("aborted");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_summary);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        volleySingleton = VolleySingleton.getinstance();
        requestQueue = volleySingleton.getrequestqueue();
        successFailImg = (ImageView)findViewById(R.id.sucess_fail_img);

        torder_id = (TextView)findViewById(R.id.transtn_id);
        pemail = (TextView)findViewById(R.id.pemail);
        tamount_paid= (TextView)findViewById(R.id.amount_paid);
        status = (TextView)findViewById(R.id.statusp);
        backhome = (Button)findViewById(R.id.backhome);
        orderProcessedNot = (TextView) findViewById(R.id.textView11); // your order processed or sorry please try again


        try {
            fetchOrder();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        backhome.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("CommitTransaction")
            @Override
            public void onClick(View v) {
finish();Intent it=new Intent(TransactionSummary.this,Home.class);
                startActivity(it);     }
        });


    }
    private void fetchOrder() throws JSONException {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.ORDER_STATUS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respons) {
                        JSONObject response;
                       dialog.dismiss();
                        try {
                             response = new JSONObject(respons);

                           if (!aborted.equals("true")) {
                               torder_id.setText(response.get("order_id").toString());
                               pemail.setText(response.get("customer_email").toString());
                               orderProcessedNot.setText("Your Order has been Processed");
                               status.setText("Payment Succesfull!!!");
                               successFailImg.setBackgroundResource(R.drawable.successful);
                               tamount_paid.setText(response.get("amount").toString() + " " + response.get("currency").toString());
                           }
                            else{
                               orderProcessedNot.setTextColor(Color.RED);
                                orderProcessedNot.setText("Your Order has been aborted");
                               status.setText("Payment Failed!!!");
                               status.setTextColor(Color.RED);
                               torder_id.setText(response.get("order_id").toString());
                               pemail.setText(response.get("customer_email").toString());
                               tamount_paid.setText("Transaction Aborted");

                               successFailImg.setBackgroundResource(R.drawable.crossout);
                               backhome.setText("Try Payment again");
                               backhome.setOnClickListener(new View.OnClickListener() {
                                   @Override
                                   public void onClick(View v) {
                                       Intent intnt =new Intent(TransactionSummary.this, Recharge.class);
                                       startActivity(intnt);
                                   }
                               });


                           }

                            Log.e("resp",response.toString());
                        } catch (JSONException e) {
                            Log.e("error",e.toString());
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_LONG).show();
                Log.e("error",error.toString());
            }
        }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("orderId",id);

                return params;
            }
        };
        requestQueue.add(stringRequest);
    }




}
