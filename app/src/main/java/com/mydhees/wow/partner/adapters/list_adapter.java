package com.mydhees.wow.partner.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.objects.room;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by jashan
 */
public class list_adapter extends RecyclerView.Adapter<list_adapter.myviewholder> {

    private LayoutInflater layoutInflater;
    ArrayList<room> rooms = new ArrayList<>();
    Context context;

    public list_adapter(Context context)
    {
        this.context=context;
        layoutInflater = LayoutInflater.from(context);

    }
    public void addroom(room newroom)
    {
        rooms.add(0,newroom);
        notifyItemInserted(0);
    }

    @Override
    public list_adapter.myviewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.hotel_room_list_item,parent,false);
        myviewholder viewholder = new myviewholder(view);


        return viewholder;
    }

    @Override
    public void onBindViewHolder(final list_adapter.myviewholder holder, final int position) {

        room currentroom = rooms.get(position);
      ///  Toast.makeText(context,currentroom.getPrice()+"",Toast.LENGTH_LONG).show();
    //   holder.price.setText("" + currentroom.getPrice());
       holder.price.setText("" + currentroom.getPrice());
        holder.room_type.setText(""+currentroom.getRoom_type());
        holder.hotel.setText(""+currentroom.getHotel_name1());
        holder.roomnumber.setText(""+currentroom.getNo_room());

        holder.ac.setText(""+currentroom.getac());
        holder.children.setText(""+currentroom.getchildren());
        holder.adults.setText(""+currentroom.getadults());

        int numberofimages = currentroom.getImage_uris().size();
        if(numberofimages==0)
            holder.images_linear_layout.setVisibility(View.GONE);
        for(int i=0;i<numberofimages;i++)
        {
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    100,
                    100);
            lp.setMargins(5,5,5,5);
            ImageView image = new ImageView(context);
            image.setLayoutParams(lp);
            Bitmap bitmap = BitmapFactory.decodeFile(currentroom.getImage_uris().get(i));
            image.setImageBitmap(bitmap);
//            image.setImageURI(currentroom.getImage_uris().get(i));
            holder.images_linear_layout.addView(image);
        }
        if(!Arrays.asList(currentroom.getFacilites()).contains(true))
            holder.facilites_linear_layout.setVisibility(View.GONE);
        int [] draw = new int[]{
                R.drawable.ic_wifi_black_24dp1,
                R.drawable.ic_restaurant_menu_black_24dp,
                R.drawable.ic_live_tv_black_24dp,
                R.drawable.reg_hotel_gym
        };
        for(int i=0;i<4;i++)
        {
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    40,
                    40);
            lp.setMargins(5,5,5,5);
            if(currentroom.getFacilites()[i]) {
                ImageView image = new ImageView(context);
                image.setMaxHeight(50);
                image.setMaxWidth(50);
                image.setLayoutParams(lp);
                image.setImageDrawable(ContextCompat.getDrawable(context, draw[i]));
//                image.setImageURI(currentroom.getImage_uris().get(i));
                holder.facilites_linear_layout.addView(image);
            }
        }

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rooms.remove(position);
                notifyDataSetChanged();
            }
        });



    }

    @Override
    public int getItemCount() {
        return rooms.size();
    }
    public void clearAll(){
        rooms.clear();
    }

    static class myviewholder extends RecyclerView.ViewHolder{
        private EditText price,hotel,roomnumber;

        private EditText room_type;
        private EditText ac,adults,children;
        private LinearLayout images_linear_layout,facilites_linear_layout;
        Button delete;

        public myviewholder(View itemview)
        {
            super(itemview);
            price= (EditText) itemview.findViewById(R.id.sales_price_tv);
            hotel= (EditText) itemview.findViewById(R.id.hotelname1_tv);
            roomnumber= (EditText) itemview.findViewById(R.id.room_no_tv);

            room_type= (EditText) itemview.findViewById(R.id.room_type_tv);
            ac= (EditText) itemview.findViewById(R.id.ac_tv);
            adults= (EditText) itemview.findViewById(R.id.adults_tv);
            children= (EditText) itemview.findViewById(R.id.children_tv);
            images_linear_layout=(LinearLayout)itemview.findViewById(R.id.images_linear_layout);
            facilites_linear_layout=(LinearLayout)itemview.findViewById(R.id.facilities_linear_layout);
            delete = (Button)itemview.findViewById(R.id.delete);

        }

    }
}
