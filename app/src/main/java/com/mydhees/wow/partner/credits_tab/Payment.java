package com.mydhees.wow.partner.credits_tab;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.cooltechworks.creditcarddesign.CardEditActivity;
import com.cooltechworks.creditcarddesign.CreditCardUtils;
import com.cooltechworks.creditcarddesign.CreditCardView;
import com.mydhees.wow.partner.R;

/**
 * Created by jashan on 06-06-2016.
 */
public class Payment extends Activity {
int status;
    Button pay,add_card;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
View show_card =(RadioButton)findViewById(R.id.card);

        LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        final int GET_NEW_CARD = 0;
        show_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(status==0) {
                    LinearLayout hiddenLayout = (LinearLayout) findViewById(R.id.card_view_wow);
                    if (hiddenLayout == null) {
                        LinearLayout myLayout = (LinearLayout) findViewById(R.id.debit_view);
                        View hiddenInfo = getLayoutInflater().inflate(R.layout.credit_card, myLayout, false);
                        myLayout.addView(hiddenInfo);
                        status=1; //activated
                        Intent intent = new Intent(Payment.this, CardEditActivity.class);
                        startActivityForResult(intent, 0);

                    }
                }
                else {
                    View myView = findViewById(R.id.card_view_wow);
                    ViewGroup parent = (ViewGroup) myView.getParent();
                    parent.removeView(myView);
                    status=0;


                }


            }
        });

        //LinearLayout myLayout = (LinearLayout) findViewById(R.id.debit_view);

       // View vi = inflater.inflate(R.layout.credit_card,myLayout,false);


    }
    private void initListener(final int index, CreditCardView creditCardView) {


        creditCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CreditCardView creditCardView = (CreditCardView) v;
                String cardNumber = creditCardView.getCardNumber();
                String expiry = creditCardView.getExpiry();
                String cardHolderName = creditCardView.getCardHolderName();

                Intent intent = new Intent(Payment.this, CardEditActivity.class);
                intent.putExtra(CreditCardUtils.EXTRA_CARD_HOLDER_NAME, cardHolderName);
                intent.putExtra(CreditCardUtils.EXTRA_CARD_NUMBER, cardNumber);
                intent.putExtra(CreditCardUtils.EXTRA_CARD_EXPIRY, expiry);
                intent.putExtra(CreditCardUtils.EXTRA_CARD_SHOW_CARD_SIDE, CreditCardUtils.CARD_SIDE_FRONT);
               // intent.putExtra(CreditCardUtils.EXTRA_VALIDATE_EXPIRY_DATE, false);


                startActivityForResult(intent, index);

            }
        });

    }
    public void onActivityResult(int reqCode, int resultCode, Intent data) {

        if(resultCode == RESULT_OK) {

            LinearLayout cardContainer = (LinearLayout) findViewById(R.id.debit_view);


            String name = data.getStringExtra(CreditCardUtils.EXTRA_CARD_HOLDER_NAME);
            String cardNumber = data.getStringExtra(CreditCardUtils.EXTRA_CARD_NUMBER);
            String expiry = data.getStringExtra(CreditCardUtils.EXTRA_CARD_EXPIRY);
            String cvv = data.getStringExtra(CreditCardUtils.EXTRA_CARD_CVV);


            if(reqCode == 0) {


                CreditCardView creditCardView = new CreditCardView(this);

                creditCardView.setCVV(cvv);
                creditCardView.setCardHolderName(name);
                creditCardView.setCardExpiry(expiry);
                creditCardView.setCardNumber(cardNumber);

                int index = cardContainer.getChildCount();
                cardContainer.addView(creditCardView);
                initListener(index, creditCardView);



            }
            else {

                CreditCardView creditCardView = (CreditCardView) cardContainer.getChildAt(reqCode);

                creditCardView.setCardExpiry(expiry);
                creditCardView.setCardNumber(cardNumber);
                creditCardView.setCardHolderName(name);
                creditCardView.setCVV(cvv);

            }
        }


    }
}

