package com.mydhees.wow.partner.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.mydhees.wow.partner.R;


import java.util.ArrayList;

/**
 * Created by jashan on 01-06-2016.
 */
public class PolicyAdapter extends RecyclerView.Adapter<PolicyAdapter.myviewholder> {


    private LayoutInflater layoutInflater1;
    ArrayList<String> policy = new ArrayList<>();
    Context context;
    int flag=1;

    public PolicyAdapter(Context context)
    {
        this.context=context;
        layoutInflater1 = LayoutInflater.from(context);

    }
    public ArrayList<String> getPolicies()
    {
        return policy;
    }
    public void addpolicy(String p)
    {

        policy.add(p);
        notifyItemInserted(policy.size());
    }
    public void clearAll(){
        policy.clear();
    }
    public ArrayList<String> getPolicy(){
        return policy;
    }


    @Override
    public PolicyAdapter.myviewholder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = layoutInflater1.inflate(R.layout.policy_row,parent,false);
        myviewholder viewholder = new myviewholder(view);

        return viewholder;
    }

    @Override
    public void onBindViewHolder(final PolicyAdapter.myviewholder holder, final int position) {

        holder.p1.setText(policy.get(position));
        holder.del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                policy.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, policy.size());
            }
        });
        if(flag==0)
            holder.del.setVisibility(View.INVISIBLE);
        else
            holder.del.setVisibility(View.VISIBLE);

    }

    @Override
    public int getItemCount() {
        return policy.size();
    }


    static class myviewholder extends RecyclerView.ViewHolder{
        private TextView p1;
        private Button del;


        public myviewholder(View itemview)
        {
            super(itemview);
            p1= (TextView )itemview.findViewById(R.id.policy12);
            del= (Button) itemview.findViewById(R.id.del_policy);

        }

    }
    public void disable(){
        flag=0;
        notifyDataSetChanged();
    }
    public void enable(){
        flag=1;
        notifyDataSetChanged();
    }
}
