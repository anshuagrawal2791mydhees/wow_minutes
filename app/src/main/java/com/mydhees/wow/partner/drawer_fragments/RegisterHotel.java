package com.mydhees.wow.partner.drawer_fragments;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mydhees.wow.partner.Home;
import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.VolleySingleton;
import com.mydhees.wow.partner.adapters.ImageRecyclerAdapter;
import com.mydhees.wow.partner.adapters.PolicyAdapter;
import com.mydhees.wow.partner.adapters.PolicyAdapter_Update;
import com.mydhees.wow.partner.allConstants.Constants;
import com.mydhees.wow.partner.allConstants.HotelRegistrationConstants;
import com.mydhees.wow.partner.objects.image;
import com.mydhees.wow.partner.registration_and_login.select_service;
import com.mydhees.wow.partner.registration_and_login.user_details;
import com.mydhees.wow.partner.utils.DividerItemDecoration;
import com.mydhees.wow.partner.utils.ImageCompressionAsyncTask;
import com.mydhees.wow.partner.utils.LocationUtility;
import com.mydhees.wow.partner.utils.MapUtility2;
import com.mydhees.wow.partner.utils.task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterHotel extends Fragment implements OnMapReadyCallback {


    public RegisterHotel() {
        // Required empty public constructor
    }


    private RecyclerView recycler,policy;
    private LinearLayoutManager mLayoutManager;

    ImageView wifi,food,parking,pool,tv,gym,business_services,coffeeshop,frontdesk,laundary_services,outdoor,health_spa,room_service,spa,travel_asst;

    private static int RESULT_LOAD_IMG = 1;
    String imgDecodableString;
    MapUtility2 locationGetter;
    EditText street_address;
    TextView street_address2;
    private GoogleMap mMap;
    MarkerOptions mo;
    Marker marker;
    ArrayList<String> addressParts = new ArrayList<>();
    ArrayList<String> addressParts2 = new ArrayList<>();
    String locationAddress = null;
    String locality = null;
    String country = null,postal_code=null;
    LocationUtility getAddress;
    LocationUtility getAddress2;
    int PLACE_PICKER_REQUEST = 1;
    ArrayList<image> images = new ArrayList<>();
    ImageRecyclerAdapter adapter;
    Button add_image,add_policy;
    private int PICK_IMAGE_REQUEST = 2;
    String image_description;
    Uri uri;
    ScrollView scrollView;
    EditText policy1,hotel_name,no_of_rooms,mobile,phone,htype;
    ArrayAdapter adapter_p;
    ListView policy_listview;
    ArrayList<image> images_uris = new ArrayList<>();
    boolean error,image_error;
    String hotelType = "";
    Button next;
    Button finish;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    PolicyAdapter policy_Adapter;

    AlertDialog.Builder builderDialog;
    AlertDialog alert;
    RatingBar ratingBar;
    ArrayList<String> faciliites = new ArrayList<>();
    Button addMoreFacilities;
    String[] extra_facilities;
    boolean[] extra_facilities_selected;
    int number=1;
    LinearLayout more_facilities;
    TextView policy_hint;
    CardView cardView;
    HashMap<String,String> data = new HashMap<>();
    SharedPreferences profile;
    SharedPreferences.Editor editor;
    LatLng hotel_location;
    String city = "";
    Boolean[] facilites_array = new Boolean[14];
    task register_hotel;
    PolicyAdapter_Update upated_Adapter;
    List<String> hotel_policies = new ArrayList<>();
    VolleySingleton volleySingleton;
    RequestQueue requestQueue;

    ImageCompressionAsyncTask imageCompressionAsyncTask;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_register_hotel, container, false);

        profile = getActivity().getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE,Context.MODE_PRIVATE);
        editor = profile.edit();
hideKeyboard(getContext());
        htype= (EditText) v.findViewById(R.id.htype);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map2);
        mapFragment.getMapAsync(this);
           finish = (Button)v.findViewById(R.id.finish);
        getAddress = new LocationUtility(getContext());
       hotel_name=(EditText) v.findViewById(R.id.hname);
        no_of_rooms=(EditText) v.findViewById(R.id.roomn);
        phone=(EditText) v.findViewById(R.id.phone);
        mobile=(EditText) v.findViewById(R.id.mobile);
        street_address = (EditText) v.findViewById(R.id.street_address);
        street_address2 = (TextView) v.findViewById(R.id.street_address2);
        recycler = (RecyclerView) v.findViewById(R.id.recycler_view);
        recycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        adapter = new ImageRecyclerAdapter(getContext());
        recycler.setAdapter(adapter);
        add_image = (Button) v.findViewById(R.id.add_image);
        scrollView = (ScrollView) v.findViewById(R.id.scrollView);
        policy1 = (EditText) v.findViewById(R.id.policy1);
        add_policy = (Button)v.findViewById(R.id.add_policy);
        addMoreFacilities = (Button)v.findViewById(R.id.add_more_facilities);
        more_facilities = (LinearLayout)v.findViewById(R.id.more_facilities);
        policy_hint=(TextView)v.findViewById(R.id.policy_hint);
        cardView = (CardView)v.findViewById(R.id.card_view);

        volleySingleton = VolleySingleton.getinstance();
        requestQueue=volleySingleton.getrequestqueue();

        //Facilities icons
        wifi= (ImageView) v.findViewById(R.id.fa1);
        wifi.setTag("wifi");
        food= (ImageView) v.findViewById(R.id.fa2);
        food.setTag("food");
        tv= (ImageView) v.findViewById(R.id.fa3);
        tv.setTag("tv");
        gym= (ImageView) v.findViewById(R.id.fa4);
        gym.setTag("gym");

         builderDialog = new AlertDialog.Builder(getContext());
        final String[] dialogList = getResources().getStringArray(R.array.hotel_type);

       //Rating bar work here
        ratingBar = (RatingBar)v.findViewById(R.id.ratingBar);



        htype.setOnClickListener(new View.OnClickListener() {
        @Override
         public void onClick(View v) {

             builderDialog.setTitle("Choose Hotel Type")
                    .setItems(R.array.hotel_type, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // The 'which' argument contains the index position
                            // of the selected item
                            hotelType = dialogList[which];
                            alert.dismiss();
                            htype.setText(hotelType);
                        }
                    });
                alert = builderDialog.show();


        }

        });
        upated_Adapter = new PolicyAdapter_Update(hotel_policies,getContext());
        policy_Adapter = new PolicyAdapter(getContext());
        policy = (RecyclerView) v.findViewById(R.id.policy_list);
        policy.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST);
        policy.addItemDecoration(itemDecoration);
        policy.setItemAnimator(new DefaultItemAnimator());
        policy.setAdapter(upated_Adapter);

        ImageView.OnClickListener onClick = new ImageView.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!faciliites.contains(v.getTag().toString()))
                {
                   faciliites.add(v.getTag().toString());
                    ImageView view = (ImageView)v;
                    view.setColorFilter(Color.parseColor("#009688"));

                }
                else {
                    faciliites.remove(v.getTag().toString());
                    ImageView view = (ImageView)v;
                    view.setColorFilter(Color.GRAY);
                }

            }
        };
        extra_facilities = new String []{"buisness services","coffee shop","front desk","spa","laundry","outdoor games","parking","room services","swimming","travel assistance"};
        extra_facilities_selected = new boolean[]{false,false,false,false,false,false,false,false,false,false};

        addMoreFacilities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMultiChoiceItems(extra_facilities,extra_facilities_selected,new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        if (isChecked) {
                            // If the user checked the item, add it to the selected items
                            extra_facilities_selected[which]=true;

                        } else if(extra_facilities_selected[which]==true) {
                            // Else, if the item is already in the array, remove it
                            extra_facilities_selected[which]=false;
                        }
                    }
                }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int hastrue = 0;
                        more_facilities.removeAllViews();
                        for(int i=0;i<extra_facilities_selected.length;i++)
                        {
                            if(extra_facilities_selected[i]==true)
                            {
                                hastrue=1;
                                more_facilities.setVisibility(View.VISIBLE);
                                ImageView image = new ImageView(getContext());
                                int id=R.drawable.ic_launcher;
                                if(i==0)
                                    id = R.drawable.businesservice;
                                else if(i==1)
                                    id = R.drawable.coffeeshop;
                                else if(i==2)
                                    id = R.drawable.frontdesk;
                                else if(i==3)
                                    id = R.drawable.spa;
                                else if(i==4)
                                    id = R.drawable.laundryservice;
                                else if(i==5)
                                    id = R.drawable.outdoor;
                                else if(i==6)
                                    id = R.drawable.parking;
                                else if(i==7)
                                    id = R.drawable.roomservice;
                                else if(i==8)
                                    id = R.drawable.reg_hotel_swim;
                                else if(i==9)
                                    id = R.drawable.travel_assistance;

                                image.setImageResource(id);
                                more_facilities.addView(image);
                            }
                        }
                        if(hastrue==0)
                            more_facilities.setVisibility(View.GONE);
                        dialog.dismiss();
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
            }
        });

        wifi.setOnClickListener(onClick);
        food.setOnClickListener(onClick);
        tv.setOnClickListener(onClick);
        gym.setOnClickListener(onClick);



        finish.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {

                if(hotel_name.getText().toString().equals("")){
                    hotel_name.setError("Required");
                    hotel_name.requestFocus();
                }
                else if(no_of_rooms.getText().toString().equals("")){
                    no_of_rooms.setError("Required");
                    no_of_rooms.requestFocus();

                }
                else if(phone.getText().toString().equals("")){
                    phone.setError("Required");
                    phone.requestFocus();
                }
                else if(mobile.getText().toString().equals("") || !(mobile.getText().toString().length()==10)){
                    mobile.setError("Required 10 Digits");
                    mobile.requestFocus();
                }

                else if(street_address.getText().toString().equals("")){
                    street_address.setError("Please click on map to get address!!!");
                    street_address.requestFocus();
                }



//                else if(a=="" || b=="" || c=="" || d==""|| e==""|| f=="" ){
//
//                    Toast.makeText(getContext(), "Select atleast one facility!!!", Toast.LENGTH_LONG).show();
//
//                }
               else {
                    data.put(HotelRegistrationConstants.OWNER_ID,profile.getString(Constants.ID,"default"));
                    data.put(HotelRegistrationConstants.HOTEL_NAME,hotel_name.getText().toString());
                    data.put(HotelRegistrationConstants.HOTEL_TYPE,htype.getText().toString());
                    data.put(HotelRegistrationConstants.NO_OF_ROOMS,no_of_rooms.getText().toString());
                    data.put(HotelRegistrationConstants.PHONE_NUMBER,phone.getText().toString());
                    data.put(HotelRegistrationConstants.MOBILE_NUMBER,mobile.getText().toString());
                    data.put(HotelRegistrationConstants.ADDRESS_LINE1,street_address.getText().toString());
                    data.put(HotelRegistrationConstants.ADDRESS_LINE2,street_address2.getText().toString());
                    data.put(HotelRegistrationConstants.ADDRESS,street_address.getText().toString().concat(street_address2.getText().toString()));
                    data.put(HotelRegistrationConstants.PIN,postal_code);
                    data.put(HotelRegistrationConstants.LONGITUDE,hotel_location.longitude+"");
                    data.put(HotelRegistrationConstants.LATITUDE,hotel_location.latitude+"");
                    data.put(HotelRegistrationConstants.CITY,city);
                    data.put(HotelRegistrationConstants.STAR_HOTEL,ratingBar.getRating()+"");
                    data.put(HotelRegistrationConstants.EMAIL,profile.getString(Constants.EMAIL,"default"));

                    for(int i=0;i<facilites_array.length;i++)
                    {
                        facilites_array[i]=false;
                        if(i>=4)
                        {
                            facilites_array[i]=extra_facilities_selected[i-4];
                        }
                    }
                    if(faciliites.contains("wifi"))
                        facilites_array[0]=true;
                    if(faciliites.contains("food"))
                        facilites_array[1]=true;
                    if(faciliites.contains("tv"))
                        facilites_array[2]=true;
                    if(faciliites.contains("gym"))
                        facilites_array[3]=true;
                    String[] facilites_keys = new String[]{HotelRegistrationConstants.HAS_WIFI,
                    HotelRegistrationConstants.HAS_DINING,
                    HotelRegistrationConstants.HAS_TV,
                    HotelRegistrationConstants.HAS_GYM,
                    HotelRegistrationConstants.HAS_BUSINESS_SERVICE,
                    HotelRegistrationConstants.HAS_COFFEE_SHOP,
                    HotelRegistrationConstants.HAS_FRONT_DESK,
                    HotelRegistrationConstants.HAS_SPA,
                    HotelRegistrationConstants.HAS_LAUNDRY,
                    HotelRegistrationConstants.HAS_OUT_DOOR_GAMES,
                    HotelRegistrationConstants.HAS_PARKING,
                    HotelRegistrationConstants.HAS_ROOM_SERVICE,
                    HotelRegistrationConstants.HAS_SWIMMING,
                    HotelRegistrationConstants.HAS_TRAVEL_ASSISTANCE};
                    JSONObject facilitesJSON=new JSONObject();
                    for(int i=0;i<14;i++)
                    {
                            try {
                                facilitesJSON.put(facilites_keys[i],facilites_array[i]);
                            } catch (JSONException e) {
                                Log.e("error",e.toString());
                            }
                    }
                    data.put(HotelRegistrationConstants.FACILITIES,facilitesJSON.toString());
                    ArrayList<String> pol = new ArrayList<String>();
                    //pol.addAll(policy_Adapter.getPolicies());
                    pol.addAll(hotel_policies);
                    String[] policies = pol.toArray(new String[pol.size()]);
                    try {
                        JSONArray policiesJSON = new JSONArray(policies);
                        data.put(HotelRegistrationConstants.POLICIES,policiesJSON.toString());
                    } catch (JSONException e) {
                        Log.e("err",e.toString());
                    }


                    //data.put(HotelRegistrationConstants.POLICIES,hotel_name.getText().toString());

                    final ProgressDialog dialog = new ProgressDialog(getContext());
                    dialog.show();
                    register_hotel = new task(getContext(),images_uris, Constants.HOTEL_REGISTER_URL,data){
                        @Override
                        protected void onPostExecute(Response response) {
                            dialog.dismiss();
//                            try {
//                                Toast.makeText(getContext(), response.body().string(), Toast.LENGTH_LONG).show();
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
//                            try {
//                                Log.e("response",response.body().string().toString());
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
//                            HashSet<String> registered_hotels_ids = new HashSet<String>();
                            HashSet<String> registered_hotels = new HashSet<String>();
//                            HashSet<String> registered_hotels_names = new HashSet<String>();
//                            HashSet<String> registered_hotels_no_of_rooms = new HashSet<String>();

                            try {
                                JSONObject resp = new JSONObject(response.body().string());
                                Log.e("respJSOn",resp.toString());
                                    registered_hotels.add(resp.toString());
//                                registered_hotels_ids.add(resp.getString("hotelId"));
//                                registered_hotels_names.add(resp.getString("hotelName"));
//                                registered_hotels_no_of_rooms.add(resp.getString("noOfRooms"));
                            } catch (JSONException e) {
                                Log.e("err",e.toString());
                            } catch (IOException e) {
                                Log.e("err",e.toString());
                            }

                            if(profile.getStringSet(Constants.REGISTERED_HOTELS, new HashSet<String>()).size()>0){
                                Log.e("storing","not_first");
                                //Log.e("data",profile.getStringSet(Constants.REGISTERED_HOTELS_NAMES,null).toString());
                                registered_hotels.addAll(profile.getStringSet(Constants.REGISTERED_HOTELS,null));

//                                registered_hotels_ids.addAll(profile.getStringSet(Constants.REGISTERED_HOTELS_IDS,null));
//                                registered_hotels_names.addAll(profile.getStringSet(Constants.REGISTERED_HOTELS_NAMES,null));
//                                registered_hotels_no_of_rooms.addAll(profile.getStringSet(Constants.REGISTERED_HOTELS_NO_OF_ROOMS,null));
                            }
                            else {
                                Log.e("storing","first");
                            }
                            Log.e("hotels",registered_hotels.toString());

                            editor.putStringSet(Constants.REGISTERED_HOTELS, registered_hotels);
//                            editor.putStringSet(Constants.REGISTERED_HOTELS_NAMES, registered_hotels_names);
//                            editor.putStringSet(Constants.REGISTERED_HOTELS_NO_OF_ROOMS, registered_hotels_no_of_rooms);
                            editor.commit();
                            mFragmentManager = getActivity().getSupportFragmentManager();
                            mFragmentTransaction = mFragmentManager.beginTransaction().setCustomAnimations(R.anim.card_flip_left_out,R.anim.card_flip_right_in);//R.anim.card_flip_right_in,R.anim.card_flip_right_out,R.anim.card_flip_left_in,R.anim.card_flip_left_out);
                            mFragmentTransaction.replace(R.id.containerView,new LeadsDashboard()).commit();
                            super.onPostExecute(response);
                        }
                    };


                    register_hotel.execute();

                }


            }
        });


        add_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i,PICK_IMAGE_REQUEST);
            }
        });



        add_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                error = false;
                if(policy1.getText().toString().matches(""))
                {
                    policy1.setError("required");
                    error=true;
                }
                else{
                    if(policy_Adapter.getPolicy().isEmpty())
                    {
                        policy_hint.setVisibility(View.GONE);
                        cardView.setVisibility(View.VISIBLE);
                    }
                   //String a= policy1.getText().toString();
                    policy1.setError(null);
                    //policy_Adapter.addpolicy("-"+policy1.getText().toString());
                    hotel_policies.add(policy1.getText().toString());
                    upated_Adapter.notifyDataSetChanged();
                    policy.scrollToPosition(policy_Adapter.getItemCount()-1);
                    policy1.setText("");

                }


            }
        });


        return v;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Toast.makeText(getContext(), "Please Wait...", Toast.LENGTH_SHORT).show();
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }


            }
        });
        locationGetter = new MapUtility2(getContext()){
            @Override
            protected void onPostExecute(Location location) {
                if(location == null)
                {
                    Toast.makeText(getContext(),"can't locate you!",Toast.LENGTH_LONG).show();
                    mo = new MarkerOptions().position(new LatLng(0,0)).title("Tap to edit your business location");
                    marker = mMap.addMarker(mo);
                    marker.showInfoWindow();


                }
                else {
                    LatLng sydney = new LatLng(location.getLatitude(), location.getLongitude());
                    mo = new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).title("Tap to edit your business location");

                    marker = mMap.addMarker(mo);
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                    //marker.setSnippet("YOU");
                    marker.showInfoWindow();

                    if (location.getLatitude() != 0.0) {

                        try {
                            addressParts.clear();
                            addressParts.addAll(getAddress.execute(new LatLng(location.getLatitude(), location.getLongitude())).get());
                        } catch (InterruptedException e) {
                            Log.e("err",e.toString());
                        } catch (ExecutionException e) {
                            Log.e("err",e.toString());
                        } finally {
                            locationAddress = addressParts.get(0);
                            if (addressParts.size() > 1) {
                                locality = addressParts.get(1);
                                country = addressParts.get(2);
                                postal_code = addressParts.get(3);
                                city=addressParts.get(4);
                                locationAddress = locationAddress.replaceAll("null", "");
                                locationAddress = locationAddress.replaceAll(locality, "");
                            }


                            if (!locationAddress.contains("Unable")) {
                                street_address.setText(locationAddress);
                                street_address2.setText(locality + ", " + country);
                            }

                            marker.setSnippet(locationAddress);
                            marker.showInfoWindow();
                        }


                        //Toast.makeText(getApplicationContext(),postal_code,Toast.LENGTH_LONG).show();
                        hotel_location = new LatLng(location.getLatitude(),location.getLongitude());

                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                                new LatLng(location.getLatitude(), location.getLongitude()), 13));


                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(new LatLng(location.getLatitude(), location.getLongitude()))      // Sets the center of the map to location user
                                .zoom(13)                   // Sets the zoom
                                .bearing(90)                // Sets the orientation of the camera to east
                                // Sets the tilt of the camera to 30 degrees
                                .build();                   // Creates a CameraPosition from the builder
                        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    }


                }

            }
        };
        locationGetter.execute();
    }

    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        getAddress2 = new LocationUtility(getContext());
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == getActivity().RESULT_OK) {
                Place place = PlacePicker.getPlace(data, getContext());
                String toastMsg = String.format("Place: %s", place.getName());
                Toast.makeText(getContext(), toastMsg, Toast.LENGTH_LONG).show();
                marker.setPosition(place.getLatLng());
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                        place.getLatLng(), 13));

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(place.getLatLng())      // Sets the center of the map to location user
                        .zoom(13)                   // Sets the zoom
                        .bearing(90)                // Sets the orientation of the camera to east
                        // Sets the tilt of the camera to 30 degrees
                        .build();                   // Creates a CameraPosition from the builder
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//                marker.setSnippet(place.getAddress().toString());
//                marker.showInfoWindow();
//                editor.putFloat(Constants.LATITUDE, (float) place.getLatLng().latitude);
//                editor.putFloat(Constants.LONGITUDE,(float)place.getLatLng().longitude);
//                editor.commit();
                //street_address.setText(place.getAddress());

                try {
                    addressParts2.clear();
                    addressParts2.addAll(getAddress2.execute(place.getLatLng()).get());

                } catch (InterruptedException e) {
                    Toast.makeText(getContext(), e.toString(), Toast.LENGTH_SHORT).show();
                } catch (ExecutionException e) {
                    Toast.makeText(getContext(), e.toString(), Toast.LENGTH_SHORT).show();
                } finally {
                    //Toast.makeText(getApplicationContext(),addressParts2.toString(),Toast.LENGTH_SHORT).show();

                    locationAddress = addressParts2.get(0);
                    if (addressParts2.size() > 1) {
                        locality = addressParts2.get(1);
                        country = addressParts2.get(2);
                        postal_code = addressParts2.get(3);
                        city = addressParts2.get(4);
                        Log.e("city",city);
                        locationAddress = locationAddress.replaceAll("null", "");
                        locationAddress = locationAddress.replaceAll(locality, "");
                    }

                    hotel_location = new LatLng(place.getLatLng().latitude,place.getLatLng().longitude);


                    if (!locationAddress.contains("Unable")) {
                        street_address.setText(locationAddress);
                        street_address2.setText(locality + ", " + country);
                    }
                    marker.setSnippet(locationAddress);
                    marker.showInfoWindow();
                    checkcity(city);

                }


                //street_address2.setText(place.getLocale);
            }
        }
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == getActivity().RESULT_OK && data != null && data.getData() != null) {



            uri = data.getData();


            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("Description");
            alertDialog.setMessage("Enter Description");

            final EditText input = new EditText(getContext());
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            input.setLayoutParams(lp);
            alertDialog.setView(input);
            alertDialog.setIcon(R.drawable.description);
            alertDialog.setCancelable(false);


            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            image_error=false;
                            image_description = input.getText().toString();
                            if (image_description.matches("")) {
                                Toast.makeText(getContext(), "Description Required", Toast.LENGTH_SHORT).show();
                               image_error  = true;
                            }
                            if(image_error==false){
                                try {
                                    String filepath = new ImageCompressionAsyncTask(true,getContext()).execute(data.getDataString()).get();
                                    Log.e("filepath",filepath);
//                                    Log.e("stringdata",data.getDataString());
                                    Log.e("uri",uri.toString());
                                    image im = new image(image_description,filepath);
                                    adapter.addImage(im);
                                    images_uris.add(im);
//                                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
//                                View view = getActivity().getCurrentFocus();
//                                imm.hideSoftInputFromWindow(view.getWindowToken(),
//                                        InputMethodManager.RESULT_UNCHANGED_SHOWN);
                                    dialog.dismiss();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                } catch (ExecutionException e) {
                                    e.printStackTrace();
                                }

                            }
                        }

                    });
            alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    hideSoftKeyboard();

                }
            });


            alertDialog.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
            alertDialog.show();


//            try {
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
//                // Log.d(TAG, String.valueOf(bitmap));
//
//                ImageView imageView = (ImageView) findViewById(R.id.imageView);
//                imageView.setImageBitmap(bitmap);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        }
    }

    private void checkcity(final String city) {
        final ProgressDialog dialog = new ProgressDialog(getContext());
        dialog.show();
        final ArrayList<String> cities_list = new ArrayList<>();
        StringRequest request = new StringRequest(Request.Method.POST, Constants.GET_CITY_LIST,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String respons) {
                        dialog.dismiss();
                        Log.e("response",respons.toString());
                        JSONObject response = null;
                        try {
                            response = new JSONObject(respons);
                        } catch (JSONException e) {
                            Log.e("error",e.toString());
                        }
                        try {
                            if(response.getBoolean("res"))
                            {
                                cities_list.add("Select City");
                                JSONArray cities = response.getJSONArray("cities");
                                for(int i=0;i<cities.length();i++)
                                    cities_list.add(cities.getString(i));

                               if(!cities_list.contains(city)){
                                   Toast.makeText(getContext(),"This place is not operational yet",Toast.LENGTH_LONG).show();
                                   street_address.setText("");
                                   street_address2.setText("");


                               }

                            }
                            else{
                                Toast.makeText(getContext(),"This service is not operational yet",Toast.LENGTH_LONG).show();
                                FragmentManager mFragmentManager;
                                FragmentTransaction mFragmentTransaction;
                                mFragmentManager = getActivity().getSupportFragmentManager();
                                mFragmentTransaction = mFragmentManager.beginTransaction().setCustomAnimations(R.anim.card_flip_left_out, R.anim.card_flip_right_in);//R.anim.card_flip_right_in,R.anim.card_flip_right_out,R.anim.card_flip_left_in,R.anim.card_flip_left_out);
                                mFragmentTransaction.replace(R.id.containerView,new LeadsDashboard()).commit();
                            }

                        } catch (JSONException e) {
                            Log.e("volley error",e.toString());
                            FragmentManager mFragmentManager;
                            FragmentTransaction mFragmentTransaction;
                            mFragmentManager = getActivity().getSupportFragmentManager();
                            mFragmentTransaction = mFragmentManager.beginTransaction().setCustomAnimations(R.anim.card_flip_left_out, R.anim.card_flip_right_in);//R.anim.card_flip_right_in,R.anim.card_flip_right_out,R.anim.card_flip_left_in,R.anim.card_flip_left_out);
                            mFragmentTransaction.replace(R.id.containerView,new LeadsDashboard()).commit();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(getContext(),error.toString(),Toast.LENGTH_LONG).show();
                Log.e("vollley error",error.toString());
                FragmentManager mFragmentManager;
                FragmentTransaction mFragmentTransaction;
                mFragmentManager = getActivity().getSupportFragmentManager();
                mFragmentTransaction = mFragmentManager.beginTransaction().setCustomAnimations(R.anim.card_flip_left_out, R.anim.card_flip_right_in);//R.anim.card_flip_right_in,R.anim.card_flip_right_out,R.anim.card_flip_left_in,R.anim.card_flip_left_out);
                mFragmentTransaction.replace(R.id.containerView,new LeadsDashboard()).commit();
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
//                params.put(Constants.NAME,name.getText().toString());
                params.put("service","hotel");
//                params.put(Constants.PASSWORD, password.getText().toString());
//                params.put(Constants.PHONE, mobile_number.getText().toString());
//                params.put(Constants.CITY, city.getSelectedItem().toString());
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }

    private final void focusOnView(final View view) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0, view.getBottom());
            }
        });
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void hideSoftKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            focusOnView(policy1);
        }


    }


    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

}

