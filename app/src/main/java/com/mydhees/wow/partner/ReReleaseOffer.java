package com.mydhees.wow.partner;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mydhees.wow.partner.allConstants.AddroomConstants1;
import com.mydhees.wow.partner.allConstants.Constants;
import com.mydhees.wow.partner.allConstants.HotelRegistrationConstants;
import com.mydhees.wow.partner.allConstants.Release_constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by Jashan Kochar on 8/19/2016.
 */
public class ReReleaseOffer extends AppCompatActivity {

    EditText no_rooms,sales_price,check_in,check_in_time,check_out,check_out_time,schedule_date,schedule_time,offername,hotel,type;
    SharedPreferences profile;
    SeekBar minute_seek;
    String min_exp;
    int mYear;
    Calendar c = Calendar.getInstance();
    Calendar d = Calendar.getInstance();
    int status;
    int mMonth;
    int mDay;
    int mHour,mHour1;
    int mMinute,mMinute1;
    String hour1,min1;
    boolean error=false;
    SharedPreferences.Editor editor;
    TextView offer_time,offer_time1;
    Button releaseOffers;
    DateFormat simpledateformat12= new SimpleDateFormat("dd-MM-yyyy");
    DateFormat simpledateformat1= new SimpleDateFormat("yyyy-MM-dd");
    DateFormat simpledateformat123= new SimpleDateFormat("HH:mm");
    DateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
    String checkindatestring,scheduleDatestring,scheduleTimestring,expiryTimestring="",expireStringDate;
    String checkoutdatestring;
    String checkintimestring;
    Calendar as = Calendar.getInstance();
    String checkouttimestring;
    Date checkin_date,sched_date;
    Date checkout_date,expire_date;
    String checkinstring,scheduledatestring,expire_string;
    String checkoutstring;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    String rid,hid,roomNo,hname,roomType,offerName,mprice,wowprice;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rerelesed);
        profile = getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE, Context.MODE_PRIVATE);
        editor= profile.edit();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        minute_seek = (SeekBar) findViewById(R.id.seekbar_timer);
        no_rooms = (EditText)findViewById(R.id.no_roomse);
        type = (EditText)findViewById(R.id.no_roomsr);
        offername = (EditText)findViewById(R.id.offer_namer);
        hotel = (EditText)findViewById(R.id.h_name);

        sales_price = (EditText)findViewById(R.id.sales_pricer);
        check_in = (EditText)findViewById(R.id.check_inr);
        check_in.setKeyListener(null);
        check_in_time = (EditText)findViewById(R.id.check_in_timer);
        check_in_time.setKeyListener(null);
        check_out = (EditText)findViewById(R.id.check_outr);
        check_out.setKeyListener(null);
        check_out_time = (EditText)findViewById(R.id.check_out_timer);
        check_out_time.setKeyListener(null);


        releaseOffers = (Button)findViewById(R.id.release_offerr);
        schedule_date= (EditText) findViewById(R.id.schedule_dater);
        schedule_time= (EditText)findViewById(R.id.schedule_timer);
        offer_time= (TextView) findViewById(R.id.offer_time_viewr);
        offer_time1= (TextView) findViewById(R.id.offer_minutes1_viewr);
        volleySingleton= VolleySingleton.getinstance();
        requestQueue = volleySingleton.getrequestqueue();

        //recieve data from intent

        Intent intent=getIntent();
       roomNo= intent.getStringExtra("roomNo");
        wowprice=intent.getStringExtra("wowPrice");
        mprice=intent.getStringExtra("maxPrice");
        offerName=intent.getStringExtra("offerName");
        hname=intent.getStringExtra("hotelName");
        roomType=intent.getStringExtra("roomType");
        hid=intent.getStringExtra("hid");
        rid=intent.getStringExtra("rid");

    hotel.setText(hname);
    offername.setText(offerName);
     no_rooms.setText(roomNo);
        sales_price.setText(wowprice);
    type.setText(roomType);
//
//Log.e("mprice",mprice);
//Log.e("roomno",roomNo);


        //auto schedule
        as.add(Calendar.MINUTE, 10);

        String date_sched_auto = simpledateformat12.format(as.getTime());

        scheduleTimestring=simpledateformat123.format(as.getTime());
        scheduleDatestring=simpledateformat1.format(as.getTime());
        schedule_date.setText(date_sched_auto);
        schedule_time.setText(scheduleTimestring);
        scheduledatestring = scheduleDatestring + "T" + scheduleTimestring;
        try {
            sched_date = parser.parse(scheduledatestring);
        } catch (ParseException e) {
            Toast.makeText(ReReleaseOffer.this, "parse error in sched date", Toast.LENGTH_LONG).show();

        }





//auto  check out date nd time



        c.add(Calendar.DATE,2);
        checkoutdatestring = simpledateformat1.format(c.getTime());
        checkouttimestring = simpledateformat123.format(c.getTime());
        String preview_checkout=simpledateformat12.format(c.getTime());
        check_out.setText(preview_checkout);
        check_out_time.setText(checkouttimestring);
        checkoutstring = checkoutdatestring + 'T' + checkouttimestring;
        try {
            checkout_date = parser.parse(checkoutstring);
        } catch (ParseException e) {
            Toast.makeText(ReReleaseOffer.this, "parse error", Toast.LENGTH_LONG).show();
        }

            //auto check in time nd date


        d.add(Calendar.DATE,1);
        checkindatestring = simpledateformat1.format(d.getTime());
        checkintimestring = simpledateformat123.format(d.getTime());
        String preview=simpledateformat12.format(d.getTime());
        check_in.setText(preview);
        check_in_time.setText(checkintimestring);
        checkinstring = checkindatestring + "T" + checkintimestring;
        try {
            checkin_date = parser.parse(checkinstring);
        } catch (ParseException e) {
            Toast.makeText(ReReleaseOffer.this, "parse error", Toast.LENGTH_LONG).show();

        }


        //minute seek
        minute_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                offer_time1.setText(String.valueOf(progress));
                min_exp=offer_time1.getText().toString();
                Calendar tmp = (Calendar) as.clone();


                tmp.add(Calendar.MINUTE, progress);
                expiryTimestring=simpledateformat123.format(tmp.getTime());
                //  expiryTimestring=scheduleTimestring;
                offer_time.setText(expiryTimestring);


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });









//check in/out tie nd date
        check_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                hideSoftKeyboard();

                // Get Current Date
                c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                check_in.setError(null);
                check_out.setError(null);
                DatePickerDialog datePickerDialog = new DatePickerDialog(ReReleaseOffer.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                //String month = (monthOfYear+one)+"";
                                String month = String.format("%02d", monthOfYear + 1);
                                String day = String.format("%02d", dayOfMonth);
                                // String day1 = String.format("%02d", dayOfMonth+one);

//                                    checkindatestring = day + "-" + month + "-" + year;
                                checkindatestring = year + "-" + month + "-" + day;

                                check_in.setText(day + "-" + month + "-" + year);
                                try {
                                    c.setTime(simpledateformat1.parse(checkindatestring));

                                    c.add(Calendar.DATE,1);
                                    checkoutdatestring = simpledateformat1.format(c.getTime());
                                    String preview_checkout=simpledateformat12.format(c.getTime());
                                    check_out.setText(preview_checkout);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }



                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                //Toast.makeText(getContext(),mMonth+"",Toast.LENGTH_LONG).show();


            }



        });

        check_in_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();

                // Get Current Time
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
                check_in_time.setError(null);
                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(ReReleaseOffer.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                String hour = String.format("%02d",hourOfDay);
                                String min = String.format("%02d",minute);
                                checkintimestring = hour + ":" + min;
                                check_in_time.setText(checkintimestring);

                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();

            }

        });

        check_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();

                // Get Current Date
                c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                check_out.setError(null);
                DatePickerDialog datePickerDialog = new DatePickerDialog(ReReleaseOffer.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                String month = String.format("%02d",monthOfYear+1);
                                String day = String.format("%02d",dayOfMonth);

//                                    checkoutdatestring = day + "-" + month + "-" + year;
                                checkoutdatestring = year+"-"+month+"-"+day;
                                // checkoutdatestring = dayOfMonth + "-" + (monthOfYear + one) + "-" + year;
                                check_out.setText(day+"-"+month+"-"+year);


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }



        });

        check_out_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();

                // Get Current Time
                c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
                check_out_time.setError(null);
                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(ReReleaseOffer.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                String hour = String.format("%02d",hourOfDay);
                                String min = String.format("%02d",minute);
                                checkouttimestring = hour + ":" + min;
                                check_out_time.setText(checkouttimestring);

                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
            }

        });
        schedule_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();

                // Get Current Date
                mYear = as.get(Calendar.YEAR);
                mMonth = as.get(Calendar.MONTH);
                mDay = as.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(ReReleaseOffer.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth){
                                //String month = (monthOfYear+one)+"";
                                String month = String.format("%02d",monthOfYear+1);
                                String day = String.format("%02d",dayOfMonth);

//                                    scheduleDatestring = day + "-" + month + "-" + year;
                                scheduleDatestring = year+"-"+month+"-"+day;

                                schedule_date.setText(day + "-" + month + "-" + year);
//
                            }
                        },mYear,mMonth,mDay);
                datePickerDialog.show();
                //Toast.makeText(getContext(),mMonth+"",Toast.LENGTH_LONG).show();


            }



        });
        schedule_time.setOnClickListener(new View.OnClickListener() {
                                             @Override
                                             public void onClick(View v) {
                                                 hideSoftKeyboard();

                                                 // Get Current Time
                                                 min_exp=null;
                                                 offer_time.setText("");
                                                 offer_time1.setText("");
                                                 minute_seek.setProgress(0);
                                                 mHour1 = as.get(Calendar.HOUR_OF_DAY);
                                                 mMinute1 = as.get(Calendar.MINUTE);


                                                 // Launch Time Picker Dialog
                                                 TimePickerDialog timePickerDialog = new TimePickerDialog(ReReleaseOffer.this,
                                                         new TimePickerDialog.OnTimeSetListener() {

                                                             @Override
                                                             public void onTimeSet(TimePicker view, int hourOfDay,
                                                                                   int minute) {

                                                                 hour1 = String.format("%02d",hourOfDay);
                                                                 min1 = String.format("%02d",minute);
                                                                 scheduleTimestring = hour1 + ":" + min1;
                                                                 schedule_time.setText(scheduleTimestring);
                                                                 try {
                                                                     as.setTime(simpledateformat123.parse(scheduleTimestring));
                                                                     expiryTimestring=simpledateformat123.format(as.getTime());
                                                                     //  expiryTimestring=scheduleTimestring;
                                                                     offer_time.setText(expiryTimestring);

                                                                 } catch (ParseException e) {
                                                                     e.printStackTrace();
                                                                 }

                                                             }
                                                         }, mHour1, mMinute1, false);
                                                 timePickerDialog.show();
                                             }

                                         }



        );


//release button

        releaseOffers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                error=false;
                if (minute_seek.getProgress() == 0) {
                    Toast.makeText(ReReleaseOffer.this, "Please set expiry time", Toast.LENGTH_SHORT).show();

                    error = true;

                }

                if (!(check_in.getText().toString().matches("") || check_in_time.getText().toString().matches(""))) {
                    checkinstring = checkindatestring + "T" + checkintimestring;
                    try {
                        checkin_date = parser.parse(checkinstring);
                    } catch (ParseException e) {
                        Toast.makeText(ReReleaseOffer.this, "parse error", Toast.LENGTH_LONG).show();

                    }
                    if (System.currentTimeMillis() > checkin_date.getTime()) {
                        Toast.makeText(ReReleaseOffer.this, "wrong check in date", Toast.LENGTH_LONG).show();
                        error = true;
                    }

                }
                if (!(check_out.getText().toString().matches("") || check_out_time.getText().toString().matches(""))) {
                    checkoutstring = checkoutdatestring + 'T' + checkouttimestring;
                    try {
                        checkout_date = parser.parse(checkoutstring);
                    } catch (ParseException e) {
                        Toast.makeText(ReReleaseOffer.this, "parse error", Toast.LENGTH_LONG).show();

                    }
                    if (System.currentTimeMillis() > checkout_date.getTime()) {
                        Toast.makeText(ReReleaseOffer.this, "wrong check out date", Toast.LENGTH_LONG).show();
                        error = true;
                    }

                }

                if (!(schedule_date.getText().toString().matches("") || schedule_time.getText().toString().matches(""))) {
                    scheduledatestring = scheduleDatestring + "T" + scheduleTimestring;
                    try {
                        sched_date = parser.parse(scheduledatestring);
                    } catch (ParseException e) {
                        Toast.makeText(ReReleaseOffer.this, "parse error", Toast.LENGTH_LONG).show();

                    }
                }


                if (!(schedule_date.getText().toString().matches("") || check_in.getText().toString().matches("") || check_out_time.getText().toString().matches("") || check_in_time.getText().toString().matches("") || expiryTimestring.equals(""))) {

                    expireStringDate = scheduleDatestring + "T" + expiryTimestring;
                    Log.e("expdate", expireStringDate);
                    Log.e("exptime", expiryTimestring);

                    try {
                        expire_date = parser.parse(expireStringDate);
                    } catch (ParseException e) {
                        Toast.makeText(ReReleaseOffer.this, "parse error1111", Toast.LENGTH_LONG).show();

                    }
                    Log.e("exptime", expire_date.toString());
                    if (sched_date.getTime() > checkin_date.getTime()) {

                        Toast.makeText(ReReleaseOffer.this, "Wrong Schedule Date", Toast.LENGTH_LONG).show();
                        error = true;
                    }

                }


                if (!check_out.getText().toString().matches("") && !check_in.getText().toString().matches("") && !check_out_time.getText().toString().matches("") && !check_in_time.getText().toString().matches("") && !checkin_date.toString().matches("") && !checkout_date.toString().matches("")) {

                    if (checkin_date.getDate() > checkout_date.getDate()) {
                        Toast.makeText(ReReleaseOffer.this, "check in can't be after check out ", Toast.LENGTH_LONG).show();
                        error = true;

                    }

                }


                if (error == false) {
                    try {
                        releaseOffer();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });





    }
private void releaseOffer()throws JSONException{

    final ProgressDialog dialog1 = new ProgressDialog(ReReleaseOffer.this);
    dialog1.show();

    StringRequest request = new StringRequest(Request.Method.POST, Constants.RELEASE_OFFER_URL,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String respons) {
                    dialog1.dismiss();
                    Log.e("response",respons.toString());
                    JSONObject response = null;
                    HashSet<String> registered_offers = new HashSet<String>();
                    try {
                        response = new JSONObject(respons);
                        registered_offers.add(response.toString());

                    } catch (JSONException e) {
                        Log.e("error",e.toString());
                    }
                    try {
                        if(response.getBoolean("res"))
                        {
                            Log.e("offerid",response.toString());

                            if(profile.getStringSet(Constants.RELEASED_OFFER, new HashSet<String>()).size()>0) {
                                Log.e("storing", "not_first");
                                //Log.e("data",profile.getStringSet(Constants.REGISTERED_HOTELS_NAMES,null).toString());
                                registered_offers.addAll(profile.getStringSet(Constants.RELEASED_OFFER, null));
                            }
//                                                  }
                            else {
                                Log.e("storing","first");
                            }
                            Log.e("offers",registered_offers.toString());
                            editor.putStringSet(Constants.RELEASED_OFFER, registered_offers);
                            editor.commit();

                            no_rooms.setText("");
                            sales_price.setText("");
                            check_in.setText("");
                            check_in_time.setText("");
                            check_out.setText("");
                            check_out_time.setText("");
                            schedule_date.setText("");
                            schedule_time.setText("");

                            offer_time.setText("");
                           Intent home=new Intent(ReReleaseOffer.this,Home.class);
                            startActivity(home);

                            Toast.makeText(ReReleaseOffer.this,"Successfully Released",Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(ReReleaseOffer.this,response.getString("response"),Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        Toast.makeText(ReReleaseOffer.this,e.toString(),Toast.LENGTH_LONG).show();
                    }

                }
            }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            dialog1.dismiss();
            Toast.makeText(ReReleaseOffer.this,error.toString(),Toast.LENGTH_LONG).show();
            Log.e("vollley error",error.toString());
        }
    }){
        @Override
        protected Map<String,String> getParams(){
            Map<String,String> params = new HashMap<String, String>();


            params.put(HotelRegistrationConstants.OWNER_ID,profile.getString(Constants.ID,"default"));
            params.put(Constants.EMAIL,profile.getString(Constants.EMAIL,"default"));
            params.put(Release_constants.HOTEL_ID,hid);
            params.put(Release_constants.ROOM_ID,rid);

            params.put(Release_constants.HOTEL_NAME,hname);
            params.put(Release_constants.OFFER_NAME,offername.getText().toString());
            params.put(Release_constants.NO_OF_ROOMS,no_rooms.getText().toString());
            params.put(Release_constants.ROOM_TYPE,roomType);
            params.put(AddroomConstants1.PRICE_MRP,mprice);
            params.put(Release_constants.WOW_PRICE,wowprice);
            params.put(Release_constants.CHECK_IN,checkinstring);
            Log.e("checkin",checkinstring);
            Log.e("checkout",checkoutstring);


            params.put(Release_constants.CHECK_OUT,checkoutstring);
            params.put(Release_constants.SCHEDULE,scheduledatestring);
            params.put(Release_constants.EXPIRY,expireStringDate);

            Log.e("params",params.toString());
            return params;
        }
    };
    request.setRetryPolicy(new DefaultRetryPolicy(
            10000,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    requestQueue.add(request);
    // Toast.makeText(getContext(), "Congratulations! Offers relaeased.", Toast.LENGTH_LONG).show();

}





    private void hideSoftKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            //focusOnView(policy);
        }


    }

    //For back button in toolbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
