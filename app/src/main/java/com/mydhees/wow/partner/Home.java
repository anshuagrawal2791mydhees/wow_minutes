package com.mydhees.wow.partner;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.mydhees.wow.partner.allConstants.Constants;
import com.mydhees.wow.partner.drawer_fragments.Account;
import com.mydhees.wow.partner.drawer_fragments.AddRoom;
import com.mydhees.wow.partner.drawer_fragments.ContactUs;
import com.mydhees.wow.partner.drawer_fragments.Credits;
import com.mydhees.wow.partner.drawer_fragments.LeadsDashboard;
import com.mydhees.wow.partner.drawer_fragments.ProfileAndReviews;
import com.mydhees.wow.partner.drawer_fragments.RegisterHotel;
import com.mydhees.wow.partner.drawer_fragments.ReleaseOffer;
import com.mydhees.wow.partner.registration_and_login.first_screen;

//import android.support.multidex.MultiDex;

//import android.support.v4.app.FragmentManager;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    View headerView;
    TextView companyName;
    TextView email;
    SharedPreferences profile;
    String title = "";
    ImageView profilepic;
    private String goToFragment="";
    private Context mContext;
    NavigationView navigationView;
    int prev_id = R.id.leads_dashboard; // varible to save the id of the previously selected item in the navigation view


//    public static ProgressDialog //;
//jk

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Intent intent2 = getIntent();
        if(intent2.getStringExtra("goToFragment")!=null)
            goToFragment = intent2.getStringExtra("goToFragment");
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        final SharedPreferences prefs = getApplicationContext().getSharedPreferences(
                Constants.LAUNCH_TIME_PREFERENCE_FILE, Context.MODE_PRIVATE);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        profile = getApplicationContext().getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE,Context.MODE_PRIVATE);
        if (!prefs.getBoolean(Constants.FIRST_TIME,false)) {
               // <---- run your one time code here

            Intent intent = new Intent(this,first_screen.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            startActivity(intent);


        }
        supportRequestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        setContentView(R.layout.activity_home);
       // SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setElevation(0f);

        mContext = this;
        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView,new LeadsDashboard()).commit();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.inflateHeaderView(R.layout.nav_header_main2);
        navigationView.setNavigationItemSelectedListener(this);




        companyName = (TextView)headerView.findViewById(R.id.companyName);
//        companyName.setText("new_text");
        email = (TextView)headerView.findViewById(R.id.email);
//        email.setText("new text");
        profilepic = (ImageView)headerView.findViewById(R.id.imageView);
        if(!profile.getString(Constants.PROFILE_IMAGE_URI,"default").equals("default"))
            profilepic.setImageBitmap(BitmapFactory.decodeFile(profile.getString(Constants.PROFILE_IMAGE_URI,"default")));

        companyName.setText(profile.getString(Constants.NAME,"Mydhees Technology Pvt Ltd"));
        email.setText(profile.getString(Constants.EMAIL,"example@example.com"));


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if(!profile.getString(Constants.PROFILE_IMAGE_URI,"default").equals("default"))
                    profilepic.setImageBitmap(BitmapFactory.decodeFile(profile.getString(Constants.PROFILE_IMAGE_URI,"default")));

                companyName.setText(profile.getString(Constants.NAME,"Mydhees Technology Pvt Ltd"));
                email.setText(profile.getString(Constants.EMAIL,"example@example.com"));

            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();




    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //noinspection SimplifiableIfStatement
        if (id == R.id.share) {
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBody = "Hey! Download an awesome app\n*WOW Minutes* and get exciting offers for Hotels,Bus,Restaurants,Paying Guest and may more.... Try it now by clicking here http://www.mydhees.com/index.php ";
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject Here");
            sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));

            return true;
        }
        if (id == R.id.helpusimprove) {
            startActivity(new Intent(Home.this,HelpUsImprove.class));

            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

//        setProgressBarIndeterminate(true);
//        setProgressBarIndeterminateVisibility(true);
       // = new ProgressDialog(this);
        //.show();
        if (id == R.id.leads_dashboard) {
            //.dismiss();
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction().setCustomAnimations(R.anim.card_flip_left_out, R.anim.card_flip_right_in);//R.anim.card_flip_right_in,R.anim.card_flip_right_out,R.anim.card_flip_left_in,R.anim.card_flip_left_out);
            mFragmentTransaction.replace(R.id.containerView,new LeadsDashboard()).commit();
            title="WOW Partners";
        } else if (id == R.id.profile_and_reviews) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mFragmentManager = getSupportFragmentManager();
                    mFragmentTransaction = mFragmentManager.beginTransaction().setCustomAnimations(R.anim.card_flip_left_out, R.anim.card_flip_right_in);//R.anim.card_flip_right_in,R.anim.card_flip_right_out,R.anim.card_flip_left_in,R.anim.card_flip_left_out);
                    mFragmentTransaction.replace(R.id.containerView,new ProfileAndReviews()).commit();

                }
            }, 400);
            title="Profile and Review";


        } else if (id == R.id.release_offer) {
            mFragmentManager = getSupportFragmentManager();

            mFragmentTransaction = mFragmentManager.beginTransaction().setCustomAnimations(R.anim.card_flip_left_out, R.anim.card_flip_right_in);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

//                    mFragmentManager = getSupportFragmentManager();
//                    mFragmentTransaction = mFragmentManager.beginTransaction().setCustomAnimations(R.anim.card_flip_left_out,R.anim.card_flip_right_in);//R.anim.card_flip_right_in,R.anim.card_flip_right_out,R.anim.card_flip_left_in,R.anim.card_flip_left_out);
                    mFragmentTransaction.replace(R.id.containerView,new ReleaseOffer()).commit();

                }
            }, 300);
            title="Release Offer";

        } else if (id == R.id.register_hotel) {
//


//            final ProgressDialog dialog = new ProgressDialog(this);
//            dialog.show();


            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    mFragmentManager = getSupportFragmentManager();
                    mFragmentTransaction = mFragmentManager.beginTransaction().setCustomAnimations(R.anim.card_flip_left_out, R.anim.card_flip_right_in);//R.anim.card_flip_right_in,R.anim.card_flip_right_out,R.anim.card_flip_left_in,R.anim.card_flip_left_out);
                    ;

                    mFragmentTransaction.replace(R.id.containerView,new RegisterHotel());
                    mFragmentTransaction.commit();

//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            dialog.dismiss();
//                        }
//                    },1500);


                }
            }, 400);
            title="Register Hotel";
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    //.dismiss();
//                }
//            },2000);


        } else if (id == R.id.add_room) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mFragmentManager = getSupportFragmentManager();
                    mFragmentTransaction = mFragmentManager.beginTransaction().setCustomAnimations(R.anim.card_flip_left_out, R.anim.card_flip_right_in);//R.anim.card_flip_right_in,R.anim.card_flip_right_out,R.anim.card_flip_left_in,R.anim.card_flip_left_out);
                    mFragmentTransaction.replace(R.id.containerView,new AddRoom()).commit();


                }
            }, 400);
            title="Add Room";
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //.dismiss();
                }
            },1000);
        } else if (id == R.id.nav_credits) {
            //.dismiss();
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction().setCustomAnimations(R.anim.card_flip_left_out, R.anim.card_flip_right_in);//R.anim.card_flip_right_in,R.anim.card_flip_right_out,R.anim.card_flip_left_in,R.anim.card_flip_left_out);
            mFragmentTransaction.replace(R.id.containerView,new Credits()).commit();
            title="Credits";

        }
        else if(id == R.id.account){
            //.dismiss();
            // edited by Pranav
            /*mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction().setCustomAnimations(R.anim.card_flip_left_out, R.anim.card_flip_right_in);//R.anim.card_flip_right_in,R.anim.card_flip_right_out,R.anim.card_flip_left_in,R.anim.card_flip_left_out);
            mFragmentTransaction.replace(R.id.containerView,new Account()).commit();
            title="Account";*/
            // implementing logout Functionality
            final SharedPreferences[] profile = new SharedPreferences[1];
            final SharedPreferences[] firstTime = new SharedPreferences[1];
            new AlertDialog.Builder(mContext)
                    .setTitle("Confirm Logout")
                    .setMessage("Are you sure you want to log out ?")
                    .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // logout
                            firstTime[0] = getSharedPreferences(Constants.LAUNCH_TIME_PREFERENCE_FILE, Context.MODE_PRIVATE);
                            profile[0] = getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor_firstTime = firstTime[0].edit();
                            SharedPreferences.Editor editor_profile = profile[0].edit();
                            editor_firstTime.putBoolean(Constants.FIRST_TIME,false);
                            editor_firstTime.apply();
                            editor_profile.clear();
                            editor_profile.apply();
                            //LoginManager.getInstance().logOut();
                            Intent intent = new Intent(mContext,first_screen.class );
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // set the checked item to the current fragment selection
                            navigationView.setCheckedItem(prev_id);
                        }
                    })
                    .setCancelable(false)
                    .show();

        }
        else if(id == R.id.contact_us){
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.containerView,new ContactUs()).commit();
            title="Contact Us";

        }

        else if(id == R.id.how_it_works){
            Intent it=new Intent(Home.this,DemoForApp.class);
            it.putExtra("HOWITWORKS",true);
            startActivity(it);

        }
        getSupportActionBar().setTitle(title);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


//    @Override
//    protected void attachBaseContext(Context base)
//    {
//        super.attachBaseContext(base);
////        MultiDex.install(Home.this);
//    }




    @Override
    protected void onResume() {
        super.onResume();

        companyName.setText(profile.getString(Constants.NAME,"Mydhees Technology Pvt Ltd"));
        email.setText(profile.getString(Constants.EMAIL,"example@example.com"));
        if(!profile.getString(Constants.PROFILE_IMAGE_URI,"default").equals("default"))
            profilepic.setImageBitmap(BitmapFactory.decodeFile(profile.getString(Constants.PROFILE_IMAGE_URI,"default")));

    }

    @Override
    protected void onPause() {

        super.onPause();
    }

}
