package com.mydhees.wow.partner.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.mydhees.wow.partner.HotelDisplay;
import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.RoomDisplay;
import com.mydhees.wow.partner.VolleySingleton;
import com.mydhees.wow.partner.objects.registeredHotels;
import com.mydhees.wow.partner.objects.roomDisplay;

import java.util.ArrayList;

/**
 * Created by Jashan Kochar on 6/27/2016.
 */
public class RoomTypeDisplay extends RecyclerView.Adapter<RoomTypeDisplay.myviewholder> {


    static Context context;
    LayoutInflater inflater;
    static ArrayList<roomDisplay> rd = new ArrayList<>();
    static roomDisplay rdObject;
    String imageurl;
    VolleySingleton volleySingleton;
    ImageLoader imageLoader;

    public RoomTypeDisplay(Context context) {
        this.context = context;
        volleySingleton = VolleySingleton.getinstance();
        imageLoader = volleySingleton.getimageloader();
        inflater=LayoutInflater.from(context);
    }

    public void displayroomType(roomDisplay reg)
    {
        rd.add(0,reg);
        notifyItemInserted(0);
    }


    public ArrayList<roomDisplay> getlist()
    {
        return rd;
    }
    @Override
    public RoomTypeDisplay.myviewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.roomtype_recycler_row,parent,false);
        myviewholder viewholder = new myviewholder(view);

        return  viewholder;
    }
    public void clearAll(){
        rd.clear();
    }
    @Override
    public void onBindViewHolder(final RoomTypeDisplay.myviewholder holder, final int position) {

        rdObject=rd.get(position);

        holder.hname.setText(rdObject.getRoomType());
        holder.rno.setText(rdObject.getRoomnos());
        Log.e("url",rdObject.getImage_link());
        if(!rdObject.getImage_link().matches(""))
        {
            imageurl = "https://s3-us-west-2.amazonaws.com/wowimagesupload/resized/middle/"+rdObject.getImage_link()+".jpg";
            imageLoader.get(imageurl, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    holder.img.setImageDrawable(null);
                    holder.img.setImageBitmap(response.getBitmap());
                }

                @Override
                public void onErrorResponse(VolleyError error) {


                }
            });
        }
        else
            holder.img.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_launcher));

        holder.cv_hotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, rd.get(position).getRoomType(), Toast.LENGTH_SHORT).show();
                Intent intent= new Intent(context,RoomDisplay.class);
                intent.putExtra("rid",rd.get(position).getRid());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

            }
        });


    }

    @Override
    public int getItemCount() {
        return rd.size();
    }

    static class myviewholder extends RecyclerView.ViewHolder {
        private TextView hname,rno;
        ImageView img;
        CardView cv_hotel;

        public myviewholder(View itemview) {
            super(itemview);
            hname = (TextView) itemview.findViewById(R.id.hotel_name222);
            rno = (TextView) itemview.findViewById(R.id.cottage);
img= (ImageView) itemview.findViewById(R.id.rm1_image);
                    cv_hotel= (CardView) itemview.findViewById(R.id.card_view_room);

        }
    }
}
