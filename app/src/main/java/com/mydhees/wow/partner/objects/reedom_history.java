package com.mydhees.wow.partner.objects;

import java.util.Date;



public class reedom_history {
    String hotel_namerh,offer_namerh,reedom_pointsrh;
    Date date_rh;

    public reedom_history(String hotel_namerh, String offer_namerh, String reedom_pointsrh, Date date_rh) {
        this.hotel_namerh = hotel_namerh;
        this.offer_namerh = offer_namerh;
        this.reedom_pointsrh = reedom_pointsrh;
        this.date_rh = date_rh;
    }

    public String getHotel_namerh() {
        return hotel_namerh;
    }

    public String getOffer_namerh() {
        return offer_namerh;
    }

    public String getReedom_pointsrh() {
        return reedom_pointsrh;
    }

    public Date getDate_rh() {
        return date_rh;
    }
}
