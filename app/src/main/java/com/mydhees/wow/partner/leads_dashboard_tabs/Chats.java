package com.mydhees.wow.partner.leads_dashboard_tabs;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.adapters.ChatListAdapter;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Chats extends Fragment {

    RecyclerView recycler;
    ArrayList<ChatListAdapter> list = new ArrayList<>();
    ChatListAdapter myadapter;
    LinearLayoutManager manager;

    public Chats() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_chats, container, false);

        myadapter=new ChatListAdapter(getContext());
        //recycler.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
//        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
//        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        recycler.setLayoutManager(layoutManager);
//
//        recycler.setAdapter(myadapter);
//        recycler.setVisibility(View.VISIBLE);
//        recycler = (RecyclerView)v.findViewById(R.id.recyclerChat);
//        recycler.setLayoutManager(manager);


        return  v;
    }

}
