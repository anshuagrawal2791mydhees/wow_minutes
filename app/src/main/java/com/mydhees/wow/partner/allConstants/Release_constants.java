package com.mydhees.wow.partner.allConstants;

/**
 * Created by Jashan Kochar on 6/20/2016.
 */
public class Release_constants {
    public static final String OWNER_ID="ownerId";
    public static final String EMAIL="email";
    public static final String HOTEL_ID="hotelId";
    public static final String ROOM_ID="roomId";
    public static final String OFFER_ID="offerId";
    public static final String HOTEL_NAME="hotelName";
    public static final String OFFER_NAME="offerName";
    public static final String NO_OF_ROOMS="noOfRooms";
    public static final String ROOM_TYPE="roomType";
    public static final String PRICE_MRP="priceMRP";
    public static final String WOW_PRICE="wowPrice";
    public static final String CHECK_IN="checkIn";
    public static final String CHECK_OUT="checkOut";
    public static final String SCHEDULE="scheduledAt";
    public static final String EXPIRY="expiresAt";


}
