package com.mydhees.wow.partner.credits_tab;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.VolleySingleton;
import com.mydhees.wow.partner.adapters.RedeemHistoryAdapter;
import com.mydhees.wow.partner.allConstants.Constants;
import com.mydhees.wow.partner.objects.RedeemHistoryObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 */
public class RedeemHistory extends Fragment {
    SharedPreferences profile;
    VolleySingleton volleysingleton;
    RequestQueue requestqueue;
    ArrayList<RedeemHistoryObject> list;
    DateFormat parser =new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
    RecyclerView recyclerView;
    RedeemHistoryAdapter adapter;




    public RedeemHistory() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_redeem_history, container, false);
        list = new ArrayList<>();
        profile = getActivity().getSharedPreferences(Constants.PROFILE_PREFERENCE_FILE,Context.MODE_PRIVATE);
        volleysingleton = VolleySingleton.getinstance();
        requestqueue=volleysingleton.getrequestqueue();
        recyclerView = (RecyclerView)v.findViewById(R.id.rh_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        adapter=new RedeemHistoryAdapter(getContext());
        recyclerView.setAdapter(adapter);
//        Redeem_history();







        return v;
    }

    @Override
    public void onStart() {
        super.onStart();

            Redeem_history();
    }

    private void Redeem_history() {
        final ProgressDialog dialog = new ProgressDialog(getContext());
//        dialog.setCancelable(false);
//        dialog.setTitle("Fetching details...");
        dialog.setMessage("Fetching details...");
//        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.REDEEM_HISTORY, new Response.Listener<String>() {
            @Override
            public void onResponse(String respons) {
//                dialog.dismiss();
                try {
                    JSONObject response = new JSONObject(respons);
                    Log.e("resp",response.toString());
                    int offer_cost = Integer.parseInt(response.getString("offer_cost"));
                    response = response.getJSONObject("result");

                    JSONArray orders = response.getJSONArray("orders");

                    JSONArray offers = response.getJSONArray("offers");
                    for (int i=0;i<orders.length();i++){
                        JSONObject order = orders.getJSONObject(i);
                        list.add(new RedeemHistoryObject(Integer.parseInt(order.getString("amount")),parser.parse(order.getJSONObject("payment_gateway_response").getString("created")),"credit",order.getString("order_id")));
                    }
                    for(int i=0;i<offers.length();i++){
                        JSONObject offer = offers.getJSONObject(i);
                        list.add(new RedeemHistoryObject(offer_cost,parser.parse(offer.getString("createdAt")),"debit",offer.getString("offerName"),offer.getString("hotelName"),offer.getString("offerId")));

                    }
                    Collections.sort(list, new Comparator<RedeemHistoryObject>() {
                        @Override
                        public int compare(RedeemHistoryObject lhs, RedeemHistoryObject rhs) {
                            return (rhs.getDate().compareTo(lhs.getDate()));
                        }
                    });
                    adapter.clearAll();
                    adapter.addAll(list);



                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                dialog.dismiss();
                Log.e("err",error.toString());
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
//
                params.put("Id",profile.getString(Constants.ID,"default"));
//
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestqueue.add(stringRequest);
    }

    // TODO: Rename method, update argument and hook method into UI event




    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

}
