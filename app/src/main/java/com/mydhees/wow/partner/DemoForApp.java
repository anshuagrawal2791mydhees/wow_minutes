package com.mydhees.wow.partner;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.WindowManager;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.mydhees.wow.partner.registration_and_login.first_screen;

/**
 * Created by Jashan Kochar on 7/12/2016.
 */
public class DemoForApp extends AppIntro {

    boolean goToHome = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // edited By Pranav
        Intent intent = getIntent();
        if (intent.getBooleanExtra("HOWITWORKS",false)){
            goToHome = true;
        }
        else {
            goToHome = false;
        }
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.primary_dark));
        }

        SampleSlide Fra=new SampleSlide();

        String title,description;
        title="WOW Minutes";
        description="Make exciting offers here!!!";
      //  addSlide(AppIntroFragment.newInstance(title, description, R.drawable.app_icon, Color.parseColor("#689F38")));
//        addSlide(AppIntroFragment.newInstance("Add Hotels", "Register your Hotels to make them reachable to customers. You can register under Register Hotel view when swipping from left to right.", R.drawable.hotl, Color.parseColor("#689F38")));
//        addSlide(AppIntroFragment.newInstance("Add Rooms", "After registering your hotel you can add any number of rooms with its type. To do so click on Add Room, while swipping screen from left to right.", R.drawable.room, Color.parseColor("#689F38")));
//        addSlide(AppIntroFragment.newInstance("Now, Let's Release Offer", "Now, finally to release last minute offers for your hotel rooms click on Release offer.", R.drawable.release, Color.parseColor("#689F38")));
//        addSlide(AppIntroFragment.newInstance("It's Simple & Done", "And, you are done releasing offers for your hotel rooms at special discounted price.", R.drawable.successful, Color.parseColor("#689F38")));
addSlide(SampleSlide.newInstance(R.layout.fragment_inro1));
addSlide(SampleSlide.newInstance(R.layout.fragment_inro2));
addSlide(SampleSlide.newInstance(R.layout.fragment_inro3));
addSlide(SampleSlide.newInstance(R.layout.fragment_inro4));
addSlide(SampleSlide.newInstance(R.layout.fragment_inro5));
        // OPTIONAL METHODS
        // Override bar/separator color.
        setBarColor(Color.parseColor("#689F38"));
        setSeparatorColor(Color.parseColor("#FFFFFF"));


        // Hide Skip/Done button.
        showSkipButton(true);
        setProgressButtonEnabled(true);

setFadeAnimation();
        // Turn vibration on and set intensity.
        // NOTE: you will probably need to ask VIBRATE permisssion in Manifest.
//        setVibrate(true);
//        setVibrateIntensity(30);
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
       finish();
        // edited by Pranav
        checkWhereToGo();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        finish();
        // edited by Pranav
        checkWhereToGo();
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
        // Do something when the slide changes.
    }
    // edited by Pranav
    public void checkWhereToGo (){
        Intent intent;
        if (goToHome){
            intent = new Intent(DemoForApp.this,Home.class);
        }
        else {
            intent = new Intent(DemoForApp.this,first_screen.class);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }
}
