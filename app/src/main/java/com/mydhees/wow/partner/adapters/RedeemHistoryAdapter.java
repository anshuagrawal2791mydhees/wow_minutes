package com.mydhees.wow.partner.adapters;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mydhees.wow.partner.R;
import com.mydhees.wow.partner.objects.RedeemHistoryObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;



public class RedeemHistoryAdapter extends RecyclerView.Adapter<RedeemHistoryAdapter.myviewholder> {
    Context context;
    LayoutInflater inflater;
    ArrayList<RedeemHistoryObject> list = new ArrayList<>();
    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");



    public RedeemHistoryAdapter(Context context) {
        this.context=context;
        inflater=LayoutInflater.from(context);
    }
    public void addAll(ArrayList<RedeemHistoryObject> sent_list){
        list.clear();
        list.addAll(sent_list);
        notifyDataSetChanged();
    }


    public ArrayList<RedeemHistoryObject> getofferlist()
    {
        return list;
    }
    public void clearAll()
    {
        list.clear();
    }

    @Override
    public myviewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.redeem_history_row,parent,false);
        myviewholder viewholder = new myviewholder(v);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(myviewholder holder, final int position) {

        RedeemHistoryObject redeem_his = list.get(position);

        if(redeem_his.getType().equals("debit")){
            holder.header.setBackgroundColor(Color.rgb(244,67,54));
            holder.tv01.setText("Offer");
            holder.tv02.setText(redeem_his.getOffer_name());
            holder.tv03.setText("Hotel Name");
            holder.tv04.setText(redeem_his.getHotel_name());
            holder.tv05.setText("Offer ID");
            holder.tv06.setText(redeem_his.getOffer_id());

        }

        else {
            holder.header.setBackgroundColor(Color.rgb(220,237,200));
            holder.tv03.setVisibility(View.GONE);
            holder.tv04.setVisibility(View.GONE);
            holder.tv05.setVisibility(View.GONE);
            holder.tv06.setVisibility(View.GONE);
            holder.tv01.setText("Order ID");
            holder.tv02.setText(redeem_his.getOrder_id());
        }
        holder.date.setText(dateFormat.format(redeem_his.getDate()));
        holder.points.setText(redeem_his.getAmount()+"");




    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class myviewholder extends RecyclerView.ViewHolder
    {
        TextView tv01,tv02,tv03,tv04,tv05,tv06;
        TextView date;
        LinearLayout header;
        TextView points;


        public myviewholder(View itemView) {
            super(itemView);

            tv01=(TextView)itemView.findViewById(R.id.tv01);
            tv02=(TextView)itemView.findViewById(R.id.tv02);
            tv03=(TextView)itemView.findViewById(R.id.tv03);
            tv04=(TextView)itemView.findViewById(R.id.tv04);
            tv05=(TextView)itemView.findViewById(R.id.tv05);
            tv06=(TextView)itemView.findViewById(R.id.tv06);
            points = (TextView)itemView.findViewById(R.id.redeem_points_rh);
            date = (TextView)itemView.findViewById(R.id.date_rh);
            header = (LinearLayout)itemView.findViewById(R.id.header);

        }
    }
}
